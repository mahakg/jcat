<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		parent::checkSignIn();
		$this->layout->setLayout('layouts/main');	
	}

	public function index()
	{
		$create_data =  $this->input->post();
                $this->load->model('test_model', 'test');
		$view_jobs = $this->test->read_view_jobs(0,10);
		$skill_name = $this->test->read_skill_set();
                $this->load->model('jobs_model', 'job');
		$submitted_jobs = $this->job->read_submitted_jobs(0,10);
                $website_activity=$this->job->site_activity();
               //print_r($website_activity);exit();
                //$content_name=$this->job->site_activity($website_activity);
             
		$user_type = $this->session->userdata('user_type');
		if($user_type == 'recruiter'){
		$this->layout->view('home/'. $user_type .'_homepage', array('view_jobs' => $view_jobs,'website_activity'=>$website_activity));
		}else if($user_type == 'candidate'){
		$this->layout->view('home/'. $user_type .'_homepage', array('submitted_jobs' => $submitted_jobs, 'skill_name' => $skill_name,'website_activity'=>$website_activity));
		}
	}	
	
	
}
