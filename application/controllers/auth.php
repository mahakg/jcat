<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('layout');	
	}

	public function login()
	{
	        $this->layout->setLayout('layouts/main');
		$body_data = array();
		//$layout_data['content_body'] = $this->load->view('auth/login', $body_data, true);
		$this->layout->view('auth/login');
	}
        
	public function oauth($providername)
	{
		$provider = $this->config->item($providername);
		$key=$provider['key'];
		$secret=$provider['secret'];

		$this->load->helper('url');

		$this->load->spark('oauth/0.3.1');

		// Create an consumer from the config
		$consumer = $this->oauth->consumer(array(
		    'key' => $key,
		    'secret' => $secret,
		));

		// Load the provider
		$provider = $this->oauth->provider($providername);

		// Create the URL to return the user to
		$callback = site_url('auth/oauth/'.$provider->name);

		if ( ! $this->input->get_post('oauth_token'))
		{
			// Add the callback URL to the consumer
			$consumer->callback($callback);

			// Get a request token for the consumer
			$token = $provider->request_token($consumer);

			// Store the token
			$this->session->set_userdata('oauth_token', base64_encode(serialize($token)));

			// Get the URL to the twitter login page
			$url = $provider->authorize($token, array(
			'oauth_callback' => $callback,
			));

			// Send the user off to login
			redirect($url);
		}
		else
		{
			if ($this->session->userdata('oauth_token'))
			{
			// Get the token from storage
			$token = unserialize(base64_decode($this->session->userdata('oauth_token')));
			}

			if ( ! empty($token) AND $token->access_token !== $this->input->get_post('oauth_token'))
			{
			// Delete the token, it is not valid
			$this->session->unset_userdata('oauth_token');

			// Send the user back to the beginning
			exit('invalid token after coming back to site');
			}

			// Get the verifier
			$verifier = $this->input->get_post('oauth_verifier');

			// Store the verifier in the token
			$token->verifier($verifier);

			// Exchange the request token for an access token
			$token = $provider->access_token($consumer, $token);

			// We got the token, let's get some user data
			$user = $provider->get_user_info($consumer, $token);
			
			$this->load->model('Auth_model', 'auth');
			$this->auth->saveData($providername,$token,$user);
		}
	}

	
	public function oauth2($providername)
	{
		$provider = $this->config->item($providername);
		$key=$provider['key'];
		$secret=$provider['secret'];
		//$this->load->library('facebook', $fb_config);
		$this->load->spark('oauth2/0.3.1');

    $provider = $this->oauth2->provider($providername, array(
        'id' => $key,
        'secret' => $secret,
    ));

    if ( ! $this->input->get('code'))
    {
        // By sending no options it'll come back here
        $url = $provider->authorize();

        redirect($url);
    }
    else
    {
        try
        {
            // Have a go at creating an access token from the code
            $token = $provider->access($_GET['code']);

            // Use this object to try and get some user details (username, full name, etc)
            $user = $provider->get_user_info($token);
          
            $this->load->model('Auth_model', 'auth');
            $this->auth->saveData($providername,$token,$user);

            // Here you should use this information to A) look for a user B) help a new user sign up with existing data.
            // If you store it all in a cookie and redirect to a registration page this is crazy-simple.
            //echo "<pre>Tokens: ";
            

            //echo "\n\nUser Info: ";
            //var_dump($user);
            
        }

        catch (OAuth2_Exception $e)
        {
            show_error('That didnt work: '.$e);
        }

    }
	}
	
	public function logout()
	{
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->session->userdata = array();
	   	$this->session->sess_destroy();
		redirect('/auth/login', 'refresh');
	}
}
