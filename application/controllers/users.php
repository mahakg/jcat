<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		//parent::checkSignIn();
		$this->layout->setLayout('layouts/main');
	}
	
	public function profile()
	{
		$userdata = $this->session->userdata('userdata');
		$this->layout->view('users/user_profile' , array('user_data'=>$userdata));
	}

	public function recruiter_profile()
	{
		$this->session->set_userdata('user_type','recruiter');
		$user_data = $this->session->userdata('userdata');
		$this->load->model('user_model', 'user');
		$user_id = $this->session->userdata('user_id');
		$user_profile_data = $this->user->getProfileData($user_id);
		$this->layout->view('users/recruiter_profile', array('user_data'=>$user_data, 'user_profile_data'=>$user_profile_data));
	}
	
	public function candidate_profile()
	{
		$this->session->set_userdata('user_type','candidate');		
		$user_data = $this->session->userdata('userdata');
		$this->load->model('user_model', 'user');
		$user_id = $this->session->userdata('user_id');
		$user_profile_data = $this->user->getProfileData($user_id);
		$this->layout->view('users/candidate_profile', array('user_data'=>$user_data, 'user_profile_data'=>$user_profile_data));
	}
	
	public function add_users()
	{
		$users_data =  $this->input->post();
		if(isset($users_data)){
			$this->load->model('user_model', 'user');
			echo $user_id = $this->session->userdata('user_id');
			if($user_id==''){
			$insert_status = $this->user->insertUsers($users_data);
			}
			else{

			$insert_status = $this->user->UpdateUsers($users_data);
			}
			if($insert_status){
				echo json_encode(array('status'=>'true'));
			}
			else{
				echo json_encode(array('status'=>'true'));
			}
		}
		//$this->layout->view('users/candidate_profile');
	}
	
	public function add_users_profile()
	{
		$users_profile_data =  $this->input->post();
		if(isset($users_profile_data)){
			$this->load->model('user_model', 'user');
			
			$profile_user_id = $this->session->userdata('user_profile_id');
			if($profile_user_id==""){
			$insert_status = $this->user->insertUserProfile($users_profile_data);
			}
			else{
			$insert_status = $this->user->updateUserProfile($users_profile_data);
			}
			if($insert_status){
				
				$user_complete_profile = $this->user->returnCompleteProfile();
				ob_start();
				
				include_once(APPPATH . "views/users/profile_details.php");
				$content = ob_get_clean();
				echo json_encode(array('status'=>'true', 'profile_data'=>$content));
			}
			else{
				echo json_encode(array('status'=>'true'));
			}
		}
		//$this->layout->view('users/candidate_profile');
	}
	
	public function register_session(){
		if($_POST){
		    $this->session->set_userdata('login','true');
		    redirect('/home/index','refresh');
		    }
	}
	
	
}
