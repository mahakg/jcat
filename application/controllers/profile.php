<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		//parent::checkSignIn();
		$this->layout->setLayout('layouts/main');	
	}
        
        function index($username= NULL){
            $user=  $username;
            $this->load->model('user_model', 'user');
            $userId= $this->user->getUserId($user);
            $user_complete_profile = $this->user->completeProfile($userId['id']);
            $this->load->model('badge_model', 'badges');
            $user_profile_data = $this->badges->getbadgeData($userId['id']);
               // print_R($user_profile_data);
               // $read_badge_name=$this->badges->readbadgeDir($user_id);
            $this->layout->setLayout('layouts/home');
            $this->layout->view('profile/user',array('user_data'=>$user_complete_profile,'user_profile_data'=>$user_profile_data,'user_id'=>$userId['id']));        
        }
        
        function user(){
            $this->layout->setLayout('layouts/home');
            $this->layout->view('profile/user');
        }
        
        function contact(){
            $create_data =  $this->input->post();
            $username=$create_data['contact']['username'];
            $sender_name=$create_data['contact']['name'];
            $sender_email=$create_data['contact']['from_email'];
            $to_mail=$create_data['contact']['to_email'];
            $subject=$create_data['contact']['message'];
            Utility::email($sender_name, $sender_email,$to_mail,"Someone has contacted you via Rate your skills",$subject,$raw="",$attachments="", $cc=array(),$bcc=array());
            $this->session->set_flashdata('msg', 'Mail  has been sent'); //set session flash
            redirect('profile/index'.'/'.$username, 'refresh');
        }
		
	public function editprofile()
	{
		$this->load->helper(array('form'));
		//$data = $this->loadcommon();
		//$siteid = $this->getsiteid();
		$this->load->model('Profile_model', 'profile');
		$userdata = $this->profile->checklogin();
		print_r($userdata);
		if (!empty($userdata)){
		echo 123;
		if (isset($_GET['err'])) {
		$data['err'] = $_GET['err'];
		}
		$userid = $userdata['id'];
		
		$data['user'] = $this->profile->getuser($userid);
		$this->load->view('profile/profile_edit', $data);
                
                /* Web activity*/
                $user_id=$this->session->userdata('user_id');
                Utility::activity($user_id,"1","Profile","Edit Profile");
                }
	}	
	public function candidate_profile()
	{
		$this->load->model('user_model', 'user');
		$user_data = $this->session->userdata('userdata');
		$user_complete_profile = $this->user->returnCompleteProfile();
              //  print_r($user_data);
		$user_id = $this->session->userdata('user_id');
		$user_profile_data = $this->user->getProfileData($user_id);
		$this->layout->view('profile/candidate_profile', array('user_data'=>$user_data, 'user_profile_data'=>$user_profile_data,'user_complete_profile'=>$user_complete_profile));
	}
	
	public function recruiter_profile()
	{
		$this->load->model('user_model', 'user');
		$user_data = $this->session->userdata('userdata');
		$user_complete_profile = $this->user->returnCompleteProfile();
		$user_id = $this->session->userdata('user_id');
		$user_profile_data = $this->user->getProfileData($user_id);
		$this->layout->view('profile/recruiter_profile', array('user_data'=>$user_data, 'user_profile_data'=>$user_profile_data,'user_complete_profile'=>$user_complete_profile));
	}
}
