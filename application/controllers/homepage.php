<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class homepage extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
                $this->layout->setLayout('layouts/home');
		$this->layout->view('homepage/home');
	}
        public function home()
	{
                $this->layout->setLayout('layouts/home');
		$this->layout->view('homepage/home');
	}
	

}
