<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {
	
	function __construct()
	{
			
		parent::__construct();
		parent::checkSignIn();
		$this->layout->setLayout('layouts/main');
	}

	public function index()
	{
		$create_data =  $this->input->post();
                $this->load->model('test_model', 'test');
		$view_jobs = $this->test->read_view_jobs(0,10);
		$skill_name = $this->test->read_skill_set();
                $this->load->model('jobs_model', 'job');
		$submitted_jobs = $this->job->read_submitted_jobs(0,10);
                $website_activity=$this->job->site_activity();
               //print_r($website_activity);exit();
                //$content_name=$this->job->site_activity($website_activity);
             
		$user_type = $this->session->userdata('user_type');
		if($user_type == 'recruiter'){
		$this->layout->view('home/'. $user_type .'_homepage', array('view_jobs' => $view_jobs,'website_activity'=>$website_activity));
		}else if($user_type == 'candidate'){
		$this->layout->view('home/'. $user_type .'_homepage', array('submitted_jobs' => $submitted_jobs, 'skill_name' => $skill_name,'website_activity'=>$website_activity));
		}
	}
	
	public function create()
	{
		$create_data =  $this->input->post();
                $skill_id=$create_data['question']['skill_id'];
		if(isset($create_data['question'])){
                    $this->load->model('Test_model', 'test');
                    $insert_status = $this->test->insertQuestions($create_data['question']);
                        if($insert_status){
                            $user_id=$this->session->userdata('user_id');
                            Utility::activity($user_id,$skill_id,"Question","New Question Added");
                            $this->session->set_flashdata('msg', 'Question has been Added'); //set session flash
                            redirect('test/create', 'refresh');
                         }
                        else{
                            //echo "Insertion Failed";
                            $this->session->set_flashdata('msg', 'Insertion Failed'); //set session flash
                            redirect('test/create', 'refresh');
                        }
                }else{
                    $this->layout->view('test/create');    
                }
	}
	
	public function manage()
	{
		$create_data =  $this->input->post();
		
		$this->load->model('Test_model', 'test');
		$skill_name = $this->test->read_skill_set();
		if(isset($create_data['profile'])){
			$create_data['profile']['skill_set'] = implode(",", $create_data['profile']['skill_set']);
			$insert_status = $this->test->insertTests($create_data['profile']);
			
			if($insert_status){
                            $this->session->set_flashdata('msg', 'Email Sent'); //set session flash
                            redirect('test/manage', 'refresh');
			}
			else{
                            $this->session->set_flashdata('msg', 'Something Went wrong'); //set session flash
                            redirect('test/manage', 'refresh');
			}
		}
		$this->layout->view('test/manage', array('skill_name' => $skill_name));
	}
	
	public function senduser()
	{
		$this->layout->view('test/senduser');
	}
	
	public function viewprofiles()
	{
		$this->load->model('Test_model', 'test');
		$test_records = $this->test->read_test_records(0,10);
		$this->layout->view('test/viewprofiles', array('test_records' => $test_records));
	}
	
	public function newopportunities()
	{
		$create_data =  $this->input->post();
		$this->load->model('Test_model', 'test');
		$skill_name = $this->test->read_skill_set();
		if(isset($create_data['profile'])){
			$create_data['profile']['skill_set'] = implode(",", $create_data['profile']['skill_set']);
			}
		if(isset($create_data['job'])){
			
			$create_data['job']['date_expiry'] = date('Y-m-d', strtotime($create_data['job']['date_expiry']));
			
			if($create_data['job_id']==""){
			$create_data['job']['added_by'] = $this->session->userdata('user_id');
			
			$insert_status = $this->test->insertNewOpportunities($create_data['job']);
			}else{
			$create_data['job']['date_modified'] = date('Y-m-d H:i:s');
			$insert_status = $this->test->updateOpportunities($create_data['job'], $create_data['job_id']);
			}
			if($insert_status){
				//ho "Record Inserted";
                                        $user_id=$this->session->userdata('user_id');
                                        Utility::activity($user_id,$create_data['job_id'],"Job","Added Job");
					redirect("/test/jobpreview?job_id=$insert_status", 'refresh');
					exit;
			}
			else{
				echo "Insertion Failed";
			}
		}
		$job_data = array('job_title'=>'', 'job_description'=>'', 'skill_set'=>'', 'experience'=>'', 'job_location'=>'', 'job_role'=>'', 'candidate_qualification'=>'', 'employment_type'=>'', 'industry'=>'', 'salary'=>'', 'company_profile'=>'', 'date_expiry'=>'', 'employment_status'=>'');
		$this->layout->view('test/newopportunities', array('job_data'=>$job_data, 'job_id'=>"", 'skill_name' => $skill_name));
	}	

	public function jobpreview()
	{
		if($_GET){
		$get_array = $_GET;
		$job_id = $get_array['job_id'];
		$this->load->model('Test_model', 'test');
		$test_jobs = $this->test->read_job_data($job_id);
		$this->layout->view('test/jobpreview', array('job_data' => $test_jobs, 'job_id' => $job_id));
		}
		else{
		echo "Invalid URL";
		}
	}
	
	public function editopportunities()
	{
		if($_GET){
		$get_array = $_GET;
		$job_id = $get_array['job_id'];
		$this->load->model('Test_model', 'test');
		$user_id = $this->session->userdata('user_id');
		$skill_name = $this->test->read_skill_set();
		$test_jobs = $this->test->read_job_data($job_id, $user_id);
		if($test_jobs){
		$this->layout->view('test/newopportunities', array('job_data' => $test_jobs, 'job_id' => $job_id, 'skill_name'=>$skill_name) );
		}
		else{
		echo "You're Not allowed to edit this job!";
		}
		}
		else{
		echo "Invalid URL";
		}
	}
	
	public function viewjobs()
	{
		$this->load->model('Test_model', 'test');
		$view_jobs = $this->test->read_view_jobs(0,10);
		$this->layout->view('test/viewjobs', array('view_jobs' => $view_jobs));
	}
}
