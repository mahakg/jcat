<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Badges extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		parent::checkSignIn();
		$this->layout->setLayout('layouts/main');
	}

	public function index()
	{
                $this->load->model('badge_model', 'badges');
                $user_id = $this->session->userdata('user_id');
		$user_profile_data = $this->badges->getbadgeData($user_id);
               // print_R($user_profile_data);
               // $read_badge_name=$this->badges->readbadgeDir($user_id);
		$this->layout->view('badges/earned_badges', array('user_profile_data'=>$user_profile_data,'user_id'=>$user_id));
	}
}
