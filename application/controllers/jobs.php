<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobs extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		parent::checkSignIn();
		$this->layout->setLayout('layouts/main');
	}

	public function index()
	{
		$create_data =  $this->input->post();
                $this->load->model('test_model', 'test');
		$view_jobs = $this->test->read_view_jobs(0,10);
		$skill_name = $this->test->read_skill_set();
                $this->load->model('jobs_model', 'job');
		$submitted_jobs = $this->job->read_submitted_jobs(0,10);
                $website_activity=$this->job->site_activity();
               //print_r($website_activity);exit();
                //$content_name=$this->job->site_activity($website_activity);
             
		$user_type = $this->session->userdata('user_type');
		if($user_type == 'recruiter'){
		$this->layout->view('home/'. $user_type .'_homepage', array('view_jobs' => $view_jobs,'website_activity'=>$website_activity));
		}else if($user_type == 'candidate'){
		$this->layout->view('home/'. $user_type .'_homepage', array('submitted_jobs' => $submitted_jobs, 'skill_name' => $skill_name,'website_activity'=>$website_activity));
		}
	}
	
	public function submitted(){
                $this->load->model('jobs_model', 'job');
		$view_jobs = $this->job->read_submitted_jobs(0,10);
		$this->layout->view('jobs/submitted', array('view_jobs' => $view_jobs));
	}
	
	public function search(){
		$create_data =  $this->input->post();
		$this->load->model('Test_model', 'test');
		$skill_name = $this->test->read_skill_set();
		
		if(isset($create_data['searchjob'])){
			$this->load->model('Jobs_model', 'job');
			$create_data['searchjob']['skill_set'] = implode(",", $create_data['searchjob']['skill_set']);
			$insert_status = $this->job->insertSearchjob($create_data['searchjob']);
			if($insert_status){
				redirect("/jobs/searchresults", 'refresh');
					exit;
			}
			else{
				echo "Insertion Failed";
			}
		}
		$this->layout->view('jobs/search', array('skill_name' => $skill_name));
	}
	
	public function searchresults(){
		$this->load->model('Jobs_model', 'job');
		$jobs_list = $this->job->search_job_data($_POST);
		$this->layout->view('jobs/searchresults', array('jobs_list'=>$jobs_list));
	}
	
        public function applyjob(){
           $this->load->model('Jobs_model', 'job');
            $apply_data =  $this->input->post();
		if(isset($apply_data['job'])){
                        $apply_data['job']['date_slot'] = date("Y-m-d", strtotime($apply_data['job']['date_slot']));
			$this->load->model('jobs_model', 'jobs');
                        
			$insert_status = $this->jobs->applydata($apply_data['job']);
			
                            if($insert_status){
                                        $this->load->model('Jobs_model', 'job');
                                        $submitted_jobs = $this->job->read_submitted_jobs(0,10);
                                        $user_id=$this->session->userdata('user_id');
                                        $jobs_details = $this->job->search_job_data(array(), $apply_data['job']['job_id']);
                                        $this->layout->view('jobs/job_description',array('jobs_details'=>$jobs_details,'user_data'=>$user_id,'submitted_jobs'=>$submitted_jobs));
                                       
                            }
			else{
				echo "Insertion Failed";
			}
		}
        }
	
	public function job_description(){
                $this->load->model('Jobs_model', 'job');
		$submitted_jobs = $this->job->read_submitted_jobs(0,10);
                $user_id=$this->session->userdata('user_id');
                if(isset($_GET['job_id'])){
                 $job_id = $_GET['job_id'];
                $jobs_details = $this->job->search_job_data(array(), $job_id);
                $this->layout->view('jobs/job_description',array('jobs_details'=>$jobs_details,'user_data'=>$user_id,'submitted_jobs'=>$submitted_jobs));
            }
	}
}
