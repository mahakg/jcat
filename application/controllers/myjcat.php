<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Myjcat extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		parent::checkSignIn();
		$this->layout->setLayout('layouts/main');
	}

	public function index()
	{
		$create_data =  $this->input->post();
                $this->load->model('test_model', 'test');
		$view_jobs = $this->test->read_view_jobs(0,10);
		$skill_name = $this->test->read_skill_set();
                $this->load->model('jobs_model', 'job');
		$submitted_jobs = $this->job->read_submitted_jobs(0,10);
                $website_activity=$this->job->site_activity();
               //print_r($website_activity);exit();
                //$content_name=$this->job->site_activity($website_activity);
             
		$user_type = $this->session->userdata('user_type');
		if($user_type == 'recruiter'){
		$this->layout->view('home/'. $user_type .'_homepage', array('view_jobs' => $view_jobs,'website_activity'=>$website_activity));
		}else if($user_type == 'candidate'){
		$this->layout->view('home/'. $user_type .'_homepage', array('submitted_jobs' => $submitted_jobs, 'skill_name' => $skill_name,'website_activity'=>$website_activity));
		}
	}
	
	public function test()
	{
		$this->load->model('Test_model', 'test');
		$skill_name = $this->test->read_skill_details();
		$this->layout->view('myjcat/test', array('skills'=>$skill_name));
	}
        
        public function skill_desc()
	{
		if($_GET){
		$get_array = $_GET;
		$skill_id = $get_array['skill_id'];
		$this->load->model('Test_model', 'test');
		$skill_details = $this->test->read_skill_details($skill_id);
		$user_id=$this->session->userdata('user_id');
		$this->layout->view('/myjcat/skill_desc', array('skill_details'=>$skill_details));
		Utility::email("Nikhil", "nikhiltuteja12@gmail.com","nikhiltuteja@yahoo.com","Test Mail","Test Message",$raw="",$attachments="", $cc=array(),$bcc=array());
                Utility::activity($user_id,$skill_id,"Skills","Reading Test  Description");
                }
		else{
		echo "Invalid URL";
		}
	}
	
	public function starttest(){
		if($_GET){
		$get_array = $_GET;
		$skill_id = $get_array['skill_id'];
		$session_skill_id = $this->session->userdata('testskill_id');
		$this->session->set_userdata('testskill_id', $skill_id);
		
		if($skill_id!=$session_skill_id){
		$this->session->unset_userdata('questions_for_test');
		$this->session->unset_userdata('attempt_question');
		$this->session->unset_userdata('questions_for_test');
		$this->session->unset_userdata('difficulty_level');
		$this->session->unset_userdata('question_id');
		}
		//exit;
                $user_id=$this->session->userdata('user_id');
                Utility::activity($user_id,$skill_id,"test","Taking test");
		redirect(array('/myjcat/taketest'));
		}
		else{
		echo "Invalid URL";
		}
	
	}
	
	
	public function taketest(){
	$user_type = $this->session->userdata('user_type');
	if($user_type!="candidate"){
	echo "You are not Allowed to view this";
	exit;
	}
		$this->load->model('Myjcat_model', 'myjcat');
		$questions_for_test = $this->session->userdata('questions_for_test');
		if(!($questions_for_test)){
		$skill_id = $this->session->userdata('testskill_id');
		$questions_for_test = $this->myjcat->returnQuestions($skill_id,0,10);
               // print_R($questions_for_test);
                
		$this->session->set_userdata('questions_for_test',$questions_for_test);
		}
                
		$attempt_question = $this->session->userdata('attempt_question');
             //   print_r($attempt_question);
                if($attempt_question>=15){
                    $this->find_badge();
                }
		if(!($attempt_question)){
		//$attempt_question = $this->myjcat->returnQuestions(1,0,20);
		$this->session->set_userdata('difficulty_level',1);
		$this->session->set_userdata('attempt_question',1);
		$this->session->set_userdata('score',500);
		}
                
		//
		$difficulty_level = $this->session->userdata('difficulty_level');
		$question_id = $this->session->userdata('question_id');
		if($question_id==""){
		$question_id = $this->myjcat->returnRandomQuestion($questions_for_test, $difficulty_level);
		$this->session->set_userdata('question_id',$question_id);
		$score = $this->session->userdata('score');
		$this->myjcat->testLog("Question Review", $question_id, $score);
		}
               // $s=array();
                $s=$this->session->userdata('used_questions');
                //print_R($s);
		$question_details = $this->myjcat->getQuestion($question_id);
		$timer= $this->myjcat->returnTimer();
		$attempt_question = $this->session->userdata('attempt_question');
		$this->layout->view('myjcat/taketest', array('question_details'=>$question_details, 'timer'=>$timer, 'attempt_question'=>$attempt_question));
	}
	
	public function questionresult(){
		$this->load->model('Myjcat_model', 'myjcat');
		$answer = $_GET['ans'];
		$time = (int)str_replace(" secs", "", $_GET['time']);
		$correct = $this->session->userdata('correct');
		$question_id = $this->session->userdata('question_id');
		$this->session->set_userdata('correct',"");
		$this->session->set_userdata('question_id',"");
		$difficulty_level = $this->session->userdata('difficulty_level');
		$attempt_question = $this->session->userdata('attempt_question');
		$attempt_question= $attempt_question+1;
		$this->session->set_userdata('attempt_question', $attempt_question);
		$score = $this->session->userdata('score');
		
		if($answer==""){
		$score = $score - ($difficulty_level*5);
		$this->session->set_userdata('score',$score);
		$this->myjcat->testLog("Time Up", $question_id, $score);
		echo json_encode(array('status'=>'true', 'points'=>$time, 'text'=>'Oh Dear !! Time is Up . The correct answer is '. $correct . '.', 'mode'=>'error'));
		}else{
		if($answer === $correct){
                                if($difficulty_level!=5){
                                    $this->session->set_userdata('difficulty_level',$difficulty_level+1);
                                }else{
                                    $this->session->set_userdata('difficulty_level',$difficulty_level);
                                }
				$score = $score + ($time*$difficulty_level*10);
				$this->session->set_userdata('score',$score);
				$this->myjcat->testLog("Correct Answer", $question_id, $score);	
			echo json_encode(array('status'=>'true', 'points'=>$time, 'text'=>'Awesome Dear !! This is Correct Answer.  Click on Next Question to proceed.', 'mode'=>'success'));
		}else{          
                                if($difficulty_level!=1){
                                    $this->session->set_userdata('difficulty_level',$difficulty_level-1);
                                }else{
                                    $this->session->set_userdata('difficulty_level',$difficulty_level);
                                }
				$score = $score - ($time*$difficulty_level*5);
				$this->session->set_userdata('score',$score);
		$this->myjcat->testLog("Wrong Answer", $question_id, $score);	
		echo json_encode(array('status'=>'true', 'points'=>$time, 'text'=>'Oh Dear!! This is Wrong Answer. '. $correct . ' is correct answer.','mode'=>'error'));
		}
		}
		$badge = $this->badge_log($score);
	}	
	
	public function skills()
	{
		if($_GET){
		$get_array = $_GET;
		$skill_id = $get_array['skill_id'];
		$this->load->model('Test_model', 'test');
		$skill_name = $this->test->read_skill_set($skill_id);
		$this->layout->view('/myjcat/skill_desc?skill_id='. $skill_id , array('skill_id'=>$skill_id) );

		}
		else{
		echo "Invalid URL";
		}
	}
	
	public function badge_log($score){
		$this->load->model('Myjcat_model', 'test');
		if($score<400)
			return;
		if($score>=400 && $score<1000){
		$type = "Platinum";
		}
		elseif($score>=1000 && $score<1500){
		$type = "Silver";
		}
		elseif($score>=1500){
		$type = "Gold";
		}
		$this->test->badgeRegister($type, $score);
	}
        
        public function find_badge(){
            $this->load->model('Myjcat_model', 'test');
            $badge_data=$this->test->find_badge();
            $this->session->set_flashdata('msg', 'Congratulations!! You have earned'.$badge_data['badge_type'].' badge.'); //set session flash
             redirect('badges/index', 'refresh');
        }
			
} 
