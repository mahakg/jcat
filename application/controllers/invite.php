<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invite extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		parent::checkSignIn();
		$this->layout->setLayout('layouts/main');
	}

	public function index()
	{
                $user_id=$this->session->userdata('user_id');
                Utility::activity($user_id,"1","invite","Invite Friends");
		$this->layout->view('invite/invite_friends');
	}
        
        public function email()
	{
		$this->layout->view('invite/email');
	}
        
        function invite_email(){
            $this->load->model('user_model', 'user');
            $user_id=$this->session->userdata('user_id');
            $user_complete_profile = $this->user->completeProfile($user_id);
            
            $create_data =  $this->input->post();
            $sender_name=$create_data['invite']['name'];
            $sender_email=$user_complete_profile['email'];
            $to_mail=$create_data['invite']['to_email'];
            $subject=$create_data['invite']['subject'];
            Utility::email($sender_name, $sender_email,$to_mail,"Invitation to connect on Rate your skill",$subject,$raw="",$attachments="", $cc=array(),$bcc=array());
            $this->session->set_flashdata('msg', 'Mail  has been sent'); //set session flash
            redirect('invite/email', 'refresh');
        }
}
