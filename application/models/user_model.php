<?

class User_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	function insertUsers($add_new_user){
	
		$user_data_session = $this->session->userdata('userdata');
		$add_new_user = array_merge($user_data_session, $add_new_user);
		//$user_id = $this->session->userdata('user_id');
		$insert_user = $this->db->insert('jcat_users', $add_new_user);
		$id = $this->db->insert_id();

		$this->session->set_userdata('user_id',$id);
		
		$this->session->set_userdata('userdata',$add_new_user);
		
		if($insert_user){
			return true;
		}
		else{
			return false;
		}
	}
	
	function updateUsers($update_user){
		$user_data_session = $this->session->userdata('userdata');
		$user_id = $this->session->userdata('user_id');
		$update_user = array_merge($user_data_session, $update_user);
		$this->db->where('id',$user_id);
		$update_status = $this->db->update('jcat_users',$update_user);
		$this->session->set_userdata('userdata',$update_user);
		
		if($update_status){
			return true;
		}
		else{
			return false;
		}
	}
	
	function insertUserProfile($user_profile_data){
		$user_id = $this->session->userdata('user_id');
		$user_profile_data['user_id'] = $user_id;
		$insert_profile = $this->db->insert('jcat_users_profile', $user_profile_data);
		$profile_id = $this->db->insert_id();

		$this->session->set_userdata('user_profile_id',$profile_id);
		
		if($insert_profile){
			return true;
		}
		else{
			return false;
		}	
	}
	
	function updateUserProfile($update_user){
		$user_profile_id = $this->session->userdata('user_profile_id');
		$this->db->where('id',$user_profile_id);
		$update_status = $this->db->update('jcat_users_profile',$update_user);
		
		if($update_status){
			return true;
		}
		else{
			return false;
		}
	}
	
	function getProfileData($user_id){
		$user_profile_id = $this->session->userdata('user_profile_id');
		$profile_data = array('id'=>'',
				'user_id'=>'',
				'company_name'=>'',
				'company_size'=>'',
				'about_company'=>'',
				'domain'=>'',
				'company_location'=>'',
				'website_url'=>'',
				'current_employer'=>'',
				'current_salary'=>'',
				'designation'=>'',
				'skill_set'=>'',
                                'skill'=>'',
				'experience'=>'',
				'highest_qualification'=>'',
				'institute_name'=>'',
				'batch_year'=>'',
				'resume_plain_text'=>'',
				'remarks'=>'',
                                'tagline'=>'',
                                'about_me'=>''
			);
		$result=$this->db->query("select * from jcat_users_profile where user_id=?",  array($user_id));
		$row = $result->row();
		if($row){
			$profile_data['id'] = $row->id;
			$profile_data['user_id'] = $row->user_id;
			$profile_data['company_name'] = $row->company_name;
			$profile_data['company_size'] = $row->company_size;
			$profile_data['about_company']= $row->about_company;
			$profile_data['domain'] = $row->domain;
			$profile_data['company_location']= $row->company_location;
			$profile_data['website_url']= $row->website_url;
			$profile_data['current_employer']=$row->current_employer;
			$profile_data['current_salary']=$row->current_salary;
			$profile_data['designation']=$row->designation;
			$profile_data['skill_set']=$row->skill_set;
                        $profile_data['skill']=$row->skill;
			$profile_data['experience']=$row->experience;
			$profile_data['highest_qualification']=$row->highest_qualification;
			$profile_data['institute_name']=$row->institute_name;
			$profile_data['batch_year'] = $row->batch_year;
			$profile_data['resume_plain_text'] = $row->resume_plain_text;
			$profile_data['remarks'] = $row->remarks;
                        $profile_data['tagline'] = $row->tagline;
                        $profile_data['about_me'] = $row->about_me;
		}
		return $profile_data;
	}
	
	public function returnCompleteProfile(){
		$user_id = $this->session->userdata('user_id');
		$profile_data = array(
				'id'=>'',
				'name'=>'',
				'email'=>'',
				'username'=>'',
				'uid'=>'',
				'provider'=>'',
				'location'=>'',
				'secret'=>'',
				'token'=>'',
                                'profileurl'=>'',
                                'tagline'=>'',
				'summary'=>'',
				'profile_user_id'=>'',
				'user_id'=>'',
				'company_name'=>'',
				'company_size'=>'',
				'about_company'=>'',
				'domain'=>'',
				'company_location'=>'',
				'website_url'=>'',
				'current_employer'=>'',
				'current_salary'=>'',
				'designation'=>'',
				'skill_set'=>'',
                                'skill'=>'',
				'experience'=>'',
				'highest_qualification'=>'',
				'institute_name'=>'',
				'batch_year'=>'',
				'resume_plain_text'=>'',
				'remarks'=>'','tagline'=>'','about_me'=>''
			);	
	
		$this->db->select('u.id, u.name, u.email, u.username, u.uid, u.provider, u.location, u.secret, u.profileurl,u.token, u.summary, u.profileurl, u.user_type, u.date_added, p.id as profile_user_id, p.user_id, p.company_name, p.company_size, p.about_company, p.domain, p.company_location, p.website_url, p.current_employer, p.current_salary, p.designation, p.skill_set,p.skill, p.experience, p.highest_qualification, p.institute_name, p.batch_year, p.resume_plain_text, p.remarks,p.tagline,p.about_me, p.date_added ');
		
		$this->db->from('jcat_users u');
		$this->db->join('jcat_users_profile p', 'u.id = p.user_id'); // this joins the user table to topics
		$this->db->where('u.id',$user_id);
                $this->db->order_by('p.id',"desc");
                $this->db->limit(1);
		$query = $this->db->get();
		$row = $query->row();
		if($row){
		
			$profile_data['id'] = $row->id;
			$profile_data['name'] = $row->name;
			$profile_data['email']=$row->email;
			$profile_data['username']=$row->username;
			$profile_data['uid']=$row->uid;
			$profile_data['provider']=$row->provider;
			$profile_data['location']=$row->location;
			$profile_data['secret']=$row->secret;
			$profile_data['token']=$row->token;
                        $profile_data['profileurl']=$row->profileurl;
                        $profile_data['tagline']=$row->tagline;
			$profile_data['summary']=$row->summary;
			$profile_data['profile_id'] = $row->profile_user_id;
			$profile_data['user_id'] = $row->user_id;
			$profile_data['company_name'] = $row->company_name;
			$profile_data['company_size'] = $row->company_size;
			$profile_data['about_company']= $row->about_company;
			$profile_data['domain'] = $row->domain;
			$profile_data['company_location']= $row->company_location;
			$profile_data['website_url']= $row->website_url;
			$profile_data['current_employer']=$row->current_employer;
			$profile_data['current_salary']=$row->current_salary;
			$profile_data['designation']=$row->designation;
			$profile_data['skill_set']=$row->skill_set;
                        $profile_data['skill']=$row->skill;
			$profile_data['experience']=$row->experience;
			$profile_data['highest_qualification']=$row->highest_qualification;
			$profile_data['institute_name']=$row->institute_name;
			$profile_data['batch_year'] =$row->batch_year;
			$profile_data['resume_plain_text'] = $row->resume_plain_text;
			$profile_data['remarks'] = $row->remarks;
                        $profile_data['tagline'] = $row->tagline;$profile_data['about_me'] = $row->about_me;
		}
		return $profile_data;
	}
        
        public function completeProfile($user_id){
		$profile_data = array(
				'id'=>'',
				'name'=>'',
				'email'=>'',
				'username'=>'',
				'uid'=>'',
				'provider'=>'',
				'location'=>'',
				'secret'=>'',
				'token'=>'',
                                'tagline'=>'',
                                 'profileurl'=>'',
				'summary'=>'',
				'profile_user_id'=>'',
				'user_id'=>'',
				'company_name'=>'',
				'company_size'=>'',
				'about_company'=>'',
				'domain'=>'',
				'company_location'=>'',
				'website_url'=>'',
				'current_employer'=>'',
				'current_salary'=>'',
				'designation'=>'',
				'skill_set'=>'',
                                'skill'=>'',
				'experience'=>'',
				'highest_qualification'=>'',
				'institute_name'=>'',
				'batch_year'=>'',
				'resume_plain_text'=>'',
				'tagline'=>'','about_me'=>'','remarks'=>''
			);	
	
		$this->db->select('u.id, u.name, u.email, u.username, u.uid, u.profileurl,u.provider, u.location, u.secret, u.token, u.summary, u.profileurl, u.user_type, u.date_added, p.id as profile_user_id, p.user_id, p.company_name, p.company_size, p.about_company, p.domain, p.company_location, p.website_url, p.current_employer, p.current_salary, p.designation, p.skill_set, p.skill,p.experience, p.highest_qualification, p.tagline,p.about_me,p.institute_name, p.batch_year, p.resume_plain_text, p.remarks, p.date_added ');
		
		$this->db->from('jcat_users u');
		$this->db->join('jcat_users_profile p', 'u.id = p.user_id'); // this joins the user table to topics
		$this->db->where('u.id',$user_id);
		$query = $this->db->get();
		
		$row = $query->row();
		if($row){
		
			$profile_data['id'] = $row->id;
			$profile_data['name'] = $row->name;
			$profile_data['email']=$row->email;
			$profile_data['username']=$row->username;
			$profile_data['uid']=$row->uid;
			$profile_data['provider']=$row->provider;
			$profile_data['location']=$row->location;
			$profile_data['secret']=$row->secret;
			$profile_data['token']=$row->token;
                        $profile_data['profileurl']=$row->profileurl;
                        $profile_data['tagline']=$row->tagline;
			$profile_data['summary']=$row->summary;
			$profile_data['profile_id'] = $row->profile_user_id;
			$profile_data['user_id'] = $row->user_id;
			$profile_data['company_name'] = $row->company_name;
			$profile_data['company_size'] = $row->company_size;
			$profile_data['about_company']= $row->about_company;
			$profile_data['domain'] = $row->domain;
			$profile_data['company_location']= $row->company_location;
			$profile_data['website_url']= $row->website_url;
			$profile_data['current_employer']=$row->current_employer;
			$profile_data['current_salary']=$row->current_salary;
			$profile_data['designation']=$row->designation;
			$profile_data['skill_set']=$row->skill_set;
                        $profile_data['skill']=$row->skill;
			$profile_data['experience']=$row->experience;
			$profile_data['highest_qualification']=$row->highest_qualification;
			$profile_data['institute_name']=$row->institute_name;
			$profile_data['batch_year'] =$row->batch_year;
			$profile_data['resume_plain_text'] = $row->resume_plain_text;
			$profile_data['remarks'] = $row->remarks;
                        $profile_data['tagline'] = $row->tagline;
                        $profile_data['about_me'] = $row->about_me;
		}
		return $profile_data;
	}
        
        function getUserId($username){
                $profile_data = array('id'=>'');
                $result=$this->db->query("select id from jcat_users where username=?",  array($username));
		$row = $result->row();
		if($row){
			$profile_data['id'] = $row->id;
                }
                return $profile_data;
        }
}
?>
