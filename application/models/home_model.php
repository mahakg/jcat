<?

class User_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	function returnCompleteProfile(){
			$profile_data = array(
				'id'=>'',
				'name'=>'',
				'email'=>'',
				'username'=>'',
				'uid'=>'',
				'provider'=>'',
				'location'=>'',
				'secret'=>'',
				'token'=>'',
				'summary'=>'',
				'profile_user_id'=>'',
				'user_id'=>'',
				'company_name'=>'',
				'company_size'=>'',
				'about_company'=>'',
				'domain'=>'',
				'company_location'=>'',
				'website_url'=>'',
				'current_employer'=>'',
				'current_salary'=>'',
				'designation'=>'',
				'skill_set'=>'',
				'experience'=>'',
				'highest_qualification'=>'',
				'institute_name'=>'',
				'batch_year'=>'',
				'resume_plain_text'=>'',
				'remarks'=>''
			);	
			$result=$this->db->query('SELECT * FROM jcat_tests');
			$row = $result->row();
		if($row){
			$profile_data['id'] = $row->id;
			$profile_data['user_id'] = $row->user_id;
			$profile_data['company_name'] = $row->company_name;
			$profile_data['company_size'] = $row->company_size;
			$profile_data['about_company']= $row->about_company;
			$profile_data['domain'] = $row->domain;
			$profile_data['company_location']= $row->company_location;
			$profile_data['website_url']= $row->website_url;
			$profile_data['current_employer']=$row->current_employer;
			$profile_data['current_salary']=$row->current_salary;
			$profile_data['designation']=$row->designation;
			$profile_data['skill_set']=$row->skill_set;
			$profile_data['experience']=$row->experience;
			$profile_data['highest_qualification']=$row->highest_qualification;
			$profile_data['institute_name']=$row->institute_name;
			$profile_data['batch_year'] = $row->batch_year;
			$profile_data['resume_plain_text'] = $row->resume_plain_text;
			$profile_data['remarks'] = $row->remarks;
		}
		return $profile_data;
	}
	
	
	}
?>
