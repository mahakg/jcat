<?

class Auth_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
    
	public function saveData($providername,$token,$user)
	{
		$usertoken= $token->access_token;
		$usersecret= @$token->secret;

		$uid=$user['uid'];

		$nickname= array_key_exists('nickname',$user)?$user['nickname']:$uid;
		$name =array_key_exists('name',$user)? $user['name']:null;
		$location= array_key_exists('location',$user)?$user['location']:null;
		$description= array_key_exists('description',$user)?$user['description']:null;
		$profileimage=array_key_exists('image',$user)? $user['image']:null;
		$email=array_key_exists('email',$user)? $user['email']:'';

		$userobj= array('username'=>$nickname,
		    'uid'=>$uid,
		    'name'=>$name,
		    'email'=>$email,
		    'location'=>$location,
		    'token'=>$usertoken,
		    'secret'=>$usersecret,
		    'provider'=>$providername,
		    'summary'=>$description,
		    'profileurl'=>$profileimage,
		);
		
		$this->load->helper('url');
		//$this->load->library('session');
		$this->session->set_userdata('userdata',$userobj);

		$result=$this->db->query("select * from jcat_users where uid=? and provider=?",  array($uid,$providername));

		$id=0;
		if($result->row())
		{
		    $this->db->where('id',$result->row()->id);
		    $this->db->update('jcat_users',$userobj);
		    $id= $result->row()->id;
		    $this->session->set_userdata('user_id',$id);
		    $this->session->set_userdata('login','true');
		    $this->session->set_userdata('user_type',$result->row()->user_type);
		    $last_url = $this->session->userdata('last_url');
		    if($last_url)
		    	redirect($last_url);
		    else
		    	redirect('/home/index','refresh');
		}
		else{
			redirect('/users/profile','refresh');
		}
		
	}
	
	public function checkLogin($redirect=false)
	{
		$login = $this->session->userdata('login');
		if(!$login)
		{
			$this->load->helper('url');
			$this->session->set_userdata('last_url',current_url());
			redirect('/auth/login');
			exit;
		}
		return $login;
	}
}

?>
