<?

class Myjcat_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	function returnQuestions($skill_id, $limit, $offset){
	$questions = array();
	for($i=1; $i<=5; $i++){
	$this->db->select('question_id');
	$query = $this->db->get_where('jcat_questions', array('skill_id' => $skill_id, 'difficulty_level'=>$i), $offset, $limit);
       echo $this->db->last_query();
	//$sql = "Select question_id from jcat_questions where skill_id='$skill_id' and difficulty_level=1 order by rand() limit 100";
	foreach ($query->result() as $row)
	{
		$questions[$i][] = $row->question_id;
	}
	
	}
	 return ($questions);
	}
	
	function returnRandomQuestion($questions_for_test, $difficulty_level){
		$question_id = array_rand($questions_for_test[$difficulty_level], 1);
                $used_questions = $this->session->userdata('used_questions');              
                if(!is_array($used_questions))
                $used_questions = array();
                $used_questions[] =$questions_for_test[$difficulty_level][$question_id] ;
                $this->session->set_userdata('used_questions',$used_questions);
		return $questions_for_test[$difficulty_level][$question_id];
	}
	function getQuestion($question_id){
		$question_details= array();
		$query = $this->db->get_where('jcat_questions', array('question_id' => $question_id))->row();
		if($query){
			$question_details['question'] = $query->question;
			$question_details['optiona'] = $query->optiona;
			$question_details['optionb'] = $query->optionb;
			$question_details['optionc'] = $query->optionc;
			$question_details['optiond'] = $query->optiond;
			$question_details['optione'] = $query->optione;
			$question_details['explanation'] = $query->explanation;
			//$used_questions = $this->session->userdata('used_questions');
			$correct = $query->correctanswer;
			$this->session->set_userdata("correct", $correct);
		}
		return $question_details;
	}
	
	function testLog($activity_name, $question_id, $score)
	{
		$log['user_id'] = $this->session->userdata('user_id');
		$log['question_id'] = $question_id;
		$log['activity_name'] = $activity_name;
		$log['score'] = $score;
		$log['skill_id'] = $this->session->userdata('testskill_id');
		$insert_question_log = $this->db->insert('jcat_test_log', $log);
		if($insert_question_log){
			return true;
		}
		else{
			return false;
		}
	}
	
	function returnTimer(){
		$difficulty_level = $this->session->userdata('difficulty_level');
		switch($difficulty_level){
		case 1:
			$timer = 15;
			break;
		case 2:
			$timer = 20;
			break;
		case 3:
			$timer = 30;
			break;		
		case 4:
			$timer = 30;
			break;	
		case 5:
			$timer = 45;
			break;	
		}
		
		return $timer;
	}
	
	function badgeRegister($type, $score){
		$badge['user_id'] = $this->session->userdata('user_id');
		$badge['skill_id'] = $this->session->userdata('testskill_id');
		$badge['badge_type'] = $type;
		$badge['score'] = $score;
		$insert_query = $this->db->insert_string('jcat_badges',$badge);
		$insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
		$create_badge = $this->db->query($insert_query);  
		
		if($create_badge){
			return true;
		}
		else{
			return false;
		}
	}
        
        function find_badge(){
		$badge['user_id'] = $this->session->userdata('user_id');
		$badge['skill_id'] = $this->session->userdata('testskill_id');
                $badge_data=array();
               $result=$this->db->query("SELECT `badge_type` FROM `jcat_badges` WHERE `user_id`='".$badge['user_id']."' and `skill_id`='".$badge['skill_id']."' order by id desc limit 1");
               $row = $result->row();
              // echo  $query=$this->db->last_query();
		if($row){
			$badge_data['badge_type'] = $row->badge_type;
                }
                return $badge_data;
        }
}	
?>
