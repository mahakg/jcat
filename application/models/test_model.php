<?

class Test_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function insertQuestions($add_new_question){

		 $insert_question = $this->db->insert('jcat_questions',$add_new_question);
		 
		 if($insert_question){
                     return true;
                     
		 }
		 else{
		 	return false;
		 }
	}
	function insertTests($add_new_test){

		 $insert_test = $this->db->insert('jcat_tests',$add_new_test);
		 
		 if($insert_test){
		 	return true;
		 }
		 else{
		 	return false;
		 }
	}
	function read_test_records($limit, $offset){

        $query = $this->db->query('SELECT * FROM jcat_tests');
        $skills = $this->read_skill_set();
        print_r($skills);
		$test_records = array();
		$i=0;
		foreach ($query->result() as $row)
		{
			$test_records[$i]['id'] =  $row->id;
			$test_records[$i]['candidate_name'] = $row->candidate_name;
			$test_records[$i]['candidate_email'] = $row->candidate_email;
			$test_records[$i]['skill_set'] = $row->skill_set;
			if ($row->conduct_status=="y") {
				$status = "conducted";
			} elseif ($row->conduct_status=="n") {
				$status = "not conducted";
			} else {
				$conduct_status = "disqualified";
			}
			
			$test_records[$i]['conduct_status'] = $status;
			
			$i++;
		}

		//echo 'Total Results: ' . $query->num_rows(); 

        return $test_records;

    }
	function insertNewOpportunities($add_new_opportunity){

		 $insert_opportunity = $this->db->insert('jcat_newopportunities',$add_new_opportunity);
		 if($insert_opportunity){
		 
		 	return $this->db->insert_id();
		 }
		 else{
		 	return false;
		 }
	}

	function read_job_data ($id="", $user_id=""){
	if($id!=""){
	$primary_condition = " id='$id'";
	}
	else{
	$primary_condition =1;
	}
	
	if($user_id!=""){
	$added_by_condition = " added_by ='$user_id'";
	}
	else{
	$added_by_condition =1;
	}
	$result = $this->db->query("SELECT * FROM jcat_newopportunities where $primary_condition and $added_by_condition");
	$row = $result->row();
	$job_data = array();
			if($row){
				$job_data['job_title'] = $row->job_title;
				$job_data['job_description'] = $row->job_description;
				$job_data['skill_set'] = $row->key_skills;
				$job_data['experience'] = $row->experience;
				$job_data['job_location'] = $row->job_location;
				$job_data['job_role'] = $row->job_role;
				$job_data['candidate_qualification'] = $row->candidate_qualification;
				$job_data['employment_type'] = $row->employment_type;
				$job_data['industry'] = $row->industry;
				$job_data['salary'] = $row->salary;
				$job_data['company_profile'] = $row->company_profile;
				$job_data['date_expiry'] = $row->date_expiry;
				$job_data['employment_status'] = $row->employment_status;
				}
			if(!empty($job_data))	
				return $job_data;
			else
				return false;
				
	}
	
	function updateOpportunities($update_new_opportunity, $job_id){
		 $this->db->where('id', $job_id);
		 $update_opportunity = $this->db->update('jcat_newopportunities',$update_new_opportunity);
		 if($update_opportunity){
		 	return $job_id;
		 }
		 else{
		 	return false;
		 }
	}
	
	function read_view_jobs($limit, $offset){
	     $query = $this->db->query('SELECT * FROM jcat_newopportunities');
		 $i=0;
		 $skills = $this->read_skill_set();
		foreach ($query->result() as $row)
		{
			$view_jobs[$i]['job_title'] =  $row->job_title;
			$view_jobs[$i]['key_skills'] = $row->key_skills;
			$view_jobs[$i]['salary'] = $row->salary;
			$view_jobs[$i]['id'] = $row->id;
			$view_jobs[$i]['status'] = $row->status;
			
			$i++;
		}
		return $view_jobs;
	}
	
	function read_skill_set($skill_id="") {
		$query=$this->db->get('jcat_skills');
		if ($query->num_rows > 0) {
			foreach ($query->result_array() as $row) {
		    $skill_name[$row['skill_id']] = $row['skill_name'];
		}
		return $skill_name;
		}
		else{
		return false;
		}
	}
	
	function read_skill_details($skill_id="") {
	
		if($skill_id!=""){
			$this->db->where('skill_id', $skill_id);
		}

	$query=$this->db->get('jcat_skills');
	if ($query->num_rows > 0) {
		$i=0;
		foreach($query->result_array() as $row){
		$skill_name[$i]['name'] = $row['skill_name'];
		$skill_name[$i]['id'] = $row['skill_id'];
		$skill_name[$i]['img'] = $row['skill_img'];
		$skill_name[$i]['desc'] = $row['skill_desc'];
		$i++;
		}
		return $skill_name;
	}
	else{
	return false;
	}
	}
}

?>
