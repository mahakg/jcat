<?
class Jobs_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function insertSearchjob($add_new_search){

		 $insert_searchjob = $this->db->insert('jcat_search',$add_new_search);
		 if($insert_searchjob){
		 	return true;
		 }
		 else{
		 	return false;
		 }
	}
	
	function search_job_data($job_filters, $job_id=""){
	//Now you need the dynamic values of $skill_set and etc here, ok ?? so okyou would need to pass the post data in this function, ok ?
	if($job_id!=""){
		$job_condition = "id='$job_id'";
	}
	else{
		$job_condition = 1;
	}
	if(isset($job_filters['skill_id']) && $job_filters['skill_id']!="" ){
		$skill_condition = "key_skills='" . $job_filters['skill_id'] . "'";
		}
		else{
		$skill_condition = 1;
		}
		if(isset($job_filters['salary']) && $job_filters['salary']!=""){
		$salary_condition = "salary='" . $job_filters['salary'] . "'";
		}
		else{
		$salary_condition = 1;
		}
		if(isset($job_filters['experience']) && $job_filters['experience']!=""){
		$experience_condition = "experience='" . $job_filters['experience'] . "'";
		}
		else{
		$experience_condition = 1;
		}
		
		if(isset($job_filters['job_location']) && $job_filters['job_location']!=""){
		$location_condition = "job_location='" . $job_filters['experience'] . "'";
		}
		else{
		$location_condition = 1;
		}
	$sql = "SELECT * FROM jcat_newopportunities where $job_condition and $skill_condition and $experience_condition and $salary_condition and $location_condition";
	$result = $this->db->query($sql);
	$result = $result->result();
	$job_data = array();
	$i=0;
			foreach($result as $row){
				$job_data[$i]['id'] = $row->id;
				$job_data[$i]['job_title'] = $row->job_title;
				$job_data[$i]['job_description'] = $row->job_description;
				$job_data[$i]['key_skills'] = $row->key_skills;
				$job_data[$i]['experience'] = $row->experience;
				$job_data[$i]['job_location'] = $row->job_location;
				$job_data[$i]['job_role'] = $row->job_role;
				$job_data[$i]['candidate_qualification'] = $row->candidate_qualification;
				$job_data[$i]['employment_type'] = $row->employment_type;
				$job_data[$i]['industry'] = $row->industry;
				$job_data[$i]['salary'] = $row->salary;
				$job_data[$i]['company_profile'] = $row->company_profile;
				$job_data[$i]['date_expiry'] = $row->date_expiry;
				$job_data[$i]['employment_status'] = $row->employment_status;
				$i++;
				}
			if(!empty($job_data))	
				return $job_data;
			else
				return false;
				
	}
        
        function applydata($job_application_data){
      		 $insert_question = $this->db->insert('jcat_job_applications',$job_application_data);
		 if($insert_question){
		 	return true;
		 }
		 else{
		 	return false;
		 }
	}
        
        function read_submitted_jobs($limit, $offset){
             $user_id=$this->session->userdata('user_id');
            // print_r($user_id);
             $job_id = $this->read_job_id();
           //  print_r($job_id);
	     $query = $this->db->query('SELECT * FROM jcat_job_applications WHERE `user_id`="'.$user_id.'"');
		 $i=0;
		$view_jobs = array(); 
		foreach ($query->result() as $row)
		{
			$view_jobs[$i]['application_status'] =  $row->application_status;
			$view_jobs[$i]['job_id'] = $job_id[$row->job_id];
			$view_jobs[$i]['id'] = $row->application_id;
			$view_jobs[$i]['date_slot'] = $row->date_slot;
			
			$i++;
		}
		return $view_jobs;
	}
        
        function read_job_id($job_id = array()) {
	//$query = $this->db->query("SELECT * FROM jcat_skills");

         $query=$this->db->get('jcat_newopportunities');
            if ($query->num_rows > 0) {
                 foreach ($query->result_array() as $row) {
                    $job_id[$row['id']] = $row['company_profile'];
                    }
                return $job_id;
            }
            else{
                return false;
            }
	}
        
       function site_activity(){
		$query=$this->db->query('SELECT j.name,j.profileurl,u.`content_id`,u.`activity_type`,u.`module_name` FROM `jcat_activity` u,jcat_users j WHERE u.user_id=j.id Group by j.name order by u.id desc limit 10');
		 $i=0;
                 $website_data=array();
		 $skills = $this->read_skill_set();
		foreach ($query->result() as $row)
		{
			$website_data[$i]['name'] =  $row->name;
                        $website_data[$i]['profileurl'] =  $row->profileurl;
			$website_data[$i]['content_id'] = $skills[$row->content_id];
			$website_data[$i]['activity_type'] = $row->activity_type;
			$website_data[$i]['module_name'] = $row->module_name;
			$i++;
		}
		return $website_data;
		
       }

        function read_skill_set($skill_id="") {
		$query=$this->db->get('jcat_skills');
		if ($query->num_rows > 0) {
			foreach ($query->result_array() as $row) {
		    $skill_name[$row['skill_id']] = $row['skill_name'];
		}
		return $skill_name;
		}
		else{
		return false;
		}
	}
        
	}

?>
