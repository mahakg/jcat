<?

class Utility
{
	var $CI;       
	function Utility()
	{
		$CI =& get_instance();
		$CI->load->database();
	}


	public static function email($from_name, $from_email,$to_email,$subject,$message,$raw="",$attachments="", $cc=array(),$bcc=array()) 
	{
		if ($raw != "") {
		$message2 = $message;
     	}
     	else {
     		$message2 = "<span style='font-size:14px;font-family:arial,liberation sans;'>$message<span>";
     	}
		$cc_addrs = $bcc_addrs ="";
		if(is_array($cc) && sizeof($cc) > 0)
		{
			$cc_addrs = implode(',', $cc);
		}

		if(is_array($bcc) && sizeof($bcc) > 0)
		{
			$bcc_addrs = implode(',', $bcc);
		}
		
		$mail = array();
		$mail['sendername'] = $from_name;
		$mail['senderemail'] = $from_email;
		$mail['recpemail'] = $to_email;
		$mail['subject'] = $subject;
		$mail['message'] = $message2;
		$mail['createdon'] = date("Y-m-d H:i:s");
		$mail['attachments'] = $attachments;
		$mail['cc'] = $cc_addrs;
		$mail['bcc'] = $bcc_addrs;
		global $CI;		
	        //$CI =   &get_instance();
	       	$mail_queue = $CI->db->insert('jcat_queue', $mail);
		if($mail_queue){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static function log_stats($contentId, $moduleName, $userId)
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP'])){
			$ip=$_SERVER['HTTP_CLIENT_IP'];
			//Is it a proxy address
		}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		  	$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
		  	$ip=$_SERVER['REMOTE_ADDR'];
		}
		
		//The value of $ip at this point would look something like: "192.0.34.166"
		$locArr = array();	

		if(function_exists('geoip_record_by_name')) 
		{ 
			$locArr = @geoip_record_by_name($ip);
			$city = @$locArr['city'];
			$country = @$locArr['country_name'];
	 	} 
		else { 
			$city = 'New Delhi'; 
			$country = 'India'; 
		}

		
		if(!empty($_SERVER['HTTP_USER_AGENT']))
		{
			$browser = $_SERVER['HTTP_USER_AGENT'];
		}
		
		$jcat_log['user_id'] = $userId; 
		$jcat_log['content_id'] = $contentId;
		$jcat_log['date_added'] = date("Y-m-d H:i:s");
		$jcat_log['week_num'] = date("W");
		$jcat_log['month_num'] = date('m');
		$jcat_log['ipaddress'] = $ip; 
		$jcat_log['browser'] = $browser; 
		$jcat_log['country'] = $country; 
		$jcat_log['city'] = $city; 
		$jcat_log['module_name'] = $moduleName;
		global $CI;
		$log_status = $CI->db->insert('jcat_log_table', $jcat_log);
		if($log_status){
			return true;
		}
		else{
			return false;
		}
	}
	// Call this function using Utility::activity($user_id, $content_id, $module_name, $activity_type); and pass respectiv values in dynamic field,
	public static function activity($user_id, $content_id, $module_name, $activity_type, $comment=""){
			$activity_data['content_id'] = $content_id;
			$activity_data['user_id'] = $user_id;
			$activity_data['module_name'] = $module_name;
			$activity_data['activity_type'] = $activity_type;
			$activity_data['activity_date'] =  date("Y-m-d H:i:s");
			$activity_data['activity_data'] = $comment;
			
			global $CI;
			$activity_status = $CI->db->insert('jcat_activity', $activity_data);
			if($activity_status){
				return true;
			}
			else{
				return false;
			}
	}
}
?>
