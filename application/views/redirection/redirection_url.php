<div class="portlet box blue">
							<div class="portlet-title">
								<h4><i class="icon-edit"></i>Code Challenge Hiring Process MidasORO.com</h4>
								<div class="tools">
									<a class="collapse" href="javascript:;"></a>
									<a class="reload" href="javascript:;"></a>
									<a class="remove" href="javascript:;"></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									
									<div class="btn-group pull-right">
										<button data-toggle="dropdown" class="btn dropdown-toggle">Tools <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">Print</a></li>
											<li><a href="#">Save as PDF</a></li>
											<li><a href="#">Export to Excel</a></li>
										</ul>
									</div>
								</div>
                                                            <div class="row-fluid">
                                                                              <h3 style="padding-bottom: 15px;">Description</h3>                                                                                                                                    
										<div class="well">
										<p>This Entire Test application are being developed in Codeigniter Framework.Following are the naming convention for the model and controller:-
                                                                                <li>Project(domain) Name:- Educonnects.com </li>
                                                                                <li>Subdomain:- Beta </li>
                                                                                <li>Controller Name:- redirection</li>
                                                                                <li>Model Name:- redirection_model </li>
                                                                                <li> View name:- redirection_url</li>
                                                                                <li> Database table:- dynamic_url </li>
                                                                                </p>	
                                                                                </div>
                                                                            </div>
                                                            <h3>   Database table dynamic_url structure and data</h3>
								<table id="sample_editable_1" class="table table-striped table-hover table-bordered dataTable" aria-describedby="sample_editable_1_info">
									<thead>
										<tr role="row">
										<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 100px;" aria-sort="ascending" aria-label="Username: activate to sort column descending">tagid</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 238px;" aria-label="Full Name: activate to sort column ascending">Url</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 116px;" aria-label="Points: activate to sort column ascending">Userid</th>
										</tr>
									</thead>
									
								<tbody role="alert" aria-live="polite" aria-relevant="all"><tr class="odd">
                                                                    <tr>
                                                                    <td>453rgf</td>
                                                                    <td>https://www.facebook.com</td>
                                                                    <td>admin</td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>5490it</td>
                                                                    <td>www.youtube.com</td>
                                                                    <td>nikhil</td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>uiu091</td>
                                                                    <td>http://www.techradar.com/news</td>
                                                                    <td>raj</td>
                                                                    </tr>
                                                                </tbody>
                                                                </table>
                                                               <div class="row-fluid">
                                                                              <h3 style="padding-bottom: 15px;">How to Test Redirection Application</h3>                                                                                                                                    
										<div class="well">
										<p>I have Passed unique id(tagid) into the url. <ul>Lets have some example on it.:-
                                                                                <li><a href="http://educonnects.com/beta/index.php/redirection/redirection_url?tagid=453rgf" class="btn blue">http://educonnects.com/beta/index.php/redirection/redirection_url?tagid=453rgf</a></li>
                                                                                <li><a href="http://educonnects.com/beta/index.php/redirection/redirection_url?tagid=5490it" class="btn blue">http://educonnects.com/beta/index.php/redirection/redirection_url?tagid=5490it</a></li>
                                                                                <li><a href="http://educonnects.com/beta/index.php/redirection/redirection_url?tagid=uiu091" class="btn blue">http://educonnects.com/beta/index.php/redirection/redirection_url?tagid=uiu091</a></li>
                                                                                </ul>  </p>	
                                                                                </div>
                                                                            </div>
                                                        </div>
							</div>
						<!-- Javascript Code -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["trackPageView"]);
  _paq.push(["enableLinkTracking"]);

  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://percentage.biz/analytics/";
    _paq.push(["setTrackerUrl", u+"piwik.php"]);
    _paq.push(["setSiteId", "3"]);
    var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
    g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Javascript Code -->
