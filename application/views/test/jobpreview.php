                           <div class="portlet box blue">
                              <div class="portlet-title">
                                 <h4><i class="icon-reorder"></i>Preview Your Job Listing</h4>
                                 <div class="tools">
                                    <a class="collapse" href="javascript:;"></a>
                                    <a class="config" data-toggle="modal" href="#portlet-config"></a>
                                    <a class="reload" href="javascript:;"></a>
                                    <a class="remove" href="javascript:;"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <div class="form-horizontal form-view">
                                    <h3> Info Job </h3>
                                    <h3 class="form-section">Job Details</h3>
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label for="title" class="control-label">Job Title:</label>
                                             <div class="controls">
                                                <span class="text bold"><?=$job_data['job_title']; ?></span>
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label for="lastName" class="control-label">Job Description:</label>
                                             <div class="controls">
                                                <span class="text"><?=$job_data['job_description']; ?></span>
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Key Skills Required:</label>
                                             <div class="controls">
                                                <span class="text"><?=$job_data['skill_set']; ?></span> 
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Experience:</label>
                                             <div class="controls">
                                                <span class="text"><?=$job_data['experience']; ?></span>
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <!--/row-->        
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Job Location:</label>
                                             <div class="controls">
                                                <span class="text"><?=$job_data['job_location']; ?></span>
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Job Role:</label>
                                             <div class="controls">                                                
                                                <span class="text"><?=$job_data['job_role']; ?></span>
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <h3 class="form-section"></h3>
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Candidate Qualification:</label>
                                             <div class="controls">
                                                <span class="text"><?=$job_data['candidate_qualification']; ?></span>
                                             </div>
                                          </div>
                                       </div>
									   <!--/span-->
									   <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Employment Type:</label>
                                             <div class="controls">                                                
                                                <span class="text"><?=$job_data['employment_type']; ?></span>
                                             </div>
                                          </div>
                                       </div>
									   <!--/span-->
                                    </div>
									
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Industry:</label>
                                             <div class="controls">
                                                <span class="text"><?=$job_data['industry']; ?></span>
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="span6">
                                          <div class="control-group">
                                             <label class="control-label">Annual Salary:</label>
                                             <div class="controls">
                                                <span class="text"><?=$job_data['salary']; ?></span>
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <!--/row-->           
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Company Profile:</label>
                                             <div class="controls">
                                                <span class="text"><?=$job_data['company_profile']; ?></span>
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Expiry Date:</label>
                                             <div class="controls">
                                                <span class="text"><?=$job_data['date_expiry']; ?></span>
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
									   
                                    </div>
									<!--/row-->           
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Employment Status:</label>
                                             <div class="controls">
                                                <span class="text"><? if($job_data['employment_status']=='parttime') echo "Part Time"; else echo "Full Time"; ?></span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-actions">
									   <a href = "<?=site_url('/test/editopportunities?job_id='. $job_id );?>" class = "btn blue">
									   <i class="icon-pencil"></i> Edit
									   </a>
                                    </div>
                                 </div>
                                 <!-- END FORM-->  
                              </div>
                           </div>
                        
