<div class="portlet box blue">
							<div class="portlet-title">
								<h4><i class="icon-edit"></i>Jobs Posted By You</h4>
								<div class="tools">
									<a class="collapse" href="javascript:;"></a>
									<a class="config" data-toggle="modal" href="#portlet-config"></a>
									<a class="reload" href="javascript:;"></a>
									<a class="remove" href="javascript:;"></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									
									<div class="btn-group pull-right">
										<button data-toggle="dropdown" class="btn dropdown-toggle">Tools <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">Print</a></li>
											<li><a href="#">Save as PDF</a></li>
											<li><a href="#">Export to Excel</a></li>
										</ul>
									</div>
								</div>
								
								<div role="grid" class="dataTables_wrapper form-inline" id="sample_editable_1_wrapper"><div class="row-fluid"><div class="span6"><div id="sample_editable_1_length" class="dataTables_length"><label><select name="sample_editable_1_length" size="1" aria-controls="sample_editable_1" class="m-wrap xsmall"><option value="10" selected="selected">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> records per page</label></div></div><div class="span6">
								
								<div class="dataTables_filter" id="sample_editable_1_filter"><label>Search: <input type="text" aria-controls="sample_editable_1" class="m-wrap medium"></label></div>
								</div>
								
								<table id="sample_editable_1" class="table table-striped table-hover table-bordered dataTable" aria-describedby="sample_editable_1_info">
									<thead>
										<tr role="row">
										<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 100px;" aria-sort="ascending" aria-label="Username: activate to sort column descending">Job id</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 238px;" aria-label="Full Name: activate to sort column ascending">Job Title</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 116px;" aria-label="Points: activate to sort column ascending">Key Skills</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 116px;" aria-label="Points: activate to sort column ascending">Salary</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 167px;" aria-label="Notes: activate to sort column ascending">Status</th>
										</tr>
									</thead>
									
								<tbody role="alert" aria-live="polite" aria-relevant="all"><tr class="odd">
 
<?php foreach ($view_jobs as $row)
{ ;?> 
<tr>
<td><?php echo $row['id'];?></td>
<td><?php echo $row['job_title'];?></td>
<td><?php echo $row['key_skills'];?></td>
<td><?php echo $row['salary'];?></td>
<td><?php echo $row['status'];?></td>


</tr>
<?php } ?>
	
											
											
											
										</tbody></table><div class="row-fluid"><div class="span6"><div class="dataTables_info" id="sample_editable_1_info">Showing 1 to 10 of 10 entries</div></div><div class="span6"><div class="dataTables_paginate paging_bootstrap pagination"><ul><li class="prev disabled"><a href="#">← Previous</a></li><li class="active"><a href="#">1</a></li><li class="next disabled"><a href="#">Next → </a></li></ul></div></div></div></div>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>