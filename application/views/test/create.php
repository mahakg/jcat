<?php $alert=$this->session->flashdata('msg');
 if($alert){  
    echo '<script type=\'text/javascript\'>';  
    echo 'alert("'.$alert.'");';  
    echo '</script>';
}  
?>
<div class="tab-content">
<div id="tab_2" class="tab-pane active">
                           <div class="portlet box green">
                              <div class="portlet-title">
                                 <h4><i class="icon-reorder"></i>Create Test</h4>
                                 <div class="tools">
                                    <a class="collapse" href="javascript:;"></a>
                                    <a class="config" data-toggle="modal" href="#portlet-config"></a>
                                    <a class="reload" href="javascript:;"></a>
                                    <a class="remove" href="javascript:;"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form class="form-horizontal" action="<?=site_url('/test/create');?>" method="post">                         
                                    <h3 class="form-section">Add a Question</h3>
                                    <div class="row-fluid">
                                       <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Select a Skill</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="question[skill_id]">
	                                                <option value="1">PHP</option>
							<option value="2">Core Java</option>
                                                        <option value="3">HTML 5</option>
							<option value="4">C#</option>
                                                        <option value="5">Mysql</option>
							<option value="6">Java script</option>
                                                        <option value="7">Creative Writing</option>
							<option value="8">Basic Mathematics</option>
                                                        <option value="9">Jquery</option>
							<option value="10">HTML</option>
                                                        <option value="11">Linux</option>
                                                        <option value="12">CSS</option>
							<option value="13">VIM</option>
                                                        <option value="14">Excel</option>
							<option value="15">GIT</option>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                       <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Question</label>
                                             <div class="controls">
                                                <input type="text" class="m-wrap span12" name="question[question]" required name="some_field" aria-invalid="false">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Option 1</label>
                                             <div class="controls">
                                                <input type="text" class="m-wrap span12" name="question[optiona]" required name="some_field" aria-invalid="false"> 
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Option 2</label>
                                             <div class="controls">
                                                <input type="text" class="m-wrap span12" name="question[optionb]" required name="some_field" aria-invalid="false"> 
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <!--/row-->           
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Option 3</label>
                                             <div class="controls">
                                                <input type="text" class="m-wrap span12" name="question[optionc]" required name="some_field" aria-invalid="false"> 
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Option 4</label>
                                             <div class="controls">
                                                <input type="text" class="m-wrap span12" name="question[optiond]" required name="some_field" aria-invalid="false"> 
                                             </div>
                                          </div>
                                       </div>
                                     </div>
                                     <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label" >Option 5</label>
                                             <div class="controls">
                                                <input type="text" class="m-wrap span12" name="question[optione]" aria-invalid="false"> 
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <div class="controls">
                                             </div>
                                          </div>
                                       </div>
                                     </div>
                                    <div class="row-fluid"> 
                                    <div class="span12">
                                       <div class="control-group">
                                             <label class="control-label">Correct Answer</label>
                                             <div class="controls">                                                
                                                <label class="radio">
                                              <div id="uniform-undefined"><input type="radio" value="optiona" name="question[correctanswer]"></div>
                                                Option1
                                                </label>
                                              </div>  
                                              <div class="controls">
                                                <label class="radio">
                                                <div id="uniform-undefined"><input type="radio" checked="" value="optionb" name="question[correctanswer]" ></div>
                                                Option2
                                                </label>
                                                </div>
                                              <div class="controls">  
                                                <label class="radio">
						<div id="uniform-undefined"><input type="radio" checked="" value="optionc" name="question[correctanswer]" ></div>
                                                Option3
                                                </label>  
                                                </div>
                                               <div class="controls">
                                                <label class="radio">
                                                <div id="uniform-undefined">
												<input type="radio" checked="" value="optiond" name="question[correctanswer]" >
												</div>
                                                Option4
                                                </label>
                                                </div>
						<div class="controls">
                                                <label class="radio">
                                                <div id="uniform-undefined"><input type="radio" checked="" value="optione" name="question[correctanswer]" ></div>
                                                Option5
                                                </label>  
                                                </div>
                                                
                                             </div>
                                          </div>
                                        </div>  
                                       <!--/span-->
                                    
                                    <!--/row-->
                                    <div class="row-fluid">
                                       <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Explanation</label>
                                             <div class="controls">
                                                <input type="text" class="m-wrap span12" name="question[explanation]">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                       <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Difficulty Level</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="question[difficulty_level]">
	                                                <option value="1">1</option>
	                                                <option value="2">2</option>
	                                                <option value="3">3</option>
	                                                <option value="4">4</option>
	                                                <option value="5">5</option>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-actions">
                                       <button class="btn blue" type="submit"><i class="icon-ok"></i> Save</button>
                                       <button class="btn" type="button">Cancel</button>
                                    </div>
                                 </form>
                                 <!-- END FORM-->                
                              </div>
                           </div>
                        </div>
</div>
