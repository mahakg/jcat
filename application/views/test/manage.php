<?php $alert=$this->session->flashdata('msg');
 if($alert){  
    echo '<script type=\'text/javascript\'>';  
    echo 'alert("'.$alert.'");';  
    echo '</script>';
}  
?>
<div class="portlet box green">
                              <div class="portlet-title">
                                 <h4><i class="icon-reorder"></i>Manage Test</h4>
                                 <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                    <a href="javascript:;" class="reload"></a>
                                    <a href="javascript:;" class="remove"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form class="form-horizontal" action="<?=site_url('/test/manage');?>" method="post">                         
                                    <h3 class="form-section">Add a Profile</h3>
                                    <div class="row-fluid">
                                       <div class="span12 ">
                                          <div class="control-group">
                              <label class="control-label">Select Role</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="profile[role]">
                                 <span class="help-inline">eg. Software Developer</span>
                              </div>
                           </div>
                                       </div>
                                    </div>
                                    
                           <div class="control-group">
										<label class="control-label">Select Skill Set</label>
										<div class="controls">
											  
											
											<select tabindex="1" data-placeholder="Choose a Category" multiple="multiple" class="span6 m-wrap" name="profile[skill_set][]">
<?php 
    foreach($skill_name as $id=>$name)
	
{
?>
    <option value="<?=$id;?>"> <?=$name;?> </option>
	<?
}
?>
</select>
										<p class="help-block">Select Multiple Skills by pressing Ctrl key.</p>
										</div>
										
									</div>
							 <div>		
						    <input type="hidden" name="profile[hash_id]" value="<?=uniqid(); ?>">
						    
						    </div>
                                    <!--<div class="row-fluid">
                                       <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Difficulty Level</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="profile[difficulty]">
	                                                <option value="1">1</option>
	                                                <option value="2">2</option>
	                                                <option value="3">3</option>
	                                                <option value="4">4</option>
	                                                <option value="5">5</option>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
									-->
									<div class="control-group">
													<label class="control-label">Candidate Name<span class="required">*</span></label>
													<div class="controls">
														<input type="text" name="profile[candidate_name]" class="span6 m-wrap" required  aria-invalid="false">
														<span class="help-inline">Provide your username</span>
													</div>
									</div>
                                    <div class="control-group">
													<label class="control-label">Email<span class="required">*</span></label>
													<div class="controls">
														<input type="email" class="span6 m-wrap" required name="profile[candidate_email]" aria-invalid="false">
														<span class="help-inline">Provide your email address</span>
													</div>
									</div>
									
									
                                    <div class="form-actions">
                                       
                                       
                                        <button type="submit" href="<?=site_url('/test/senduser');?>" class="btn blue"><i class="icon-ok"></i>
Continue to Send E-Mail 
										</button>
                                       
                                    </div>
                                 </form>
                                 <!-- END FORM-->                
                              </div>
                           </div>
