<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<h4><i class="icon-edit"></i> Test Profiles</h4>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									
									<div class="btn-group pull-right">
										<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">Print</a></li>
											<li><a href="#">Save as PDF</a></li>
											<li><a href="#">Export to Excel</a></li>
										</ul>
									</div>
								</div>
								
								<div id="sample_editable_1_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row-fluid"><div class="span6"><div class="dataTables_length" id="sample_editable_1_length"><label><select class="m-wrap xsmall" aria-controls="sample_editable_1" size="1" name="sample_editable_1_length"><option selected="selected" value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> records per page</label></div></div><div class="span6"><div id="sample_editable_1_filter" class="dataTables_filter"><label>Search: <input type="text" class="m-wrap medium" aria-controls="sample_editable_1"></label></div>
								</div>
								
								<table aria-describedby="sample_editable_1_info" class="table table-striped table-hover table-bordered dataTable" id="sample_editable_1">
									<thead>
										<tr role="row">
										<th aria-label="Username: activate to sort column descending" aria-sort="ascending" style="width: 20px;" colspan="1" rowspan="1" aria-controls="sample_editable_1" tabindex="0" role="columnheader" class="sorting_asc">#</th>
										<th aria-label="Full Name: activate to sort column ascending" style="width: 238px;" colspan="1" rowspan="1" aria-controls="sample_editable_1" tabindex="0" role="columnheader" class="sorting">Candidate Name</th>
										<th aria-label="Points: activate to sort column ascending" style="width: 116px;" colspan="1" rowspan="1" aria-controls="sample_editable_1" tabindex="0" role="columnheader" class="sorting">Candidate E-mail</th>
										<th aria-label="Points: activate to sort column ascending" style="width: 116px;" colspan="1" rowspan="1" aria-controls="sample_editable_1" tabindex="0" role="columnheader" class="sorting">Skill-Set</th>
										<th aria-label="Notes: activate to sort column ascending" style="width: 167px;" colspan="1" rowspan="1" aria-controls="sample_editable_1" tabindex="0" role="columnheader" class="sorting">Status</th>
										</tr>
									</thead>
									
								<tbody aria-relevant="all" aria-live="polite" role="alert"><tr class="odd">
<?php foreach ($test_records as $row)
{ ;?> 
<tr>
<td><?php echo $row['id'];?></td>
<td><?php echo $row['candidate_name'];?></td>
<td><?php echo $row['candidate_email'];?></td>
<td><?php echo $row['skill_set'];?></td>
<td><?php echo $row['conduct_status'];?></td>


</tr>
<?php } ?>	
											
											
											
										</tbody></table><div class="row-fluid"><div class="span6"><div id="sample_editable_1_info" class="dataTables_info">Showing 1 to 6 of 6 entries</div></div><div class="span6"><div class="dataTables_paginate paging_bootstrap pagination"><ul><li class="prev disabled"><a href="#">← Previous</a></li><li class="active"><a href="#">1</a></li><li class="next disabled"><a href="#">Next → </a></li></ul></div></div></div></div>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
</div>
