<div class="portlet box green">
                              <div class="portlet-title">
                                 <h4><i class="icon-reorder"></i>Invite User</h4>
                                 <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                    <a href="javascript:;" class="reload"></a>
                                    <a href="javascript:;" class="remove"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="#" class="form-horizontal">                         
                                    <h3 class="form-section">Add a Question</h3>
                                    
                                    <div class="control-group">
													<label class="control-label">Username<span class="required">*</span></label>
													<div class="controls">
														<input type="text" name="username" class="span6 m-wrap">
														<span class="help-inline">Provide your username</span>
													</div>
												</div>
                                    <div class="control-group">
													<label class="control-label">Email<span class="required">*</span></label>
													<div class="controls">
														<input type="text" name="email" class="span6 m-wrap">
														<span class="help-inline">Provide your email address</span>
													</div>
												</div>
                                    
                                    <div class="form-actions">
                                       <button type="submit" class="btn blue"> Continue
<i class="m-icon-swapright m-icon-white"></i></button>
                                       
                                    </div>
                                 </form>
                                 <!-- END FORM-->                
                              </div>
                           </div>
