<div class="tab-content">
<div id="tab_2" class="tab-pane active">
                           <div class="portlet box green">
                              <div class="portlet-title">
                                 <h4><i class="icon-reorder"></i>Add a New Opportunity</h4>
                                 <div class="tools">
                                    <a class="collapse" href="javascript:;"></a>
                                    <a class="config" data-toggle="modal" href="#portlet-config"></a>
                                    <a class="reload" href="javascript:;"></a>
                                    <a class="remove" href="javascript:;"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form class="form-horizontal" action="<?=site_url('/test/newopportunities');?>" method="post">                         
                                    <h3 class="form-section">Post a Job</h3>
                                    
                                    <div class="control-group">
                                       <label class="control-label">Job Title</label>
                                       <div class="controls">
                                          <input type="text" class="m-wrap large" name="job[job_title]" value="<?=$job_data['job_title']; ?>">
                                       </div>
                                    </div>
									<div>
									<input type="hidden" name="job_id" value="<?=$job_id?>">
									</div>
									<div class="control-group">
                                       <label class="control-label">Job Description</label>
                                       <div class="controls">
                                          <textarea rows="3" class="large m-wrap" name="job[job_description]"><?=$job_data['job_description']; ?></textarea>
                                       </div>
                                    </div>
									
									                           <div class="control-group">
										<label class="control-label">Select Skill Set</label>
										<div class="controls">
											  
											
											<select tabindex="1" data-placeholder="Choose a Category" multiple="multiple" class="span6 m-wrap" name="profile[skill_set][]">
<?php 
    foreach($skill_name as $id=>$name)
	
{
?>
    <option value="<?=$id;?>"> <?=$name;?> </option>
	<?
}
?>
</select>
										<p class="help-block">Select Multiple Skills by pressing Ctrl key.</p>
										</div>
										
									</div>
									
									<div class="control-group">
                                       <label class="control-label">Work Experience</label>
                                       <div class="controls">
                                          <select tabindex="1" class="large m-wrap" name="job[experience]">
                                             <option value="Fresher" <?=$job_data['experience'] == "Fresher" ? 'selected="selected"' : '' ; ?>>Fresher</option>
                                             <option value="1-2 Years" <?=$job_data['experience'] == "1-2 Years" ? 'selected="selected"' : '' ; ?>>1-2 Years</option>
                                             <option value="2-5 Years" <?=$job_data['experience'] == "2-5 Years" ? 'selected="selected"' : '' ; ?>>2-5 Years</option>
                                             <option value="5-10 Years" <?=$job_data['experience'] == "5-10 Years" ? 'selected="selected"' : '' ; ?>>5-10 Years</option>
											 <option value="10-20 Years" <?=$job_data['experience'] == "10-20 Years" ? 'selected="selected"' : '' ; ?>>10-20 Years</option>
											 <option value="20 Years and Above" <?=$job_data['experience'] == "20 Years and Above" ? 'selected="selected"' : '' ; ?>>20 Years and Above</option>

                                          </select>
                                       </div>
                                    </div>
									
									<div class="control-group">
                              <label class="control-label">Job Location</label>
                              <div class="controls">
                                 <input type="text" data-source="[&quot;Alwar&quot;,&quot;Delhi&quot;,&quot;Calcutta&quot;,&quot;Connecticut&quot;,&quot;NewDelhi&quot;,&quot;Jaipur&quot;,&quot;Gurgaon&quot;,&quot;Haridwar&quot;,&quot;Noida&quot;,&quot;Indore&quot;,&quot;Ajmer&quot;,&quot;Pune&quot;,&quot;Mumbai&quot;,&quot;Hyderabad&quot;,&quot;Ahemdabad&quot;,&quot;Chennai&quot;,&quot;Surat&quot;,&quot;Lucknow&quot;,&quot;Kanpur&quot;,&quot;Nagpur&quot;,&quot;Thane&quot;,&quot;Bhopal&quot;,&quot;Montana&quot;,&quot;Agra&quot;,&quot;Nashik&quot;,&quot;Faridabad&quot;,&quot;Amritsar&quot;,&quot;Chandigarh&quot;,&quot;Raipur&quot;,&quot;Dehradun&quot;,&quot;Nagpur&quot;,&quot;agartala&quot;]" data-items="4" data-provide="typeahead" style="margin: 0 auto;" class="large m-wrap" name="job[job_location]" value="<?=$job_data['job_location']; ?>">
                              </div>
                           </div>
									
									<div class="control-group">
                                       <label class="control-label">Job Role</label>
                                       <div class="controls">
                                          <select tabindex="1" class="large m-wrap" name="job[job_role]">
                                             <option value="">--Select--</option>
						<option label="Freshers/Trainee" value="11">Freshers/Trainee</option>
<option label="Accounting/Auditing/Tax/Financial Services" value="1">Accounting/Auditing/Tax/Financial Services</option>
<option label="Admin/office support/secretary/Data Entry" value="2">Admin/office support/secretary/Data Entry</option>
<option label="Advertising/Event Management/Media/PR" value="3">Advertising/Event Management/Media/PR</option>
<option label="Agriculture/Fishing/Forestry/Horticulture" value="7">Agriculture/Fishing/Forestry/Horticulture</option>
<option label="Airlines/Travel/Ticketing" value="4">Airlines/Travel/Ticketing</option>
<option label="Architecture/Construction/Interior/PropertyMgmt" value="6">Architecture/Construction/Interior/PropertyMgmt</option>
<option label="Banking/Insurance" value="5">Banking/Insurance</option>
<option label="Beauty/Personal Care/Fitness/SPA" value="9">Beauty/Personal Care/Fitness/SPA</option>
<option label="Biotech/Research" value="8">Biotech/Research</option>
<option label="BPO/ITES/KPO/Customer Service/Technical Support" value="13">BPO/ITES/KPO/Customer Service/Technical Support</option>
<option label="Business Strategy/Management/Consulting" value="10">Business Strategy/Management/Consulting</option>
<option label="Engineering" value="15">Engineering</option>
<option label="Engineering Design/Production" value="16">Engineering Design/Production</option>
<option label="Fashion Designing/Modeling/Jewellery" value="17">Fashion Designing/Modeling/Jewellery</option>
<option label="Health Care/ Medicine/Nursing" value="20">Health Care/ Medicine/Nursing</option>
<option label="Hotel Management/Catering/Food Technology" value="19">Hotel Management/Catering/Food Technology</option>
<option label="Human Resources" value="22">Human Resources</option>
<option label="Import/Export" value="23">Import/Export</option>
<option label="IT - Hardware" value="25">IT - Hardware</option>
<option label="IT - Networking" value="26">IT - Networking</option>
<option label="IT- Software" value="27">IT- Software</option>
<option label="Legal/Law" value="29">Legal/Law</option>
<option label="Marketing &amp; Product Management/Tele Marketing" value="31">Marketing &amp; Product Management/Tele Marketing</option>
<option label="Operations" value="33">Operations</option>
<option label="Packaging /Delivery/ Logistics Operations" value="14">Packaging /Delivery/ Logistics Operations</option>
<option label="Pharmaceuticals" value="34">Pharmaceuticals</option>
<option label="Photography/TV / films / Radio" value="35">Photography/TV / films / Radio</option>
<option label="Quality Control" value="37">Quality Control</option>
<option label="Retail/Counter/Merchandising/Procurement" value="38">Retail/Counter/Merchandising/Procurement</option>
<option label="Sales/Business Development" value="39">Sales/Business Development</option>
<option label="Security/National Defence Personnel" value="41">Security/National Defence Personnel</option>
<option label="Shipping/Merchant Navy" value="32">Shipping/Merchant Navy</option>
<option label="Skilled Labour/Technician" value="42">Skilled Labour/Technician</option>
<option label="Social Service/NGO" value="43">Social Service/NGO</option>
<option label="Supply chain Mgmt/Distribution/Inventory Mgmt" value="44">Supply chain Mgmt/Distribution/Inventory Mgmt</option>
<option label="Teaching/Training" value="45">Teaching/Training</option>
<option label="Top Management/Senior Management" value="30">Top Management/Senior Management</option>
<option label="Graphic Designer/Visualizer" value="12">Graphic Designer/Visualizer</option>
<option label="Editorial/Content/Journalism" value="47">Editorial/Content/Journalism</option>
<option label="Others" value="48">Others</option>

				
                                          </select>
                                       </div>
                                    </div>
									
									<div class="control-group">
                                       <label class="control-label">Candidate Qualification</label>
                                       <div class="controls">
                                          <select tabindex="1" class="large m-wrap" name="job[candidate_qualification]">
                                             <option label="Any" value="1">Any</option>
<option label="Any Diploma" value="2">Any Diploma</option>
<option label="Any Engineer" value="3">Any Engineer</option>
<option label="Any Finance" value="4">Any Finance</option>
<option label="Any Graduate" value="5">Any Graduate</option>
<option label="Any IT / Computers" value="6">Any IT / Computers</option>
<option label="Any Post Graduate" value="7">Any Post Graduate</option>
<option label="Any Post PG" value="8">Any Post PG</option>
<option label="Any Management" value="489">Any Management</option>
<option label="ACS - Corporate secretaryship" value="9">ACS - Corporate secretaryship</option>
<option label="ACS - Other" value="10">ACS - Other</option>
<option label="B.A - Anthropology" value="11">B.A - Anthropology</option>
<option label="B.A - Arts &amp; Humanities" value="12">B.A - Arts &amp; Humanities</option>
<option label="B.A - Communications" value="13">B.A - Communications</option>
<option label="B.A - Economics" value="14">B.A - Economics</option>
<option label="B.A - English" value="15">B.A - English</option>
<option label="B.A - Film" value="16">B.A - Film</option>
<option label="B.A - Fine Arts" value="17">B.A - Fine Arts</option>
<option label="B.A - Foreign Language" value="18">B.A - Foreign Language</option>
<option label="B.A - Geography" value="19">B.A - Geography</option>
<option label="B.A - Hindi" value="20">B.A - Hindi</option>
<option label="B.A - History" value="21">B.A - History</option>
<option label="B.A - Home Science" value="22">B.A - Home Science</option>
<option label="B.A - International Studies" value="23">B.A - International Studies</option>
<option label="B.A - Journalism" value="24">B.A - Journalism</option>
<option label="B.A - Languages ( Indian)" value="415">B.A - Languages ( Indian)</option>
<option label="B.A - Languages ( International)" value="416">B.A - Languages ( International)</option>
<option label="B.A - Literature" value="25">B.A - Literature</option>
<option label="B.A - Maths" value="417">B.A - Maths</option>
<option label="B.A - Music" value="26">B.A - Music</option>
<option label="B.A - Pass course" value="27">B.A - Pass course</option>
<option label="B.A - Political Science" value="29">B.A - Political Science</option>
<option label="B.A - Philosophy" value="28">B.A - Philosophy</option>
<option label="B.A - PR/Advertising" value="30">B.A - PR/Advertising</option>
<option label="B.A - Psychology" value="418">B.A - Psychology</option>
<option label="B.A - Sanskrit" value="31">B.A - Sanskrit</option>
<option label="B.A - Social Science" value="32">B.A - Social Science</option>
<option label="B.A - Social Work" value="33">B.A - Social Work</option>
<option label="B.A - Sociology" value="34">B.A - Sociology</option>
<option label="B.A - Statistics" value="35">B.A - Statistics</option>
<option label="B.A - Tamil" value="36">B.A - Tamil</option>
<option label="B.A - Telugu" value="37">B.A - Telugu</option>
<option label="B.A - Vocational Course" value="38">B.A - Vocational Course</option>
<option label="B.A - Other" value="39">B.A - Other</option>
<option label="B.Arch - Architecture" value="41">B.Arch - Architecture</option>
<option label="B.Arch - Other" value="419">B.Arch - Other</option>
<option label="B.Com - Commerce" value="43">B.Com - Commerce</option>
<option label="B.Com - Other" value="420">B.Com - Other</option>
<option label="B.Ed - Education" value="44">B.Ed - Education</option>
<option label="B.Ed - Other" value="421">B.Ed - Other</option>
<option label="B.L.I.Sc - Library &amp; information Science" value="45">B.L.I.Sc - Library &amp; information Science</option>
<option label="B.L.I.Sc - Other" value="422">B.L.I.Sc - Other</option>
<option label="B.Music - Music" value="46">B.Music - Music</option>
<option label="B.Music - Other" value="423">B.Music - Other</option>
<option label="B.Pharm - Pharmacy" value="47">B.Pharm - Pharmacy</option>
<option label="B.Pharm - Other" value="424">B.Pharm - Other</option>
<option label="B.Sc - Agriculture" value="48">B.Sc - Agriculture</option>
<option label="B.Sc - Anthropology" value="49">B.Sc - Anthropology</option>
<option label="B.Sc - Bio-Chemistry" value="50">B.Sc - Bio-Chemistry</option>
<option label="B.Sc - Biology" value="51">B.Sc - Biology</option>
<option label="B.Sc - Botany" value="52">B.Sc - Botany</option>
<option label="B.Sc - Chemistry" value="53">B.Sc - Chemistry</option>
<option label="B.Sc - Computers/IT" value="54">B.Sc - Computers/IT</option>
<option label="B.Sc - Dairy Technology" value="55">B.Sc - Dairy Technology</option>
<option label="B.Sc - Economics" value="56">B.Sc - Economics</option>
<option label="B.Sc - Electronics" value="57">B.Sc - Electronics</option>
<option label="B.Sc - Environmental Science" value="58">B.Sc - Environmental Science</option>
<option label="B.Sc - Food Technology" value="59">B.Sc - Food Technology</option>
<option label="B.Sc - Geography" value="60">B.Sc - Geography</option>
<option label="B.Sc - Geology" value="61">B.Sc - Geology</option>
<option label="B.Sc - Home Science" value="62">B.Sc - Home Science</option>
<option label="B.Sc - Mathematics" value="63">B.Sc - Mathematics</option>
<option label="B.Sc - Micro-biology" value="64">B.Sc - Micro-biology</option>
<option label="B.Sc - Nursing" value="65">B.Sc - Nursing</option>
<option label="B.Sc - Pass Course" value="66">B.Sc - Pass Course</option>
<option label="B.Sc - Physics" value="67">B.Sc - Physics</option>
<option label="B.Sc - Plant -Biology" value="68">B.Sc - Plant -Biology</option>
<option label="B.Sc - Psychology" value="69">B.Sc - Psychology</option>
<option label="B.Sc - Statistics" value="70">B.Sc - Statistics</option>
<option label="B.Sc - Textiles" value="71">B.Sc - Textiles</option>
<option label="B.Sc - Visual Communication" value="72">B.Sc - Visual Communication</option>
<option label="B.Sc - Zoology" value="73">B.Sc - Zoology</option>
<option label="B.Sc - Other" value="425">B.Sc - Other</option>
<option label="BBA - Management" value="74">BBA - Management</option>
<option label="BBA - Administration" value="426">BBA - Administration</option>
<option label="BBA - Other" value="427">BBA - Other</option>
<option label="BCA - Computers" value="75">BCA - Computers</option>
<option label="BCA - Other" value="428">BCA - Other</option>
<option label="BCS - Computer Science" value="76">BCS - Computer Science</option>
<option label="BCS - Corporate Secretary ship" value="77">BCS - Corporate Secretary ship</option>
<option label="BCS - Other" value="78">BCS - Other</option>
<option label="BDS - Dentistry" value="79">BDS - Dentistry</option>
<option label="BDS - Other" value="429">BDS - Other</option>
<option label="B.E./B.Tech - Agriculture" value="80">B.E./B.Tech - Agriculture</option>
<option label="B.E./B.Tech - Architecture" value="81">B.E./B.Tech - Architecture</option>
<option label="B.E./B.Tech - Automobile" value="82">B.E./B.Tech - Automobile</option>
<option label="B.E./B.Tech - Aviation" value="83">B.E./B.Tech - Aviation</option>
<option label="B.E./B.Tech - Bio - chemistry/ Bio- technology" value="84">B.E./B.Tech - Bio - chemistry/ Bio- technology</option>
<option label="B.E./B.Tech - Bio-medical" value="85">B.E./B.Tech - Bio-medical</option>
<option label="B.E./B.Tech - Ceramics" value="86">B.E./B.Tech - Ceramics</option>
<option label="B.E./B.Tech - Chemicals" value="87">B.E./B.Tech - Chemicals</option>
<option label="B.E./B.Tech - Civil" value="88">B.E./B.Tech - Civil</option>
<option label="B.E./B.Tech - Computers/IT" value="89">B.E./B.Tech - Computers/IT</option>
<option label="B.E./B.Tech - Electrical" value="90">B.E./B.Tech - Electrical</option>
<option label="B.E./B.Tech - Electronics/Telecommunication" value="91">B.E./B.Tech - Electronics/Telecommunication</option>
<option label="B.E./B.Tech - Energy" value="92">B.E./B.Tech - Energy</option>
<option label="B.E./B.Tech - Environmental" value="93">B.E./B.Tech - Environmental</option>
<option label="B.E./B.Tech - Instrumentation" value="94">B.E./B.Tech - Instrumentation</option>
<option label="B.E./B.Tech - Leather Technology" value="95">B.E./B.Tech - Leather Technology</option>
<option label="B.E./B.Tech - Marine" value="96">B.E./B.Tech - Marine</option>
<option label="B.E./B.Tech - Mechanical" value="97">B.E./B.Tech - Mechanical</option>
<option label="B.E./B.Tech - Metallurgy" value="98">B.E./B.Tech - Metallurgy</option>
<option label="B.E./B.Tech - Mineral" value="99">B.E./B.Tech - Mineral</option>
<option label="B.E./B.Tech - Mining" value="100">B.E./B.Tech - Mining</option>
<option label="B.E./B.Tech - Nuclear" value="430">B.E./B.Tech - Nuclear</option>
<option label="B.E./B.Tech - Oceanography" value="101">B.E./B.Tech - Oceanography</option>
<option label="B.E./B.Tech - Paint/oil" value="102">B.E./B.Tech - Paint/oil</option>
<option label="B.E./B.Tech - Petroleum" value="103">B.E./B.Tech - Petroleum</option>
<option label="B.E./B.Tech - Plastics" value="104">B.E./B.Tech - Plastics</option>
<option label="B.E./B.Tech - Production/Industrial" value="105">B.E./B.Tech - Production/Industrial</option>
<option label="B.E./B.Tech - Textile" value="106">B.E./B.Tech - Textile</option>
<option label="B.E./B.Tech - Other" value="107">B.E./B.Tech - Other</option>
<option label="BHM - Hotel Management" value="108">BHM - Hotel Management</option>
<option label="BHM - Other" value="431">BHM - Other</option>
<option label="BL/LLB - Law" value="109">BL/LLB - Law</option>
<option label="BL/LLB - Other" value="432">BL/LLB - Other</option>
<option label="BVSC - Veterinary Science" value="110">BVSC - Veterinary Science</option>
<option label="BVSC - Other" value="433">BVSC - Other</option>
<option label="C.A. -  C.A." value="111">C.A. -  C.A.</option>
<option label="C.A. - Other" value="434">C.A. - Other</option>
<option label="CFA - Chartered Financial Analysis" value="490">CFA - Chartered Financial Analysis</option>
<option label="Diploma - Anesthesiology (DA)" value="112">Diploma - Anesthesiology (DA)</option>
<option label="Diploma - Architecture" value="113">Diploma - Architecture</option>
<option label="Diploma - Bio-Informatics" value="114">Diploma - Bio-Informatics</option>
<option label="Diploma - Business Management" value="115">Diploma - Business Management</option>
<option label="Diploma - Chemical" value="116">Diploma - Chemical</option>
<option label="Diploma - Civil" value="117">Diploma - Civil</option>
<option label="Diploma - Computer Application (A.D.C)" value="118">Diploma - Computer Application (A.D.C)</option>
<option label="Diploma - Computer Management" value="119">Diploma - Computer Management</option>
<option label="Diploma - Co-operative Laws" value="120">Diploma - Co-operative Laws</option>
<option label="Diploma - Education for Deaf" value="121">Diploma - Education for Deaf</option>
<option label="Diploma - Electrical" value="122">Diploma - Electrical</option>
<option label="Diploma - Electronics/Telecommunication" value="123">Diploma - Electronics/Telecommunication</option>
<option label="Diploma - Engineering" value="124">Diploma - Engineering</option>
<option label="Diploma - Family Planning (DFP) Population Pro" value="125">Diploma - Family Planning (DFP) Population Pro</option>
<option label="Diploma - Fashion Designing/ other Designing" value="126">Diploma - Fashion Designing/ other Designing</option>
<option label="Diploma - Financial Services" value="127">Diploma - Financial Services</option>
<option label="Diploma - Graphic / Web Designing" value="128">Diploma - Graphic / Web Designing</option>
<option label="Diploma - Hospital Administration" value="129">Diploma - Hospital Administration</option>
<option label="Diploma - Hotel Management" value="130">Diploma - Hotel Management</option>
<option label="Diploma - Interior Designing" value="131">Diploma - Interior Designing</option>
<option label="Diploma - Labors laws &amp; Labor Welfare" value="132">Diploma - Labors laws &amp; Labor Welfare</option>
<option label="Diploma - Legislative Drafting" value="133">Diploma - Legislative Drafting</option>
<option label="Diploma - Manuscriptology" value="134">Diploma - Manuscriptology</option>
<option label="Diploma - Marketing" value="135">Diploma - Marketing</option>
<option label="Diploma - Mechanical" value="136">Diploma - Mechanical</option>
<option label="Diploma - Music" value="137">Diploma - Music</option>
<option label="Diploma - Neurological &amp; Neurosurgical Nursing" value="138">Diploma - Neurological &amp; Neurosurgical Nursing</option>
<option label="Diploma - Pharmacy" value="139">Diploma - Pharmacy</option>
<option label="Diploma - Print Administration ( D.P.A)" value="140">Diploma - Print Administration ( D.P.A)</option>
<option label="Diploma - Public Health" value="141">Diploma - Public Health</option>
<option label="Diploma - Rural Journalism &amp; Mass Communication" value="142">Diploma - Rural Journalism &amp; Mass Communication</option>
<option label="Diploma - Sports Journalism" value="143">Diploma - Sports Journalism</option>
<option label="Diploma - Taxation Laws" value="144">Diploma - Taxation Laws</option>
<option label="Diploma - Travel Management/Tourism" value="145">Diploma - Travel Management/Tourism</option>
<option label="Diploma - Tuberculosis and chest diseases" value="146">Diploma - Tuberculosis and chest diseases</option>
<option label="Diploma - Venereology and Dermatology" value="147">Diploma - Venereology and Dermatology</option>
<option label="Diploma - Visual Arts" value="148">Diploma - Visual Arts</option>
<option label="Diploma - Vocational Course" value="149">Diploma - Vocational Course</option>
<option label="Diploma - Export/Import" value="435">Diploma - Export/Import</option>
<option label="Diploma - Insurance" value="436">Diploma - Insurance</option>
<option label="Diploma - Management" value="437">Diploma - Management</option>
<option label="Diploma - Tourism" value="438">Diploma - Tourism</option>
<option label="Diploma - Other" value="150">Diploma - Other</option>
<option label="ICWA - Cost Accounting" value="151">ICWA - Cost Accounting</option>
<option label="ICWA - Other" value="439">ICWA - Other</option>
<option label="Integreted PG - Journalism/Mass communication" value="152">Integreted PG - Journalism/Mass communication</option>
<option label="Integreted PG - Management" value="153">Integreted PG - Management</option>
<option label="Integreted PG - PR/Advertising" value="154">Integreted PG - PR/Advertising</option>
<option label="Integreted PG - Tourism" value="155">Integreted PG - Tourism</option>
<option label="Integreted PG - Other" value="156">Integreted PG - Other</option>
<option label="M.Arch - Architecture" value="157">M.Arch - Architecture</option>
<option label="M.Arch - Building Engineering &amp; Management" value="440">M.Arch - Building Engineering &amp; Management</option>
<option label="M.Arch - Landscape Engineering" value="441">M.Arch - Landscape Engineering</option>
<option label="M.Arch - Planning" value="442">M.Arch - Planning</option>
<option label="M.Arch - Other" value="443">M.Arch - Other</option>
<option label="M.Com - Commerce" value="158">M.Com - Commerce</option>
<option label="M.Com - Other" value="444">M.Com - Other</option>
<option label="M.Ed - Education" value="159">M.Ed - Education</option>
<option label="M.Ed - Other" value="445">M.Ed - Other</option>
<option label="M.L.I.Sc - Library &amp; Information Science" value="160">M.L.I.Sc - Library &amp; Information Science</option>
<option label="M.L.I.Sc - Other" value="161">M.L.I.Sc - Other</option>
<option label="M.Pharm - Pharmacy" value="162">M.Pharm - Pharmacy</option>
<option label="M.Pharm - Other" value="446">M.Pharm - Other</option>
<option label="M.Phil - Anthropology" value="163">M.Phil - Anthropology</option>
<option label="M.Phil - Applied geology" value="164">M.Phil - Applied geology</option>
<option label="M.Phil - Chemistry" value="165">M.Phil - Chemistry</option>
<option label="M.Phil - Commerce" value="166">M.Phil - Commerce</option>
<option label="M.Phil - Computer Science" value="167">M.Phil - Computer Science</option>
<option label="M.Phil - Drama &amp; Theater Arts" value="168">M.Phil - Drama &amp; Theater Arts</option>
<option label="M.Phil - Economics" value="169">M.Phil - Economics</option>
<option label="M.Phil - Education" value="170">M.Phil - Education</option>
<option label="M.Phil - English" value="171">M.Phil - English</option>
<option label="M.Phil - Entrepreneurship" value="172">M.Phil - Entrepreneurship</option>
<option label="M.Phil - Environmental Science" value="173">M.Phil - Environmental Science</option>
<option label="M.Phil - Hindi" value="174">M.Phil - Hindi</option>
<option label="M.Phil - History" value="175">M.Phil - History</option>
<option label="M.Phil - JMC" value="176">M.Phil - JMC</option>
<option label="M.Phil - Labour Studies" value="177">M.Phil - Labour Studies</option>
<option label="M.Phil - Life science" value="178">M.Phil - Life science</option>
<option label="M.Phil - Literature" value="179">M.Phil - Literature</option>
<option label="M.Phil - Management" value="180">M.Phil - Management</option>
<option label="M.Phil - Mathematics" value="181">M.Phil - Mathematics</option>
<option label="M.Phil - Peace Making and Gandhian Thought" value="182">M.Phil - Peace Making and Gandhian Thought</option>
<option label="M.Phil - Philosophy" value="183">M.Phil - Philosophy</option>
<option label="M.Phil - Physics" value="184">M.Phil - Physics</option>
<option label="M.Phil - Political Science" value="185">M.Phil - Political Science</option>
<option label="M.Phil - Public Administration" value="186">M.Phil - Public Administration</option>
<option label="M.Phil - Sociology" value="187">M.Phil - Sociology</option>
<option label="M.Phil - Tamil" value="188">M.Phil - Tamil</option>
<option label="M.Phil - Telugu" value="189">M.Phil - Telugu</option>
<option label="M.Phil - Tourism" value="190">M.Phil - Tourism</option>
<option label="M.Phil - Other" value="191">M.Phil - Other</option>
<option label="M.Sc - Agriculture" value="192">M.Sc - Agriculture</option>
<option label="M.Sc - Anthropology" value="193">M.Sc - Anthropology</option>
<option label="M.Sc - Bio-Chemistry" value="194">M.Sc - Bio-Chemistry</option>
<option label="M.Sc - Biology" value="195">M.Sc - Biology</option>
<option label="M.Sc - Botany" value="196">M.Sc - Botany</option>
<option label="M.Sc - Chemistry" value="197">M.Sc - Chemistry</option>
<option label="M.Sc - Computers/IT" value="198">M.Sc - Computers/IT</option>
<option label="M.Sc - Dairy Technology" value="199">M.Sc - Dairy Technology</option>
<option label="M.Sc - Economics" value="200">M.Sc - Economics</option>
<option label="M.Sc - Electronics" value="201">M.Sc - Electronics</option>
<option label="M.Sc - Environmental Science" value="202">M.Sc - Environmental Science</option>
<option label="M.Sc - Food Technology" value="203">M.Sc - Food Technology</option>
<option label="M.Sc - Geography" value="204">M.Sc - Geography</option>
<option label="M.Sc - Geology" value="205">M.Sc - Geology</option>
<option label="M.Sc - Home Science" value="206">M.Sc - Home Science</option>
<option label="M.Sc - Mathematics" value="207">M.Sc - Mathematics</option>
<option label="M.Sc - Micro-biology" value="208">M.Sc - Micro-biology</option>
<option label="M.Sc - Nursing" value="209">M.Sc - Nursing</option>
<option label="M.Sc - Pass Course" value="210">M.Sc - Pass Course</option>
<option label="M.Sc - Physics" value="211">M.Sc - Physics</option>
<option label="M.Sc - Plant -Biology" value="212">M.Sc - Plant -Biology</option>
<option label="M.Sc - Psychology" value="213">M.Sc - Psychology</option>
<option label="M.Sc - Statistics" value="214">M.Sc - Statistics</option>
<option label="M.Sc - Textiles" value="215">M.Sc - Textiles</option>
<option label="M.Sc - Visual Communication" value="216">M.Sc - Visual Communication</option>
<option label="M.Sc - Zoology" value="217">M.Sc - Zoology</option>
<option label="M.Sc - Other" value="218">M.Sc - Other</option>
<option label="M.A - Anthropology" value="219">M.A - Anthropology</option>
<option label="M.A - Arts &amp; Humanities" value="220">M.A - Arts &amp; Humanities</option>
<option label="M.A - Communications" value="221">M.A - Communications</option>
<option label="M.A - Economics" value="222">M.A - Economics</option>
<option label="M.A - English" value="223">M.A - English</option>
<option label="M.A - Film" value="224">M.A - Film</option>
<option label="M.A - Fine Arts" value="225">M.A - Fine Arts</option>
<option label="M.A - Foreign Language" value="447">M.A - Foreign Language</option>
<option label="M.A - Geography" value="226">M.A - Geography</option>
<option label="M.A - Hindi" value="227">M.A - Hindi</option>
<option label="M.A - History" value="448">M.A - History</option>
<option label="M.A - Home Science" value="228">M.A - Home Science</option>
<option label="M.A - International Studies" value="229">M.A - International Studies</option>
<option label="M.A - Journalism" value="230">M.A - Journalism</option>
<option label="M.A - Languages ( Indian)" value="449">M.A - Languages ( Indian)</option>
<option label="M.A - Languages ( International)" value="450">M.A - Languages ( International)</option>
<option label="M.A - Literature" value="231">M.A - Literature</option>
<option label="M.A - Maths" value="451">M.A - Maths</option>
<option label="M.A - Music" value="232">M.A - Music</option>
<option label="M.A - Pass Course" value="233">M.A - Pass Course</option>
<option label="M.A - Philosophy" value="234">M.A - Philosophy</option>
<option label="M.A - Political Science" value="235">M.A - Political Science</option>
<option label="M.A - PR/Advertising" value="236">M.A - PR/Advertising</option>
<option label="M.A - Sanskrit" value="238">M.A - Sanskrit</option>
<option label="M.A - Social Science" value="239">M.A - Social Science</option>
<option label="M.A - Social Work" value="240">M.A - Social Work</option>
<option label="M.A - Sociology" value="241">M.A - Sociology</option>
<option label="M.A - Statistics" value="242">M.A - Statistics</option>
<option label="M.A - Tamil" value="243">M.A - Tamil</option>
<option label="M.A - Telugu" value="452">M.A - Telugu</option>
<option label="M.A - Vocational Course" value="244">M.A - Vocational Course</option>
<option label="M.A - Public Administration" value="237">M.A - Public Administration</option>
<option label="M.A - Psychology" value="453">M.A - Psychology</option>
<option label="M.A - Other" value="245">M.A - Other</option>
<option label="MBA/PGDM/PGDBA - Advertising/Mass Communication" value="246">MBA/PGDM/PGDBA - Advertising/Mass Communication</option>
<option label="MBA/PGDM/PGDBA - Fashion Design" value="247">MBA/PGDM/PGDBA - Fashion Design</option>
<option label="MBA/PGDM/PGDBA - Finance" value="248">MBA/PGDM/PGDBA - Finance</option>
<option label="MBA/PGDM/PGDBA - HR/Industrial Relations" value="249">MBA/PGDM/PGDBA - HR/Industrial Relations</option>
<option label="MBA/PGDM/PGDBA - Information Technology" value="250">MBA/PGDM/PGDBA - Information Technology</option>
<option label="MBA/PGDM/PGDBA - International Business" value="251">MBA/PGDM/PGDBA - International Business</option>
<option label="MBA/PGDM/PGDBA - Marketing" value="252">MBA/PGDM/PGDBA - Marketing</option>
<option label="MBA/PGDM/PGDBA - Systems" value="253">MBA/PGDM/PGDBA - Systems</option>
<option label="MBA/PGDM/PGDBA - Other Management" value="254">MBA/PGDM/PGDBA - Other Management</option>
<option label="MBA/PGDM/PGDBA - Other" value="255">MBA/PGDM/PGDBA - Other</option>
<option label="MBBS - Medicine" value="256">MBBS - Medicine</option>
<option label="MBBS - Other" value="454">MBBS - Other</option>
<option label="MBL/LLBC - Business Law" value="257">MBL/LLBC - Business Law</option>
<option label="MBL/LLBC - Other" value="455">MBL/LLBC - Other</option>
<option label="MCA/PGDCA - Computers" value="258">MCA/PGDCA - Computers</option>
<option label="MCA/PGDCA - Other" value="456">MCA/PGDCA - Other</option>
<option label="MD/MS - Anesthesiology" value="457">MD/MS - Anesthesiology</option>
<option label="MD/MS - Bio-Chemistry" value="458">MD/MS - Bio-Chemistry</option>
<option label="MD/MS - Cardiology" value="259">MD/MS - Cardiology</option>
<option label="MD/MS - Dentistry" value="260">MD/MS - Dentistry</option>
<option label="MD/MS - Dermatology" value="261">MD/MS - Dermatology</option>
<option label="MD/MS - Emergency Medicine" value="459">MD/MS - Emergency Medicine</option>
<option label="MD/MS - ENT" value="262">MD/MS - ENT</option>
<option label="MD/MS - Gastroenterology" value="460">MD/MS - Gastroenterology</option>
<option label="MD/MS - General" value="263">MD/MS - General</option>
<option label="MD/MS - Genetic Medicine" value="461">MD/MS - Genetic Medicine</option>
<option label="MD/MS - Gynecology" value="264">MD/MS - Gynecology</option>
<option label="MD/MS - Hepatology" value="265">MD/MS - Hepatology</option>
<option label="MD/MS - Immunology" value="266">MD/MS - Immunology</option>
<option label="MD/MS - Internal Medicine" value="462">MD/MS - Internal Medicine</option>
<option label="MD/MS - Microbiology" value="267">MD/MS - Microbiology</option>
<option label="MD/MS - Neonatal" value="268">MD/MS - Neonatal</option>
<option label="MD/MS - Nephrology" value="269">MD/MS - Nephrology</option>
<option label="MD/MS - Neurology" value="463">MD/MS - Neurology</option>
<option label="MD/MS - Nuclear Medicine" value="464">MD/MS - Nuclear Medicine</option>
<option label="MD/MS - Obestretrics" value="270">MD/MS - Obestretrics</option>
<option label="MD/MS - Oncology" value="271">MD/MS - Oncology</option>
<option label="MD/MS - Ophthalmology" value="272">MD/MS - Ophthalmology</option>
<option label="MD/MS - Orthopedic" value="273">MD/MS - Orthopedic</option>
<option label="MD/MS - Pathology" value="274">MD/MS - Pathology</option>
<option label="MD/MS - Pediatrics" value="275">MD/MS - Pediatrics</option>
<option label="MD/MS - Preventive and social medicine" value="465">MD/MS - Preventive and social medicine</option>
<option label="MD/MS - Psychiatry" value="276">MD/MS - Psychiatry</option>
<option label="MD/MS - Psychology" value="277">MD/MS - Psychology</option>
<option label="MD/MS - Physiotherapy" value="466">MD/MS - Physiotherapy</option>
<option label="MD/MS - Radiology" value="278">MD/MS - Radiology</option>
<option label="MD/MS - Rehabilitation Medicine" value="467">MD/MS - Rehabilitation Medicine</option>
<option label="MD/MS - Reproductive Medicine" value="468">MD/MS - Reproductive Medicine</option>
<option label="MD/MS - Rheumatology" value="279">MD/MS - Rheumatology</option>
<option label="MD/MS - Surgery" value="469">MD/MS - Surgery</option>
<option label="MD/MS - Urology" value="470">MD/MS - Urology</option>
<option label="MD/MS - Other" value="280">MD/MS - Other</option>
<option label="M.E./M.Tech - Agriculture" value="281">M.E./M.Tech - Agriculture</option>
<option label="M.E./M.Tech - Applied Electronics" value="282">M.E./M.Tech - Applied Electronics</option>
<option label="M.E./M.Tech - Architecture" value="283">M.E./M.Tech - Architecture</option>
<option label="M.E./M.Tech - Automobile" value="284">M.E./M.Tech - Automobile</option>
<option label="M.E./M.Tech - Aviation" value="285">M.E./M.Tech - Aviation</option>
<option label="M.E./M.Tech - Bio-chemistry/Bio Technology" value="286">M.E./M.Tech - Bio-chemistry/Bio Technology</option>
<option label="M.E./M.Tech - Bio-Medical" value="287">M.E./M.Tech - Bio-Medical</option>
<option label="M.E./M.Tech - Ceramics" value="288">M.E./M.Tech - Ceramics</option>
<option label="M.E./M.Tech - Chemical" value="289">M.E./M.Tech - Chemical</option>
<option label="M.E./M.Tech - Civil" value="290">M.E./M.Tech - Civil</option>
<option label="M.E./M.Tech - Computers/IT" value="291">M.E./M.Tech - Computers/IT</option>
<option label="M.E./M.Tech - Electrical" value="292">M.E./M.Tech - Electrical</option>
<option label="M.E./M.Tech - Electronics/Telecommunication" value="293">M.E./M.Tech - Electronics/Telecommunication</option>
<option label="M.E./M.Tech - Embedded Systems" value="294">M.E./M.Tech - Embedded Systems</option>
<option label="M.E./M.Tech - Energy" value="295">M.E./M.Tech - Energy</option>
<option label="M.E./M.Tech - Environmental" value="296">M.E./M.Tech - Environmental</option>
<option label="M.E./M.Tech - Geomatics" value="297">M.E./M.Tech - Geomatics</option>
<option label="M.E./M.Tech - Instrumentation" value="298">M.E./M.Tech - Instrumentation</option>
<option label="M.E./M.Tech - Leather Technology" value="299">M.E./M.Tech - Leather Technology</option>
<option label="M.E./M.Tech - Marine" value="300">M.E./M.Tech - Marine</option>
<option label="M.E./M.Tech - Mechanical" value="301">M.E./M.Tech - Mechanical</option>
<option label="M.E./M.Tech - Metallurgy" value="302">M.E./M.Tech - Metallurgy</option>
<option label="M.E./M.Tech - Mineral" value="303">M.E./M.Tech - Mineral</option>
<option label="M.E./M.Tech - Mining" value="304">M.E./M.Tech - Mining</option>
<option label="M.E./M.Tech - Oceanography" value="305">M.E./M.Tech - Oceanography</option>
<option label="M.E./M.Tech - Paint/oil" value="306">M.E./M.Tech - Paint/oil</option>
<option label="M.E./M.Tech - Petroleum" value="307">M.E./M.Tech - Petroleum</option>
<option label="M.E./M.Tech - Plastics" value="308">M.E./M.Tech - Plastics</option>
<option label="M.E./M.Tech - Production/Industrial" value="309">M.E./M.Tech - Production/Industrial</option>
<option label="M.E./M.Tech - Structural" value="310">M.E./M.Tech - Structural</option>
<option label="M.E./M.Tech - Textile" value="311">M.E./M.Tech - Textile</option>
<option label="M.E./M.Tech - Thermal" value="312">M.E./M.Tech - Thermal</option>
<option label="M.E./M.Tech - Other" value="313">M.E./M.Tech - Other</option>
<option label="MFA - Fine Arts" value="314">MFA - Fine Arts</option>
<option label="MFA - Other" value="315">MFA - Other</option>
<option label="MHRM - Human Resource" value="316">MHRM - Human Resource</option>
<option label="MHRM - Other" value="471">MHRM - Other</option>
<option label="MIB - International Business" value="317">MIB - International Business</option>
<option label="MIB - Other" value="318">MIB - Other</option>
<option label="ML/LLM - Law" value="319">ML/LLM - Law</option>
<option label="ML/LLM - Other" value="472">ML/LLM - Other</option>
<option label="MLM - Labor" value="320">MLM - Labor</option>
<option label="MLM - Other" value="473">MLM - Other</option>
<option label="MMS - Management Studies" value="321">MMS - Management Studies</option>
<option label="MMS - Other" value="322">MMS - Other</option>
<option label="MP.Ed - Physical Education" value="323">MP.Ed - Physical Education</option>
<option label="MP.Ed - Other" value="474">MP.Ed - Other</option>
<option label="MSW - Social Work" value="324">MSW - Social Work</option>
<option label="MSW - Other" value="475">MSW - Other</option>
<option label="MTM - Tourism" value="325">MTM - Tourism</option>
<option label="MTM - Other" value="476">MTM - Other</option>
<option label="MVSC - Veterinary Science" value="326">MVSC - Veterinary Science</option>
<option label="MVSC - Other" value="477">MVSC - Other</option>
<option label="FRCS/FRCP - General Surgeon" value="327">FRCS/FRCP - General Surgeon</option>
<option label="FRCS/FRCP - General Physician" value="328">FRCS/FRCP - General Physician</option>
<option label="FRCS/FRCP - Other" value="329">FRCS/FRCP - Other</option>
<option label="PG Diploma - Advertising" value="330">PG Diploma - Advertising</option>
<option label="PG Diploma - Anaesthesiology(DA)" value="331">PG Diploma - Anaesthesiology(DA)</option>
<option label="PG Diploma - Architecture" value="333">PG Diploma - Architecture</option>
<option label="PG Diploma - Bio-Informatics" value="332">PG Diploma - Bio-Informatics</option>
<option label="PG Diploma - Bio-Technology" value="335">PG Diploma - Bio-Technology</option>
<option label="PG Diploma - Business Management/Administration" value="336">PG Diploma - Business Management/Administration</option>
<option label="PG Diploma - Chemical" value="337">PG Diploma - Chemical</option>
<option label="PG Diploma - Civil" value="338">PG Diploma - Civil</option>
<option label="PG Diploma - Clinical Bio-Chemistry" value="339">PG Diploma - Clinical Bio-Chemistry</option>
<option label="PG Diploma - Computer Application/Management" value="340">PG Diploma - Computer Application/Management</option>
<option label="PG Diploma - Co-operative laws" value="341">PG Diploma - Co-operative laws</option>
<option label="PG Diploma - Education for Deaf" value="342">PG Diploma - Education for Deaf</option>
<option label="PG Diploma - Electrical" value="343">PG Diploma - Electrical</option>
<option label="PG Diploma - Electronics/Telecommunication" value="344">PG Diploma - Electronics/Telecommunication</option>
<option label="PG Diploma - Engineering" value="345">PG Diploma - Engineering</option>
<option label="PG Diploma - Family Planning (DFB) Population Pro" value="346">PG Diploma - Family Planning (DFB) Population Pro</option>
<option label="PG Diploma - Fashion Designing/Other Designing" value="347">PG Diploma - Fashion Designing/Other Designing</option>
<option label="PG Diploma - Financial Services (D.F.S)" value="348">PG Diploma - Financial Services (D.F.S)</option>
<option label="PG Diploma - Graphic / Web Designing" value="349">PG Diploma - Graphic / Web Designing</option>
<option label="PG Diploma - Hospital Administration" value="350">PG Diploma - Hospital Administration</option>
<option label="PG Diploma - Hotel Management" value="351">PG Diploma - Hotel Management</option>
<option label="PG Diploma - Human Resource" value="352">PG Diploma - Human Resource</option>
<option label="PG Diploma - Interior Designing" value="353">PG Diploma - Interior Designing</option>
<option label="PG Diploma - International Business" value="354">PG Diploma - International Business</option>
<option label="PG Diploma - Journalism and Mass Communication" value="355">PG Diploma - Journalism and Mass Communication</option>
<option label="PG Diploma - Labour Laws and Labour Welfare" value="356">PG Diploma - Labour Laws and Labour Welfare</option>
<option label="PG Diploma - Legislative Drafting" value="357">PG Diploma - Legislative Drafting</option>
<option label="PG Diploma - Manuscriptology" value="358">PG Diploma - Manuscriptology</option>
<option label="PG Diploma - Marketing" value="359">PG Diploma - Marketing</option>
<option label="PG Diploma - Mechanical" value="360">PG Diploma - Mechanical</option>
<option label="PG Diploma - Music" value="361">PG Diploma - Music</option>
<option label="PG Diploma - Neurological &amp; Neurosurgical Nursing" value="362">PG Diploma - Neurological &amp; Neurosurgical Nursing</option>
<option label="PG Diploma - Pharmacy" value="363">PG Diploma - Pharmacy</option>
<option label="PG Diploma - Polymer Science" value="364">PG Diploma - Polymer Science</option>
<option label="PG Diploma - Print Administration (D.P.A)" value="365">PG Diploma - Print Administration (D.P.A)</option>
<option label="PG Diploma - Public Health" value="366">PG Diploma - Public Health</option>
<option label="PG Diploma - Rural Management" value="367">PG Diploma - Rural Management</option>
<option label="PG Diploma - Sports Journalism" value="368">PG Diploma - Sports Journalism</option>
<option label="PG Diploma - Taxation Laws" value="369">PG Diploma - Taxation Laws</option>
<option label="PG Diploma - Travel management/Tourism" value="370">PG Diploma - Travel management/Tourism</option>
<option label="PG Diploma - Tuberculosis and chest diseases" value="371">PG Diploma - Tuberculosis and chest diseases</option>
<option label="PG Diploma - Venereology and Dermatology" value="372">PG Diploma - Venereology and Dermatology</option>
<option label="PG Diploma - Visual Arts" value="373">PG Diploma - Visual Arts</option>
<option label="PG Diploma - Vocational Course" value="374">PG Diploma - Vocational Course</option>
<option label="PG Diploma - Other" value="375">PG Diploma - Other</option>
<option label="Ph.D - Advertising/Mass Communication" value="376">Ph.D - Advertising/Mass Communication</option>
<option label="Ph.D - Agriculture" value="377">Ph.D - Agriculture</option>
<option label="Ph.D - Ancient Indian culture" value="378">Ph.D - Ancient Indian culture</option>
<option label="Ph.D - Aviation" value="379">Ph.D - Aviation</option>
<option label="Ph.D - Bio-Technology" value="380">Ph.D - Bio-Technology</option>
<option label="Ph.D - Botany" value="381">Ph.D - Botany</option>
<option label="Ph.D - Chemical" value="382">Ph.D - Chemical</option>
<option label="Ph.D - Chemistry" value="383">Ph.D - Chemistry</option>
<option label="Ph.D - Commerce" value="384">Ph.D - Commerce</option>
<option label="Ph.D - Computer science/Engineering" value="406">Ph.D - Computer science/Engineering</option>
<option label="Ph.D - Dairy Technology" value="385">Ph.D - Dairy Technology</option>
<option label="Ph.D - Economics" value="386">Ph.D - Economics</option>
<option label="Ph.D - Ecology &amp; Environmental Science" value="407">Ph.D - Ecology &amp; Environmental Science</option>
<option label="Ph.D - English" value="387">Ph.D - English</option>
<option label="Ph.D - Food Technology" value="388">Ph.D - Food Technology</option>
<option label="Ph.D - Geology" value="389">Ph.D - Geology</option>
<option label="Ph.D - History/Fine Arts" value="390">Ph.D - History/Fine Arts</option>
<option label="Ph.D - Instrumentation" value="391">Ph.D - Instrumentation</option>
<option label="Ph.D - International Studies" value="404">Ph.D - International Studies</option>
<option label="Ph.D - Life Science" value="405">Ph.D - Life Science</option>
<option label="Ph.D - Literature" value="392">Ph.D - Literature</option>
<option label="Ph.D - Management" value="403">Ph.D - Management</option>
<option label="Ph.D - Mathematics" value="393">Ph.D - Mathematics</option>
<option label="Ph.D - Medicine" value="394">Ph.D - Medicine</option>
<option label="Ph.D - Mining" value="395">Ph.D - Mining</option>
<option label="Ph.D - Philosophy" value="396">Ph.D - Philosophy</option>
<option label="Ph.D - Physics" value="397">Ph.D - Physics</option>
<option label="Ph.D - Political Science" value="398">Ph.D - Political Science</option>
<option label="Ph.D - Pharmacy" value="399">Ph.D - Pharmacy</option>
<option label="Ph.D - Statistics" value="478">Ph.D - Statistics</option>
<option label="Ph.D - Sociology" value="408">Ph.D - Sociology</option>
<option label="Ph.D - Tamil" value="400">Ph.D - Tamil</option>
<option label="Ph.D - Visual Arts" value="401">Ph.D - Visual Arts</option>
<option label="Ph.D - Zoology" value="402">Ph.D - Zoology</option>
<option label="Ph.D - Other Doctorate" value="409">Ph.D - Other Doctorate</option>
<option label="Ph.D - Other" value="479">Ph.D - Other</option>
<option label="SSLC - S.S.L.C" value="410">SSLC - S.S.L.C</option>
<option label="SSLC - Other" value="411">SSLC - Other</option>
<option label="XII - XII" value="412">XII - XII</option>
<option label="XII - Other" value="413">XII - Other</option>
<option label="Other - Other" value="414">Other - Other</option>
<option label="Aviation - ALTP" value="480">Aviation - ALTP</option>
<option label="Aviation - CPL" value="481">Aviation - CPL</option>
<option label="Aviation - PPL" value="482">Aviation - PPL</option>
<option label="B.Plan - Planning" value="483">B.Plan - Planning</option>
<option label="B.Plan - Other" value="484">B.Plan - Other</option>
<option label="BGL - Law" value="485">BGL - Law</option>
<option label="BGL - Other" value="486">BGL - Other</option>
<option label="MCM - Computers Management" value="487">MCM - Computers Management</option>
<option label="MCM - Other" value="488">MCM - Other</option>
                                          </select>
                                       </div>
                                    </div>
									
									<div class="control-group">
                                       <label class="control-label">Industry</label>
                                       <div class="controls">
                                          <select tabindex="1" class="large m-wrap" name="job[industry]">
                                             <option label="Fresh Graduate/ No Industry Experience" value="1">Fresh Graduate/ No Industry Experience</option>
<option label="Accounting / Audit / Tax Services" value="2">Accounting / Audit / Tax Services</option>
<option label="Advertising / Event Management / PR / MR" value="3">Advertising / Event Management / PR / MR</option>
<option label="Agriculture / Dairy / Fishing" value="4">Agriculture / Dairy / Fishing</option>
<option label="Architecture / Interior Design" value="6">Architecture / Interior Design</option>
<option label="Automotive / Ancillaries" value="7">Automotive / Ancillaries</option>
<option label="Banking &amp; Financial Services" value="8">Banking &amp; Financial Services</option>
<option label="BPO / KPO / ITES / CRM / Transcription / E Learning" value="16">BPO / KPO / ITES / CRM / Transcription / E Learning</option>
<option label="Chemicals / Polymer / Plastic / Rubber" value="10">Chemicals / Polymer / Plastic / Rubber</option>
<option label="Computer Hardware" value="11">Computer Hardware</option>
<option label="Construction / Cement / Metals" value="12">Construction / Cement / Metals</option>
<option label="Consulting / Market Research" value="14">Consulting / Market Research</option>
<option label="Courier / Freight / Transportation" value="15">Courier / Freight / Transportation</option>
<option label="Data Processing Services" value="17">Data Processing Services</option>
<option label="Education / Training" value="18">Education / Training</option>
<option label="Employment Services / Recruitment Services" value="21">Employment Services / Recruitment Services</option>
<option label="Engineering Services" value="22">Engineering Services</option>
<option label="Entertainment / Media / Publishing" value="23">Entertainment / Media / Publishing</option>
<option label="Fashion / Beauty / Fitness / Accessories" value="24">Fashion / Beauty / Fitness / Accessories</option>
<option label="Fertilizer/ Pesticides" value="49">Fertilizer/ Pesticides</option>
<option label="FMCG / Consumer Durables" value="13">FMCG / Consumer Durables</option>
<option label="Food &amp; Beverages" value="25">Food &amp; Beverages</option>
<option label="Gems &amp; Jewellery" value="50">Gems &amp; Jewellery</option>
<option label="Government/ PSU/ Defence" value="51">Government/ PSU/ Defence</option>
<option label="Health Care / Medical Services / Hospitals" value="26">Health Care / Medical Services / Hospitals</option>
<option label="Hospitality / Airlines / Travel / Tourism" value="27">Hospitality / Airlines / Travel / Tourism</option>
<option label="Import / Export" value="52">Import / Export</option>
<option label="Information Technology" value="28">Information Technology</option>
<option label="Insurance" value="29">Insurance</option>
<option label="Internet / Dotcom" value="30">Internet / Dotcom</option>
<option label="Leather" value="31">Leather</option>
<option label="Legal / Law" value="32">Legal / Law</option>
<option label="Logistics / Supply Chain" value="33">Logistics / Supply Chain</option>
<option label="Manufacturing / Industrial / Production/ Machinery" value="34">Manufacturing / Industrial / Production/ Machinery</option>
<option label="NGO / Social Services" value="36">NGO / Social Services</option>
<option label="Office Supplies / Office Maintenance" value="37">Office Supplies / Office Maintenance</option>
<option label="Others" value="48">Others</option>
<option label="Paints" value="53">Paints</option>
<option label="Paper" value="38">Paper</option>
<option label="Petroleum / Oil &amp; Gases / Energy &amp; Utilities" value="39">Petroleum / Oil &amp; Gases / Energy &amp; Utilities</option>
<option label="Pharmaceuticals / Biotechnology / Clinical Research" value="40">Pharmaceuticals / Biotechnology / Clinical Research</option>
<option label="Printing / Packaging" value="54">Printing / Packaging</option>
<option label="Real Estate / Property" value="41">Real Estate / Property</option>
<option label="Retail / Wholesale" value="42">Retail / Wholesale</option>
<option label="Security / Protection Services" value="43">Security / Protection Services</option>
<option label="Semiconductors &amp; Electrical" value="20">Semiconductors &amp; Electrical</option>
<option label="Shipping / Marine" value="44">Shipping / Marine</option>
<option label="Telecom / ISP" value="45">Telecom / ISP</option>
<option label="Textile / Garments" value="55">Textile / Garments</option>
<option label="Warehousing" value="47">Warehousing</option>
<option label="Wood and Fibre" value="56">Wood and Fibre</option>
                                          </select>
                                       </div>
                                    </div>
									
									<div class="control-group">
                                       <label class="control-label">Annual Salary</label>
                                       <div class="controls">
                                          <select tabindex="1" class="large m-wrap" name="job[salary]">
                                             <option value="Less than 50000" <?=$job_data['salary'] == "Less than 50000" ? 'selected="selected"' : '' ; ?>>Less than 50000</option>
	 <option value="50,000-2,00,000" <?=$job_data['salary'] == "50,000-2,00,000" ? 'selected="selected"' : '' ; ?>>50,000-2,00,000</option>
	 <option value="2,00,000-5,00,000" <?=$job_data['salary'] == "2,00,000-5,00,000" ? 'selected="selected"' : '' ; ?>>2,00,000-5,00,000</option>
	 <option value="5,00,000-7,50,000" <?=$job_data['salary'] == "5,00,000-7,50,000" ? 'selected="selected"' : '' ; ?>>5,00,000-7,50,000</option>
	 <option value="7,50,000-10,00,000" <?=$job_data['salary'] == "7,50,000-10,00,000" ? 'selected="selected"' : '' ; ?>>7,50,000-10,00,000</option>
	 <option value="10,00,000 and above" <?=$job_data['salary'] == "10,00,000 and above" ? 'selected="selected"' : '' ; ?>>10,00,000 and above</option>
											 </select>
                                       </div>
                                    </div>
									
									<div class="control-group">
                                       <label class="control-label">About Company</label>
                                       <div class="controls">
                                          <textarea rows="3" class="large m-wrap" name="job[company_profile]"><?=$job_data['company_profile']; ?></textarea>
                                       </div>
                                    </div>
									
									<div class="control-group">
                                       <label class="control-label">Employment Type</label>
                                       <div class="controls">
                                          <select tabindex="1" class="large m-wrap" name="job[employment_type]">
<option value="">--Select--</option>
					<option label="Permanent" value="1" selected="selected">Permanent</option>
<option label="Temporary/Contractual" value="2">Temporary/Contractual</option>
<option label="Freelancer" value="3">Freelancer</option>                                          </select>
                                       </div>
                                    </div>
									
									<div class="control-group">
                                       <label class="control-label">Employment Status</label>
                                       <div class="controls">
                                          <label class="radio">
                                          <div class="radio" id="uniform-undefined">
										  <span <? if($job_data['employment_status']=='parttime') {?> class="checked" <? } ?>>
										  <input type="radio" value="fulltime" name="job[employment_status]" style="opacity: 0;">
										  </span>
										  </div>
                                          Full Time
                                          </label>
                                          <label class="radio">
                                          <div class="radio" id="uniform-undefined"><span <? if($job_data['employment_status']=='fulltime') {?> class="checked" <? } ?>>
										  <input type="radio" checked="" value="parttime" name="job[employment_status]" style="opacity: 0;">
										  </span>
										  </div>
                                          Part Time
                                          </label>  
                                       </div>
                                    </div>
									
									<div class="control-group">
                              <label class="control-label">Expiry Date</label>
                              <div class="controls">
                                 <input type="text" value="<?=$job_data['date_expiry']; ?>" size="16" class="m-wrap m-ctrl-medium date-picker" name="job[date_expiry]">
                              </div>
                           </div>
									
									
                                    <div class="form-actions">
                                       <button class="btn blue" type="submit"><i class="icon-ok"></i>
									   Post This Job
                                       </button>
                                    </div>
                                 </form>
                                 <!-- END FORM-->                
                              </div>
                           </div>
                        </div>
</div>
