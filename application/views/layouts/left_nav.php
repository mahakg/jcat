<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        	
			<ul>
				<li>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<li>
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<form class="sidebar-search">
						<div class="input-box">
							<a class="remove" href="javascript:;"></a>
							<input type="text" placeholder="Search...">				
							<input type="button" value=" " class="submit">
						</div>
					</form>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<?
				$tab['home'] = $tab['test'] = $tab['invite'] = $tab['jobs'] = $tab['myjcat'] = $tab['badges'] = "";
				
				$module = $this->uri->segment(1);
				$tab[$module] = 'active';
				
				$user_type = $this->session->userdata('user_type');
				
				 ?>
				
				<li class="start <?=$tab['home']?> ">
					<a href="<?=site_url('/home/index');?>">
					<i class="icon-home"></i> 
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>
				<? if($user_type=='recruiter'){ ?>
				<li class="has-sub <?=$tab['test']?> ">
					<a href="<?=site_url('/test/index');?>">
					<i class="icon-bookmark-empty"></i> 
					<span class="title">Manage Test Profiles</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="<?=site_url('/test/create');?>">Create Test</a></li>
						<li><a href="<?=site_url('/test/manage');?>">Manage Test</a></li>
						<li><a href="<?=site_url('/test/viewprofiles');?>">View Test Profiles</a></li>
						<li><a href="<?=site_url('/test/newopportunities');?>">Add New Oppotunities</a></li>
						<li><a href="<?=site_url('/test/viewjobs');?>">View Posted Jobs</a></li>
					</ul>
				</li>
				<? } ?>
				<? if($user_type=='candidate'){ ?>
				<li class="has-sub <?=$tab['jobs']?>">
					<a href="<?=site_url('/jobs/index');?>">
					<i class="icon-th-list"></i> 
					<span class="title">Job-Section</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="<?=site_url('/jobs/search');?>">Search New Opportunities</a></li>
						<li><a href="<?=site_url('/jobs/submitted');?>">Submitted Applications</a></li>
					</ul>
				</li>
				<li class="has-sub <?=$tab['myjcat']?>">
					<a href="<?=site_url('/myjcat/index');?>">
					<i class="icon-th-list"></i> 
					<span class="title">My Skills</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="<?=site_url('/myjcat/test');?>">Test Your Skills</a></li>
						<li><a href="<?=site_url('/myjcat/scorecards');?>">View J-Cat Score card</a></li>
					</ul>
				</li>
				<li class="has-sub <?=$tab['badges']?>">
					<a href="<?=site_url('/badges/index');?>">
					<i class="icon-map-marker"></i> 
					<span class="title">My Badges</span>
					<span class="arrow "></span>
					</a>
					<!--<ul class="sub">
						<li><a href="maps_google.html">Google Maps</a></li>
						<li><a href="maps_vector.html">Vector Maps</a></li>
					</ul>-->
				</li>
				<? } ?>
				<li class="has-sub <?=$tab['invite']?>">
					<a href="<?=site_url('/invite/index');?>">
					<i class="icon-table"></i> 
					<span class="title">Invite Friends</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="<?=site_url('/invite/index');?>">Invite Your Facebook Friends</a></li>
						<li><a href="<?=site_url('/invite/email');?>">Send Invitation Email</a></li>
					</ul>
				</li>
				
				<!--
				<li class="">
					<a href="charts.html">
					<i class="icon-bar-chart"></i> 
					<span class="title">Visual Charts</span>
					</a>
				</li>
				<li class="">
					<a href="calendar.html">
					<i class="icon-calendar"></i> 
					<span class="title">Calendar</span>
					</a>
				</li>
				<li class="">
					<a href="gallery.html">
					<i class="icon-camera"></i> 
					<span class="title">Gallery</span>
					</a>
				</li>
				<li class="has-sub <?=$tab[$module]?>">
					<a href="javascript:;">
					<i class="icon-briefcase"></i> 
					<span class="title">Extra</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="extra_profile.html">User Profile</a></li>
						<li><a href="extra_faq.html">FAQ</a></li>
						<li><a href="extra_search.html">Search Results</a></li>
						<li><a href="extra_invoice.html">Invoice</a></li>
						<li><a href="extra_pricing_table.html">Pricing Tables</a></li>
						<li><a href="extra_404.html">404 Page</a></li>
						<li><a href="extra_500.html">500 Page</a></li>
						<li><a href="extra_blank.html">Blank Page</a></li>
						<li><a href="extra_full_width.html">Full Width Page</a></li>
					</ul>
				</li>
				<li class="">
					<a href="login.html">
					<i class="icon-user"></i> 
					<span class="title">Login Page</span>
					</a>
				</li>-->
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
