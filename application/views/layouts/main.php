<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Rate Your Skill</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="<?=asset_url(); ?>css/bootstrap.min.css" rel="stylesheet" />
		
	<link href="<?=asset_url(); ?>css/metro.css" rel="stylesheet" />
	<link href="<?=asset_url(); ?>css/bootstrap-responsive.min.css" rel="stylesheet" />

	<link href="<?=asset_url(); ?>css/font-awesome.css" rel="stylesheet" />
	<link href="<?=asset_url(); ?>css/style.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?=asset_url(); ?>css/chosen.css" />
	<link href="<?=asset_url(); ?>css/style_responsive.css" rel="stylesheet" />
	<link href="<?=asset_url(); ?>css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="<?=asset_url(); ?>jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" href="<?=asset_url(); ?>gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="<?=asset_url(); ?>uniform/css/uniform.default.css" />
	<!--<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />-->
	<link rel="shortcut icon" href="favicon.ico" />
	<script src="<?=asset_url(); ?>js/jquery-1.8.3.min.js"></script>
	
	   <script type="text/javascript" src="<?=asset_url(); ?>js/chosen.jquery.min.js"></script>
		
        <script>	
        function openDialog(element_id)
                {
                        $('body').append('<div class="modal-backdrop fade in"></div>');
                        $("#"+element_id).show();
                }

                function removeModel(element_id){

                        $("#"+element_id).hide();
                        $("div[class='modal-backdrop fade in']").remove();
                }	
        </script>	
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<? 
$login = $this->session->userdata('login'); 
$userdata = $this->session->userdata('userdata');
?>
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.html">
                                    <img alt="logo" src="<?=asset_url(); ?>/img/images/logo_1.png" width="125" height="60"  />
				</a>
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
				<img src="<?=asset_url(); ?>img/menu-toggler.png" alt="" />
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<!-- BEGIN TOP NAVIGATION MENU -->
				<? if($login){
				$user_type = $this->session->userdata('user_type');
				 ?>
				<ul class="nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN 	
					<li class="dropdown" id="header_notification_bar">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-warning-sign"></i>
						<span class="badge">6</span>
						</a>
						<ul class="dropdown-menu extended notification">
							<li>
								<p>You have 14 new notifications</p>
							</li>
							<li>
								<a href="javascript:;" onclick="App.onNotificationClick(1)">
								<span class="label label-success"><i class="icon-plus"></i></span>
								New user registered. 
								<span class="time">Just now</span>
								</a>
							</li>
							<li>
								<a href="#">
								<span class="label label-important"><i class="icon-bolt"></i></span>
								Server #12 overloaded. 
								<span class="time">15 mins</span>
								</a>
							</li>
							<li>
								<a href="#">
								<span class="label label-warning"><i class="icon-bell"></i></span>
								Server #2 not respoding.
								<span class="time">22 mins</span>
								</a>
							</li>
							<li>
								<a href="#">
								<span class="label label-info"><i class="icon-bullhorn"></i></span>
								Application error.
								<span class="time">40 mins</span>
								</a>
							</li>
							<li>
								<a href="#">
								<span class="label label-important"><i class="icon-bolt"></i></span>
								Database overloaded 68%. 
								<span class="time">2 hrs</span>
								</a>
							</li>
							<li>
								<a href="#">
								<span class="label label-important"><i class="icon-bolt"></i></span>
								2 user IP blocked.
								<span class="time">5 hrs</span>
								</a>
							</li>
							<li class="external">
								<a href="#">See all notifications <i class="m-icon-swapright"></i></a>
							</li>
						</ul>
					</li>
					<!-- END NOTIFICATION DROPDOWN -->
					<!-- BEGIN INBOX DROPDOWN
					<li class="dropdown" id="header_inbox_bar">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-envelope-alt"></i>
						<span class="badge">5</span>
						</a>
						<ul class="dropdown-menu extended inbox">
							<li>
								<p>You have 12 new messages</p>
							</li>
							<li>
								<a href="#">
								<span class="photo"><img src="<?=asset_url(); ?>img/avatar2.jpg" alt="" /></span>
								<span class="subject">
								<span class="from">Lisa Wong</span>
								<span class="time">Just Now</span>
								</span>
								<span class="message">
								Vivamus sed auctor nibh congue nibh. auctor nibh
								auctor nibh...
								</span>  
								</a>
							</li>
							<li>
								<a href="#">
								<span class="photo"><img src="<?=asset_url(); ?>img/avatar3.jpg" alt="" /></span>
								<span class="subject">
								<span class="from">Richard Doe</span>
								<span class="time">16 mins</span>
								</span>
								<span class="message">
								Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh
								auctor nibh...
								</span>  
								</a>
							</li>
							<li>
								<a href="#">
								<span class="photo"><img src="<?=asset_url(); ?>img/avatar1.jpg" alt="" /></span>
								<span class="subject">
								<span class="from">Bob Nilson</span>
								<span class="time">2 hrs</span>
								</span>
								<span class="message">
								Vivamus sed nibh auctor nibh congue nibh. auctor nibh
								auctor nibh...
								</span>  
								</a>
							</li>
							<li class="external">
								<a href="#">See all messages <i class="m-icon-swapright"></i></a>
							</li>
						</ul>
					</li>-->
					<!-- END INBOX DROPDOWN -->
					<!-- BEGIN TODO DROPDOWN 
					<li class="dropdown" id="header_task_bar">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-tasks"></i>
						<span class="badge">5</span>
						</a>
						<ul class="dropdown-menu extended tasks">
							<li>
								<p>You have 12 pending tasks</p>
							</li>
							<li>
								<a href="#">
								<span class="task">
								<span class="desc">New release v1.2</span>
								<span class="percent">30%</span>
								</span>
								<span class="progress progress-success ">
								<span style="width: 30%;" class="bar"></span>
								</span>
								</a>
							</li>
							<li>
								<a href="#">
								<span class="task">
								<span class="desc">Application deployment</span>
								<span class="percent">65%</span>
								</span>
								<span class="progress progress-danger progress-striped active">
								<span style="width: 65%;" class="bar"></span>
								</span>
								</a>
							</li>
							<li>
								<a href="#">
								<span class="task">
								<span class="desc">Mobile app release</span>
								<span class="percent">98%</span>
								</span>
								<span class="progress progress-success">
								<span style="width: 98%;" class="bar"></span>
								</span>
								</a>
							</li>
							<li>
								<a href="#">
								<span class="task">
								<span class="desc">Database migration</span>
								<span class="percent">10%</span>
								</span>
								<span class="progress progress-warning progress-striped">
								<span style="width: 10%;" class="bar"></span>
								</span>
								</a>
							</li>
							<li>
								<a href="#">
								<span class="task">
								<span class="desc">Web server upgrade</span>
								<span class="percent">58%</span>
								</span>
								<span class="progress progress-info">
								<span style="width: 58%;" class="bar"></span>
								</span>
								</a>
							</li>
							<li>
								<a href="#">
								<span class="task">
								<span class="desc">Mobile development</span>
								<span class="percent">85%</span>
								</span>
								<span class="progress progress-success">
								<span style="width: 85%;" class="bar"></span>
								</span>
								</a>
							</li>
							<li class="external">
								<a href="#">See all tasks <i class="m-icon-swapright"></i></a>
							</li>
						</ul>
					</li>-->
					<!-- END TODO DROPDOWN -->
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img alt="" src="<?php echo $userdata['profileurl'];?>" width="45"  />
						<span class="username"><?=$userdata['name']; ?></span>
						<i class="icon-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a href="<?=site_url('/profile/'. $user_type . '_profile');?>"><i class="icon-user"></i> My Profile</a></li>
							<li><a href="<?=site_url('profile/index'.'/'.$userdata['username']);?>"><i class="icon-calendar"></i> My Social Profile</a></li>
							<li><a href="#"><i class="icon-tasks"></i> My Tasks</a></li>
							<li class="divider"></li>
							<li><a href="<?=site_url('/auth/logout');?>"><i class="icon-key"></i> Log Out</a></li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<? } ?>
				<!-- END TOP NAVIGATION MENU -->	
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<? 
	if($login){
	include_once("left_nav.php"); 
	}
	?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
<div style="margin-top:60px; margin-left: 20px;margin-right: 20px;">			
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
  <?=$content_for_layout?>
</div>  
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		2013 &copy; RateYourSkill.com
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>	
	<![endif]-->
	<!--<script src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>-->
	<script src="<?=asset_url(); ?>js/jquery-1.8.3.min.js"></script>
	<script src="<?=asset_url(); ?>breakpoints/breakpoints.js"></script>
	<script src="<?=asset_url(); ?>jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>	
	   <script src="<?=asset_url(); ?>js/jquery.bootstrap.wizard.min.js"></script>
	
   <script type="text/javascript" src="<?=asset_url(); ?>js/chosen.jquery.min.js"></script>
	<!--<script src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>-->
	<script src="<?=asset_url(); ?>js/bootstrap.min.js"></script>
	<script src="<?=asset_url(); ?>js/jquery.blockui.js"></script>	
	<script src="<?=asset_url(); ?>js/jquery.cookie.js"></script>
	<!--<script src="assets/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>	
	<script src="assets/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>	
	<script src="assets/flot/jquery.flot.js"></script>
	<script src="assets/flot/jquery.flot.resize.js"></script>-->
	<script type="text/javascript" src="<?=asset_url(); ?>gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="<?=asset_url(); ?>uniform/jquery.uniform.min.js"></script>	
	<script type="text/javascript" src="<?=asset_url(); ?>js/jquery.pulsate.min.js"></script>
	<!--<script type="text/javascript" src="<?=asset_url(); ?>bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="<?=asset_url(); ?>bootstrap-daterangepicker/daterangepicker.js"></script>-->
	<script src="<?=asset_url(); ?>js/app.js"></script>
   	<script>
		jQuery(document).ready(function() {	
                    App.setPage("sliders");  // set current page
	//		App.setPage("index");  // set current page
			App.init(); // init the rest of plugins and elements
			
		});
		
	</script>
<script>
function addUserPersonalDetails()
{
	$.ajax({
	type: 'POST',
	url: "<? echo site_url('/users/add_users'); ?>",
	data: $("#personal-details").serialize(),
	dataType: 'json',
	success: function(){
	}
	});
}
function addUserProfileDetails()
{
	$.ajax({
	type: 'POST',
	url: "<? echo site_url('/users/add_users_profile'); ?>",
	data: 	 $("#profile-details").serialize(),
	dataType: 'json',
	success: function(data){
		var status = data.status;
		if(status=='true'){
			var content = data.profile_data;
			$("#tab2").html(content);
                        window.location.reload(true);
		}
	}
	});
}
</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
