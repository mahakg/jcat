<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width, maximum-scale=1, user-scalable=no" />

  <title> Rate Your Skills-India's Next Big Recruiting Solution</title>
  

  <link rel="stylesheet" href="<?=asset_url(); ?>css/foundation.css">
  <link rel="stylesheet" href="<?=asset_url(); ?>css/flexslider.css">
  <link rel="stylesheet" href="<?=asset_url(); ?>css/animate.css">
  <link rel="stylesheet" href="<?=asset_url(); ?>css/home.css">
  <link rel="stylesheet" href="<?=asset_url(); ?>css/responsive.css">
  <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Lily+Script+One' rel='stylesheet' type='text/css'>

  <script src="javascripts/modernizr.foundation.js"></script>
</head>
<body>
<div id="nik-bucket">
<div id="nik-lay">
	<div class="row">
			<div class="nik">
			<div id="logo" class="left animated fadeInDown">
			<img alt="front" src="<?=asset_url(); ?>/img/images/4.png" />
			</div>
			
			<div id="mobile-toggle" class="show-for-small"><i class="entypo list"></i></div>
			<div id="nav-holder" class="right">
				<ul>
					<li><a href="#recruiter" class="scroll">Recruiters</a></li>
					<li><a href="#gallery-scroll" class="scroll">Job-Seeker</a></li>
					<li><a href="#job-seeker" class="scroll">How it works ?</a></li>                                
					<li><a target="_blank" href="../auth/login"><div class="blue-btn">Login</div></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<section id="home">
	<div class="row">

			<div id="home-slider" class="flexslider">
				<ul class="slides">
					<li>
						<div class="six columns animated fadeInUpBig slide-left hide-for-small">
							<img alt="front" src="<?=asset_url(); ?>/img/images/proficient.png" />
						</div>
						
						<div class="six columns animated fadeInRightBig slide-right">
							<h1 class="text-right">Jcat Badge</h1>
							<div class="hr hide-for-small"></div>
							<p class="right text-right  hide-for-small">
								J-Cat Content about the product should be listed here.Images has to be created to attract users.
							</p>
							
							<a href="#recruiter" class="scroll"><div class="btn right">Explore Features</div></a>
						</div>
					</li>
					
					<li>
						<div class="six columns animated fadeInLeftBig slide-left  hide-for-small">
						</div>
						
						<div class="six columns animated fadeInDownBig slide-right">
							<h1 class="text-right">Job Seeker??</h1>
							<div class="hr  hide-for-small"></div>
							<p class="right text-right  hide-for-small">
							Explore J-Cat tests to open your eyes to the power of your own skills. Use what you know to help you get ahead. 
							</p>
							
							<a href="#login" class="scroll"><div class="btn right">Login</div></a>
						</div>
					</li>
					
				
				</ul>
			</div>
		
	</div>
</section>

<a id="recruiter"></a>
<section id="feature-list" class="even-section">

	<div class="row inner-section section-preamble text-center animated fadeInDown">
		<h2>Total Hiring Clarity, Achieved</h2>
		
		<p>
                      Choose from hundreds of skill tests to achieve total clarity about your candidates.
		</p>
		<div class="hr"></div>
	</div>
	
	<div class="row feature-row inner-section flyIn">
		<div class="three columns feature text-center">
			<img alt="Feature Icon" src="<?=asset_url(); ?>/img/images/icons/create_skillset.png" />
			<h4 class="alt-h">Create a Skill Test</h4>
			<p>
                             A Skill-Set represents a cluster of skills you're looking for in job applicants for a specific role. Select up to five skills you want to test in candidates.
			</p>
		</div>
		
		<div class="three columns feature text-center">
			<img alt="Feature Icon" src="<?=asset_url(); ?>/img/images/icons/share_skillset.png" />
			<h4 class="alt-h">Share Skill Test</h4>
			<p>
                            Include a link to your Skill-Set in a job post, or email the link to your job candidates to test their skills.
			</p>
		</div>
		
		<div class="three columns feature text-center">
			<img alt="Feature Icon" src="<?=asset_url(); ?>/img/images/icons/test_skillset.png" />
			<h4 class="alt-h">Test Candidates</h4>
			<p>
				Candidates take each of the tests in your SkillSet, verifying their skills in as few as 10 questions.
			</p>
		</div>
		
		<div class="three columns feature text-center">
			<img alt="Feature Icon" src="<?=asset_url(); ?>/img/images/icons/ranked_skillset.png" />
			<h4 class="alt-h">Review Candidates</h4>
			<p>
				View and sort candidate scores in your Skill-Sets dashboard.
			</p>
		</div>
	</div>


</section>

<a id="gallery-scroll"></a>
<section id="features-detail" class="odd-section">

	<div class="row inner-section section-preamble text-center flyIn">
		<h2>GET YOUR SCORES!!!!</h2>
		
		<p>
			Start Browsing 1000 of Test and Get Scores.
		</p>
		<div class="hr"></div>
	</div>
	
	<div class="row inner-section platform-row">
	
		<div class="six columns flyLeft text-center">
			<div class="flexslider">
				<ul class="slides">
					<li>
						<img alt="desktop" src="<?=asset_url(); ?>/img/images/getscores.png" />
					</li>
					
					
					
				</ul>
			</div>
		</div>
		
		<div class="six columns flyRight text-right">
			<h2 class="text-right">Answer Multiple Choice Questions</h2>
			<div class="hr"></div>
			<p class="text-right">
				Jcat's adaptive scoring algorithm can determine your skill in any digital technology in just 25 questions in under 60 seconds!
			</p>
			<div class="blue-btn">Get Scores</div>
		</div>
		
	
	</div>
	
	<div class="mid-row">
		<div class="row inner-section platform-row">
		
	
			<div class="six columns flyLeft text-left">
				<h2 class="text-left">Answer Questions</h2>
				<div class="hr"></div>
				<p class="text-left">
					Choose from hundreds of tests in skill areas that matter. 
				</p>
				<div class="green-btn">Take Test</div>
			</div>
			
			<div class="six columns flyRight text-center">
				<div class="flexslider">
					<ul class="slides">
						<li>
							<img alt="desktop" src="<?=asset_url(); ?>/img/images/question.png" />
						</li>
					</ul>
				</div>
			</div>
			
		
		</div>
	</div>
	
	<a id="job-seeker"></a>
<section id="feature-list" class="even-section">

	<div class="row inner-section section-preamble text-center animated fadeInDown">
		<h2>What Is Rate Your Skill ?</h2>
		
		<p>
                See what we are ?
		</p>
		<div class="hr"></div>
	</div>
	
	<div class="row feature-row inner-section flyIn">
		<div class="three columns feature text-center">
			<img alt="Feature Icon" src="<?=asset_url(); ?>/img/images/crowdsourced_tests.png" />
			<h4 class="alt-h">Crowd Tests</h4>
			<p>
                    Anyone can add tests and questions, so our tests stay up-to-date even on rapidly-changing subjects.
			</p>
		</div>
		
		<div class="three columns feature text-center">
			<img alt="Feature Icon" src="<?=asset_url(); ?>/img/images/validated_questions.png" />
			<h4 class="alt-h">Validated Questions</h4>
			<p>
				New questions start in the nursery, where user feedback helps get them ready to fly. Mature questions have a stronger impact on your score.	
			</p>
		</div>
		
		<div class="three columns feature text-center">
			<img alt="Feature Icon" src="<?=asset_url(); ?>/img/images/simple_scoring.png" />
			<h4 class="alt-h">Simple Fast Scoring</h4>
			<p>
				J-Cat's adaptive scoring algorithm can determine your skill in any digital technology in just 25 questions in under 60 seconds!
			</p>
		</div>
		
		<div class="three columns feature text-center">
			<img alt="Feature Icon" src="<?=asset_url(); ?>/img/images/badges.png" />
			<h4 class="alt-h">Badges</h4>
			<p>
				Users receive badges that represent their skill level and achievement. Share badges wherever you are building your professional identity online.
			</p>
		</div>
	</div>

</section>
<section>
	<div class="row inner-section platform-row">
	
		<div class="six columns flyLeft text-center">
			<div class="flexslider">
				<ul class="slides">
				
					<li>
						<img alt="desktop" src="<?=asset_url(); ?>/img/images/choose_your_level.png" />
					</li>
				</ul>
			</div>
		</div>
		
		<div class="six columns flyRight text-right">
			<h2 class="text-right">Show your Level</h2>
			<div class="hr"></div>
			<p class="text-right">
				Start taking test and Show your proficiency in the test. 
			</p>
			<div class="green-btn">Start Test</div>
		</div>
		
	
	</div>

</section>

<section id="newsletter">

	<div class="row inner-section">
		<div class="seven columns">
			<i class="entypo newspaper">&nbsp;</i>
			<div id="newsletter-text">
				<h3 class="text-white">Keep up to date with our latest Jobs and Internship Notifications!</h3>	
			</div>
		</div>
	
	
		<div class="five columns">
			<div class="details-error-wrap hide">Please enter a valid email address</div>
			<input class="newsletter-email left" type="text" placeholder="Your Email Address" />
			<input class="newsletter-btn left btn form-send" type="submit" value="Subscribe" />
			<div class="hide green-btn newsletter-btn left btn form-sent">Subscribed</div>
		</div>
	
	</div>
	
</section>

<section id="footer">
	<div class="row inner-section">
	
		<div class="three columns">
			<h5 class="footer-title">NAVIGATE</h5>
			
			<ul>
				<li><a href="#recruiter" class="scroll">Recruiter</a></li>
				<li><a href="#gallery-scroll" class="scroll">Job-Seeker</a></li>
			</ul>
		</div>
		
		<div class="three columns">
			<h5 class="footer-title">ABOUT US</h5>
			
			<p>
				Content Writers are working on it :P
		    </p>
		</div>
		
		<div class="three columns">
			<h5 class="footer-title">STAY INFORMED</h5>
			
			<div class="details-error-wrap hide">Please enter a valid email address</div>
			<input class="newsletter-email left" type="text" placeholder="Your Email Address" />
			<input class="newsletter-btn right btn form-send" type="submit" value="Subscribe" />
			<div class="hide green-btn newsletter-btn right btn form-sent">Subscribed</div>
			
		</div>
		
		<div class="three columns text-right">
			<h5 class="footer-title">CONTACT</h5>
			<div class="mail-link">
				<i class="entypo mail text-white"></i>
				<a href="mailto:nikhiltuteja.com">xyz@jcat.com</a>
				
			</div>
			<div class="footer-social">
				<a href="#"><i class="entypo-social facebook text-white"></i></a>
				<a href="#"><i class="entypo-social twitter text-white"></i></a>
				<a href="#"><i class="entypo-social behance text-white"></i></a>
				<a href="#"><i class="entypo-social dribbble text-white"></i></a>
				<a href="#"><i class="entypo-social vimeo text-white"></i></a>
			</div>
			<h3 class="footer-title">Jcat-Recruiting Software</h3>
			
		</div>
	
	</div>
</section>

</div>

  
  <!-- Included JS Files (Compressed) -->
  <script src="<?=asset_url(); ?>js/foundation.min.js"></script>
  
  <!-- Initialize JS Plugins -->
  <script src="<?=asset_url(); ?>js/jquery.flexslider-min.js"></script>
  <script src="<?=asset_url(); ?>js/inview.js"></script>
  <script src="<?=asset_url(); ?>js/smooth-scroll.js"></script>
  <script src="<?=asset_url(); ?>js/scripts.js"></script>
  

  
</body>
</html>
