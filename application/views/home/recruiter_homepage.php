<div class="container-fluid" style>
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN STYLE CUSTOMIZER -->
		<div class="color-panel hidden-phone">
			<div class="color-mode-icons icon-color"></div>
			<div class="color-mode-icons icon-color-close"></div>
			<div class="color-mode">
				<p>THEME COLOR</p>
				<ul class="inline">
					<li data-style="default" class="color-black current color-default"></li>
					<li data-style="blue" class="color-blue"></li>
					<li data-style="brown" class="color-brown"></li>
					<li data-style="purple" class="color-purple"></li>
					<li data-style="light" class="color-white color-light"></li>
				</ul>
				<label class="hidden-phone">
				<input type="checkbox" value="" checked="" class="header">
				<span class="color-mode-label">Fixed Header</span>
				</label>							
			</div>
		</div>
		<!-- END BEGIN STYLE CUSTOMIZER -->   	
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
		<h3 class="page-title">
			Dashboard				
			<small>statistics and more</small>
		</h3>
		<ul class="breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="index.html">Home</a> 
				<i class="icon-angle-right"></i>
			</li>
			<li><a href="#">Dashboard</a></li>
			<li class="pull-right no-text-shadow">
				<div data-original-title="Change dashboard date range" data-placement="top" data-desktop="tooltips" data-tablet="" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" id="dashboard-report-range">
					<i class="icon-calendar"></i>
					<span></span>
					<i class="icon-angle-down"></i>
				</div>
			</li>
		</ul>
		<!-- END PAGE TITLE & BREADCRUMB-->
	</div>
</div>
<!-- END PAGE HEADER-->
<div id="dashboard">
	<!-- BEGIN DASHBOARD STATS -->
	<div class="row-fluid">
		<div data-desktop="span3" data-tablet="span6" class="span3 responsive">
			<div class="dashboard-stat blue">
				<div class="visual">
                                    <i class="icon-bookmark"></i>
				</div>
				<div class="details">
					<div class="number">
						250
					</div>
					<div class="desc">									
						New Skills Added
					</div>
				</div>
                                    
			</div>
		</div>
		<div data-desktop="span3" data-tablet="span6" class="span3 responsive">
			<div class="dashboard-stat green">
				<div class="visual">
                                    <i class="icon-trophy"></i>   
				</div>
				<div class="details">
					<div class="number">2000</div>
					<div class="desc">New Badges Earned</div>
				</div>
										
			</div>
		</div>
		<div data-desktop="span3" data-tablet="span6  fix-offset" class="span3 responsive">
			<div class="dashboard-stat purple">
				<div class="visual">
                                    <i class="icon-user"></i>
				</div>
				<div class="details">
					<div class="number">14000</div>
					<div class="desc">Test Takers</div>
				</div>
									
			</div>
		</div>
		<div data-desktop="span3" data-tablet="span6" class="span3 responsive">
			<div class="dashboard-stat yellow">
				<div class="visual">
					<i class="icon-bullhorn"></i>
				</div>
				<div class="details">
					<div class="number">200</div>
					<div class="desc">Job Posted</div>
				</div>
										
			</div>
		</div>
	</div>
	<!-- END DASHBOARD STATS -->
	<div class="clearfix"></div>
	<div class="row-fluid">
		<div class="span6">
			<!-- BEGIN PORTLET-->
			<div class="portlet solid bordered light-grey">
				<div class="portlet-title">
					<h4><i class="icon-bar-chart"></i>View Your Posted Jobs</h4>
													<table id="sample_editable_1" class="table table-striped table-hover table-bordered dataTable" aria-describedby="sample_editable_1_info">
									<thead>
										<tr role="row">
										<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 100px;" aria-sort="ascending" aria-label="Username: activate to sort column descending">Job id</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 238px;" aria-label="Full Name: activate to sort column ascending">Job Title</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 116px;" aria-label="Points: activate to sort column ascending">Key Skills</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 116px;" aria-label="Points: activate to sort column ascending">Salary</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 167px;" aria-label="Notes: activate to sort column ascending">Status</th>
										</tr>
									</thead>
									
								<tbody role="alert" aria-live="polite" aria-relevant="all"><tr class="odd">
 
<?php foreach ($view_jobs as $row)
{ ;?> 
<tr>
<td><?php echo $row['id'];?></td>
<td><?php echo $row['job_title'];?></td>
<td><?php echo $row['key_skills'];?></td>
<td><?php echo $row['salary'];?></td>
<td><?php echo $row['status'];?></td>


</tr>
<?php } ?>
	
											
											
											
										</tbody></table>
				</div>
				
			</div>                                    <div id="popular">
                                        <h3 class="form-section">Most Popular Skills</h3>
				
<div class="row-fluid">
						
                                                <div class="span2">
                                                    <div class="item">
							<a href="assets/img/gallery/image1.jpg" title="Photo" data-rel="fancybox-button" class="fancybox-button">
                                                            <img alt="maths" src="<?=asset_url(); ?>img/icons/basic_maths.png" width="60" height="60" />							
                                                        </a>
                                                    </div>
                                                    <div class="test_info"> 
		 					<h4><a _url_slug="basic-math" href="http://rateyourskill.com/beta/index.php/myjcat/skill_desc?skill_id=8" class="skill-name">Basic Math</a></h4>
		 					<p></p>
		 					<p>20,987 testers</p>
                                                    </div>
						</div>
						
                                                <div class="span2">
                                                    <div class="item">
                                                        <a href="assets/img/gallery/image1.jpg" title="Photo" data-rel="fancybox-button" class="fancybox-button">
                                                            <img alt="php" src="<?=asset_url(); ?>img/icons/php.png" width="60" height="60" />							
							</a>
                                                    </div>
                                                    <div class="test_info"> 
		 					<h4><a _url_slug="php" href="http://rateyourskill.com/beta/index.php/myjcat/skill_desc?skill_id=1" class="skill-name">PHP</a></h4>
		 					<p></p>
		 					<p>20,987 testers</p>
                                                    </div>
						</div>
									
						<div class="span2">
                                                    <div class="item">
							<a href="assets/img/gallery/image2.jpg" title="Photo" data-rel="fancybox-button" class="fancybox-button">
								<img alt="html" src="<?=asset_url(); ?>img/icons/excel.png" width="60" height="60" />										
							</a>
                                                    </div>
                                                    
                                                    <div class="test_info"> 
		 					<h4><a _url_slug="basic-math" href="http://rateyourskill.com/beta/index.php/myjcat/skill_desc?skill_id=14" class="skill-name">Excel</a></h4>
		 					<p></p>
		 					<p>16,754 testers</p>
                                                    </div>
                                                </div>
						
                                                <div class="span2">
                                                    <div class="item">
							<a href="assets/img/gallery/image3.jpg" title="Photo" data-rel="fancybox-button" class="fancybox-button">
                                                        	<img alt="logo" src="<?=asset_url(); ?>img/icons/icon_html.png" width="60" height="60"/>										
							</a>
                                                    </div>
                                                    
                                                    <div class="test_info"> 
		 					<h4><a _url_slug="logo-design" href="http://rateyourskill.com/beta/index.php/myjcat/skill_desc?skill_id=10" class="skill-name">HTML</a></h4>
		 					<p></p>
		 					<p>678 testers</p>
                                                    </div>
						</div>
						
                                                <div class="span2">
                                                    <div class="item">
							<a href="assets/img/gallery/image4.jpg" title="Photo" data-rel="fancybox-button" class="fancybox-button">
								<img alt="mysql" src="<?=asset_url(); ?>img/icons/mysql.png" width="60" height="60"/>										
							</a>
                                                    </div>
                                                    <div class="test_info"> 
                                                            <h4><a _url_slug="mysql" href="http://rateyourskill.com/beta/index.php/myjcat/skill_desc?skill_id=5" class="skill-name">MySQl</a></h4>
                                                            <p></p>
                                                            <p>19,002
                                                            testers</p>
                                                    </div>
						</div>
					</div>
                                    </div>
			<!-- END PORTLET-->
		</div>

		<div class="span6">
			<!-- BEGIN PORTLET-->
                      <div class="box-fluid" style="margin-top:-20px;margin-bottom:10px;float:left">
                                  <h4 class="box-header round-top" style="size:10px;padding:7px 7px;list-style:none"> Site Activity
                                  </h4>         
                                  <div id="ticker" style="max-height:370px;height:370px;overflow:hidden;overflow-y:auto;float:left;border:1px solid #DDD;width:100%">
                                         <?php foreach ($website_activity as $row)
{ ;?> <div class="feedhead">
                                                      <a target="_blank" href="">
                                                          <div class="row-fluid feed">
                                                              <div class="span2">
                                                              <img src="<?php echo $row['profileurl'];?>" >
                                                              </div>
                                                              <div class="span8" style="margin-top:5px"><?php echo $row['name'];?> &nbsp;&nbsp;&nbsp;has just  &nbsp;&nbsp<?php echo $row['activity_type']; ?> &nbsp; <?php if ($row['module_name']!="invite"){echo "of "."".$row['content_id'];}?>
                                                         </div>
                                                      </a>

                                    </div>
                                 </div><?php } ?>
                      </div>
			
			<!-- END PORTLET-->
		</div>

	<div class="clearfix"></div>
	<div class="row-fluid">
		<div class="span6">
			<!-- BEGIN REGIONAL STATS PORTLET-->
			<div class="portlet">
				<div class="portlet-body">
				</div>
			</div>
			<!-- END REGIONAL STATS PORTLET-->
		</div>
	</div>
	<div class="clearfix"></div>
</div>
</div>
</div>