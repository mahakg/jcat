<?php $alert=$this->session->flashdata('msg');
 if($alert){  
    echo '<script type=\'text/javascript\'>';  
    echo 'alert("'.$alert.'");';  
    echo '</script>';
}  ?>
<div class="portlet box green">
                              <div class="portlet-title">
                                 <h4><i class="icon-reorder"></i>Invite User</h4>
                                 <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="javascript:;" class="reload"></a>
                                    <a href="javascript:;" class="remove"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="<?=site_url('/invite/invite_email');?>" method="post" class="form-horizontal">                         
                                    <h3 class="form-section">Send an Invitation Email</h3>
                                    
                                    <div class="control-group">
													<label class="control-label">Name<span class="required">*</span></label>
													<div class="controls">
														<input type="text" name="invite[name]" class="span6 m-wrap">
														<span class="help-inline">Name</span>
													</div>
												</div>
                                    <div class="control-group">
													<label class="control-label">Email<span class="required">*</span></label>
													<div class="controls">
														<input type="text" name="invite[to_email]" class="span6 m-wrap">
														<span class="help-inline">Email address</span>
													</div>
												</div>
                                    <div class="control-group">
													<label class="control-label">Subject<span class="required">*</span></label>
													<div class="controls">
														<input type="text" name="invite[subject]" class="span6 m-wrap">
														<span class="help-inline">Subject</span>
													</div>
												</div>
                                                                      
                                    <div class="form-actions">
                                       <button type="submit" class="btn blue"> Submit
<i class="m-icon-swapright m-icon-white"></i></button>
                                       
                                    </div>
                                 </form>
                                 <!-- END FORM-->                
                              </div>
                           </div>
