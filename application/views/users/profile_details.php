<div style="margin-left: 60px">
<form action="<?=site_url('/users/register_session');?>" id="register_form" method="post">	
	<div class="tab-pane profile-classic row-fluid active" id="tab_1_2">
		<ul class="unstyled span10">
				<? $user_type = $this->session->userdata('user_type');  ?>
					<li><span>Name:</span><?=$user_complete_profile['name']; ?></li>
					<li><span>User Name:</span> <?=$user_complete_profile['username']; ?></li>
					<li><span>Email:</span> <a href="#"><?=$user_complete_profile['email']; ?></a></li>
					<li><span>About:</span> <?=$user_complete_profile['summary']; ?></li>

				<?	if($user_type=='candidate'){	?>
					<li><span>Current Employer:</span> <?=$user_complete_profile['current_employer']; ?></li>
					<li><span>Current Salary:</span> <?=$user_complete_profile['current_salary']; ?></li>
					<li><span>Designation:</span><?=$user_complete_profile['designation']; ?></li>
					<li><span>Skill Set:</span><?=$user_complete_profile['skill_set']; ?></li>
					<li><span>Experience:</span><?=$user_complete_profile['experience']; ?></li>
					<li><span>Highest Qualification:</span><?=$user_complete_profile['highest_qualification']; ?></li>
					<li><span>Institute/ Organization:</span><?=$user_complete_profile['institute_name']; ?></li>
					<li><span>Batch Year:</span> <?=$user_complete_profile['batch_year']; ?></li>
				<? } elseif($user_type=='recruiter'){ ?>
					<li><span>Company Name:</span><?=$user_complete_profile['company_name']; ?></li>
					<li><span>Company Size:</span> <?=$user_complete_profile['company_size']; ?></li>
					<li><span>About Company:</span> <a href="#"><?=$user_complete_profile['about_company']; ?></a></li>
					<li><span>Domain:</span> <?=$user_complete_profile['domain']; ?></li>
					<li><span>Company Location:</span> <?=$user_complete_profile['company_location']; ?></li>
				<? } ?>
					<li><span>Website/ URL:</span><?=$user_complete_profile['website_url']; ?></li>
					<li><span>Remarks:</span> <?=$user_complete_profile['remarks']; ?></li>
		</ul>
		
		<div class="control-group">
        <div class="controls">
          <label class="checkbox">
          <input type="checkbox" name="tnc"> I agree to the <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a>
          </label>  
          <div id="register_tnc_error"></div>
        </div>
      </div>
	</div>	
	</form>	
</div>
