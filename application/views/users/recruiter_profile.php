<style>
.page-content {
    margin: auto;
    min-height: 860px;
    width: 100%;
}
.row-fluid [class*="span"] {
    -moz-box-sizing: border-box;
    display: block;
    float: left;
    margin-left: 12%;
    min-height: 30px;
    
}
</style>
<div class="container-fluid">
	<div style="width:80%; margin:auto; ">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                     
                  <h3 class="page-title">
                     Welcome to jCat
                  </h3>
                  
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div id="form_wizard_1" class="portlet box blue">
                     <div class="portlet-title">
                        <h4>
                           <i class="icon-reorder"></i> Profile Wizard - <span class="step-title">Step 1 of 3</span>
                        </h4>
                        <div class="tools hidden-phone">
                           <a class="collapse" href="javascript:;"></a>
                           <a class="config" data-toggle="modal" href="#portlet-config"></a>
                           <a class="reload" href="javascript:;"></a>
                           <a class="remove" href="javascript:;"></a>
                        </div>
                     </div>
                     <div class="portlet-body form">
                           <div class="form-wizard">
                              <div class="navbar steps">
                                 <div class="navbar-inner">
                                    <ul class="row-fluid nav nav-pills">
                                       <li class="span3 active">
                                          <a class="step active" data-toggle="tab" href="#tab1">
                                          <span class="number">1</span>
                                          <span class="desc"><i class="icon-ok"></i> Account Setup</span>   
                                          </a>
                                       </li>
                                       <li class="span3">
                                          <a class="step" data-toggle="tab" href="#tab2">
                                          <span class="number">2</span>
                                          <span class="desc"><i class="icon-ok"></i> Profile Setup</span>   
                                          </a>
                                       </li>
                                       <li class="span3">
                                          <a class="step" data-toggle="tab" href="#tab3">
                                          <span class="number">3</span>
                                          <span class="desc"><i class="icon-ok"></i> Profile View</span>   
                                          </a>
                                       </li>
                                       
                                    </ul>
                                 </div>
                              </div>
                              <div class="progress progress-success progress-striped" id="bar">
                                 <div class="bar" style="width: 25%;"></div>
                              </div>
							  <div style="float:left; width: 25%; ">
							  <div class="">
                              <div class="">
                                 <div data-provides="fileupload" class="fileupload fileupload-new">
                                    <div style="width: 200px; height: 150px;" class="fileupload-new thumbnail">
                                    <?
                                    	if(isset($user_data['profileurl']) && $user_data['profileurl']==""){
                                    	$src = asset_url() . 'img/size.gif';
                                    	}
                                    	else{
                                    	$src = $user_data['profileurl'];
                                    	}
                                     ?>
                                       <img alt="" src="<?=$src; ?>" width=100% >
                                    </div>
                                    <div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
                                    <!--<div>
                                       <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                       <span class="fileupload-exists">Change</span>
                                       <input type="file" class="default"></span>
                                       <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a>
                                    </div>-->
                                 </div>
                                 
                              </div>
                           </div> 
							  </div>
                              <div class="tab-content">
                              
                                 <div id="tab1" class="tab-pane active">
                              <form id="personal-details" name="personal-details" method="post">   
                                    <div class="control-group">
                                       <label class="control-label">Name</label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" id="name" name="name" value="<?=$user_data['name']; ?>">
										  <input type="hidden" name="user_type" id="user_type" value="recruiter" />
                                       </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">User Name</label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" id="username" name="username" value="<?=$user_data['username']; ?>">
                                       </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">E-Mail</label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" id="email" name="email" value="<?=$user_data['email']; ?>">
				                    </div>
                                    </div>
									
									
					<div class="control-group">
                                       <label class="control-label">About Me</label>
                                       <div class="controls">
                                          <textarea rows="3" class="span6 m-wrap" id="summary" name="summary"><?=$user_data['summary']; ?></textarea>
					</div>
                                    </div>
                                 </form>   
                                 </div>
                                 
                                 <div id="tab2" class="tab-pane">
                                 <form id="profile-details" name="profile-details" method="post">   
                                    <div class="control-group">
                                       <label class="control-label">Company Name</label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" name="company_name" value="<?=$user_profile_data['company_name'];  ?>">
                                       </div>
                                    </div>
                                    <div class="control-group">
                              <label class="control-label">Company Type</label>
                              <div class="controls">
                                 <select tabindex="1" data-placeholder="Choose a Category" class="span6 m-wrap" name="company_size" >
                                    <option value="5" <?=($user_profile_data['company_size'] == 5 ? 'selected="selected"' : ''); ?>>Large Size</option>
<option value="2" <?= ( $user_profile_data['company_size'] == 2 ? 'selected="selected"' : '' ) ?>>Medium Size </option>
<option value="1" <?= ( $user_profile_data['company_size'] == 1 ? 'selected="selected"' : '' ) ?>>MNC</option>
<option value="6" <?= ( $user_profile_data['company_size'] == 6 ? 'selected="selected"' : '' ) ?>>Non Profit</option>
<option value="3" <?= ( $user_profile_data['company_size'] == 3 ? 'selected="selected"' : '' ) ?>>PSU</option>
<option value="4" <?= ( $user_profile_data['company_size'] == 4 ? 'selected="selected"' : '' ) ?>>Start Up</option>
                                    
                                 </select>
                              </div>
							  </div>
							  <div class="control-group">
                                       <label class="control-label">About Company</label>
                                       <div class="controls">
                                          <textarea rows="3" class="span6 m-wrap" name="about_company"><?=$user_profile_data['about_company'];  ?></textarea>
                                       </div>
                                    </div>
                                    
									<div class="control-group">
                              <label class="control-label">Domain</label>
                              <div class="controls">
                                 <select tabindex="1" data-placeholder="Choose a Category" class="span6 m-wrap" onclick="$('#domain_type').val(this.value);" name="domain">
                                    
					<option value="6">Accounting / Finance </option>
					<option value="7">Advertising / PR / MR / Event Management </option>
					<option value="8">Agriculture / Dairy </option>
					<option value="9">Animation / Gaming </option>
					<option value="10">Architecture / Interior Design </option>
					<option value="3">Automobile / Auto Anciliary / Auto Components </option>
					<option value="11">Aviation / Aerospace Firms </option>
					<option value="5">Banking / Financial Services / Broking </option>
					<option value="2">BPO / Call Centre / ITES </option>
					<option value="12">Brewery / Distillery </option>
					<option value="13">Ceramics / Sanitary ware </option>
					<option value="14">Chemicals / PetroChemical / Plastic / Rubber </option>
					<option value="4">Construction / Engineering / Cement / Metals </option>
					<option value="15">Consumer Electronics / Appliances / Durables </option>
					<option value="16">Courier / Transportation / Freight / Warehousing </option>
					<option value="61">Design/UI/UX</option>
					<option value="59">E-Commerce</option>
					<option value="17">Education / Teaching / Training </option>
					<option value="18">Electricals / Switchgears </option>
					<option value="19">Export / Import </option>
					<option value="20">Facility Management </option>
					<option value="21">Fertilizers / Pesticides </option>
					<option value="22">FMCG / Foods / Beverage </option>
					<option value="23">Food Processing </option>
					<option value="24">Fresher / Trainee / Entry Level </option>
					<option value="25">Gems / Jewellery </option>
					<option value="26">Glass / Glassware </option>
					<option value="27">Government / Defence </option>
					<option value="28">Heat Ventilation / Air Conditioning </option>
					<option value="29">Industrial Products / Heavy Machinery </option>
					<option value="30">Insurance </option>
					<option value="31">Iron and Steel </option>
					<option value="32">IT-Hardware & Networking </option>
					<option value="1">IT-Software / Software Services </option>
					<option value="33">KPO / Research / Analytics </option>
					<option value="34">Legal </option>
					<option value="35">Media / Entertainment / Internet </option>
					<option value="36">Medical / Healthcare / Hospitals </option>
					<option value="37">Mining / Quarrying </option>
					<option value="38">NGO / Social Services / Regulators / Industry Associations </option>
					<option value="39">Office Equipment / Automation </option>
					<option value="40">Oil and Gas / Energy / Power / Infrastructure </option>
					<option value="58">Other </option>
					<option value="42">Pharma / Biotech / Clinical Research </option>
					<option value="43">Printing / Packaging </option>
					<option value="62">Private Equity/Investment Banking</option>
					<option value="44">Publishing </option>
					<option value="41">Pulp and Paper </option>
					<option value="45">Real Estate / Property </option>
					<option value="46">Recruitment / Staffing </option>
					<option value="47">Retail / Wholesale </option>
					<option value="48">Security / Law Enforcement </option>
					<option value="60">Seed Fund/Venture Capital/ Angel Investor</option>
					<option value="49">Semiconductors / Electronics </option>
					<option value="50">Shipping / Marine </option>
					<option value="51">Strategy / Management Consulting Firms </option>
					<option value="52">Telcom / ISP </option>
					<option value="53">Textiles / Garments / Accessories </option>
					<option value="54">Travel / Hotels / Restaurants / Airlines / Railways </option>
					<option value="55">Tyres </option>
					<option value="56">Water Treatment / Waste Management </option>
					<option value="57">Wellness / Fitness / Sports </option>
                                 </select>
                              </div>
                           </div>
                                    
                                    <div class="control-group">
                              <label class="control-label">Company Location</label>
                              <div class="controls">
                                 <input type="text" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;California&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" data-items="4" data-provide="typeahead" class="span6 m-wrap" name="company_location" value="<?=$user_profile_data['company_location'];  ?>">
                                 
                              </div>
                           </div>
						   <div class="control-group">
                                       <label class="control-label">Website URL</label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" name="website_url" value="<?=$user_profile_data['website_url'];  ?>">
                                       </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">Remarks</label>
                                       <div class="controls">
                                          <textarea rows="3" class="span6 m-wrap" name="remarks"><?=$user_profile_data['remarks'];  ?></textarea>
                                       </div>
                                    </div>
                                 </form>
                                 </div>
					<div id="tab3" class="tab-pane">
					</div>
                              <div class="form-actions clearfix">
                                 <a class="btn button-previous" href="javascript:;" style="display: none;">
                                 <i class="m-icon-swapleft"></i> Back 
                                 </a>
                                 <a class="btn blue button-next" href="javascript:;" onclick="if($('#tab1').is(':visible')){addUserPersonalDetails();} if($('#tab2').is(':visible')){ addUserProfileDetails(); } ">
                                 Continue <i class="m-icon-swapright m-icon-white"></i>
                                 </a>
                                 <button class="btn green button-submit" style="display: none;" onclick="$('#register_form').submit();">
                                 Submit <i class="m-icon-swapright m-icon-white"></i>
                                 </button>
                              </div>
                           </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
            </div>
         </div>
