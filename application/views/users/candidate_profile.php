<style>
.page-content {
    margin: auto;
    min-height: 860px;
    width: 100%;
}
.row-fluid [class*="span"] {
    -moz-box-sizing: border-box;
    display: block;
    float: left;
    margin-left: 12%;
    min-height: 30px;
    
}
</style>

<div class="container-fluid">
		<div style="width:80%; margin:auto; ">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                     
                  <h3 class="page-title">
                     Welcome to jCat
                  </h3>
                  
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div id="form_wizard_1" class="portlet box blue">
                     <div class="portlet-title">
                        <h4>
                           <i class="icon-reorder"></i> Profile Wizard - <span class="step-title">Step 1 of 3</span>
                        </h4>
                        <div class="tools hidden-phone">
                           <a class="collapse" href="javascript:;"></a>
                           <a class="config" data-toggle="modal" href="#portlet-config"></a>
                           <a class="reload" href="javascript:;"></a>
                           <a class="remove" href="javascript:;"></a>
                        </div>
                     </div>
                     <div class="portlet-body form">
                           <div class="form-wizard">
							
                              <div class="navbar steps">
                                 <div class="navbar-inner">
                                    <ul class="row-fluid nav nav-pills">
                                       <li class="span3 active">
                                          <a class="step active" data-toggle="tab" href="#tab1">
                                          <span class="number">1</span>
                                          <span class="desc"><i class="icon-ok"></i> Account Setup</span>   
                                          </a>
                                       </li>
                                       <li class="span3">
                                          <a class="step" data-toggle="tab" href="#tab2">
                                          <span class="number">2</span>
                                          <span class="desc"><i class="icon-ok"></i> Profile Setup</span>   
                                          </a>
                                       </li>
                                       <li class="span3">
                                          <a class="step" data-toggle="tab" href="#tab3">
                                          <span class="number">3</span>
                                          <span class="desc"><i class="icon-ok"></i> Profile View</span>   
                                          </a>
                                       </li>
                                       
                                    </ul>
                                 </div>
                              </div>
                              <div class="progress progress-success progress-striped" id="bar">
                                 <div class="bar" style="width: 25%;"></div>
                              </div>
							  <div style="float:left; width: 25%; ">
							  <div class="">
                              <div class="">
                                 <div data-provides="fileupload" class="fileupload fileupload-new">
                                    <div data-provides="fileupload" class="fileupload fileupload-new">
                                    <div style="width: 200px; height: 150px;" class="fileupload-new thumbnail">
                                    <?
                                    	if(isset($user_data['profileurl']) && $user_data['profileurl']==""){
                                    	$src = asset_url() . 'img/size.gif';
                                    	}
                                    	else{
                                    	$src = $user_data['profileurl'];
                                    	}
                                     ?>
                                       <img alt="" src="<?=$src; ?>" width=100% >
                                    </div>

                                    <div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
                                   <!-- <div>
                                       <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                       <span class="fileupload-exists">Change</span>
                                       <input type="file" class="default"></span>
                                       <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a>
                                    </div>-->
                                 </div>
                                 
                              </div>
                           </div> 
							  </div>
				</div>			  
                              <div class="tab-content">
                                 <div id="tab1" class="tab-pane active">
<form id="personal-details" name="personal-details" method="post">   
                                    <div class="control-group">
                                       <label class="control-label">Name</label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" id="name" name="name" value="<?=$user_data['name']; ?>">
										  <input type="hidden" name="user_type" id="user_type" value="candidate" />
                                       </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">User Name</label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" id="username" name="username" value="<?=$user_data['username']; ?>">
                                       </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">E-Mail</label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" id="email" name="email" value="<?=$user_data['email']; ?>">
				                    </div>
                                    </div>
					
					<div class="control-group">
                                       <label class="control-label">About Me</label>
                                       <div class="controls">
                                          <textarea rows="3" class="span6 m-wrap" id="summary" name="summary"><?=$user_data['summary']; ?></textarea>
					</div>
                                    </div>
                                 </form>   

                                 </div>
								 
                                 <div id="tab2" class="tab-pane">
                                  <form id="profile-details" name="profile-details" method="post">   
                                    <div class="control-group">
                                       <label class="control-label">Current Employer</label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" name="current_employer" value="<?=$user_profile_data['current_employer'];  ?>">
                                       </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Current Salary</label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" name="current_salary" value="<?=$user_profile_data['current_salary'];  ?>">
                                       </div>
                                    </div>
									
									<div class="control-group">
                                       <label class="control-label" >Designation</label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" name="designation" value="<?=$user_profile_data['designation'];  ?>">
                                       </div>
                                    </div>
                                    <div class="control-group">
                              <label class="control-label">Type of Company</label>
                              <div class="controls">
                                 <select tabindex="1" data-placeholder="Choose a Category" class="span6 m-wrap" name="skill_set"  >
                                    <option value="Large Company" <?= ($user_profile_data['skill_set'] == "5" ? 'selected="selected"' : '' ) ?>>Large Company</option>
<option value="Medium Size" <?= ($user_profile_data['skill_set'] == 2 ? 'selected="selected"' : '' ) ?>>Medium Size </option>
<option value="MNC" <?= ($user_profile_data['skill_set'] == 1 ? 'selected="selected"' : '' ) ?>>MNC</option>
<option value="NON PROFIT" <?= ($user_profile_data['skill_set'] == 6 ? 'selected="selected"' : '' ) ?>>Non Profit</option>
<option value="PSU" <?= ($user_profile_data['skill_set'] == 3 ? 'selected="selected"' : '' ) ?>>PSU</option>
<option value="STARTUP" <?= ($user_profile_data['skill_set'] == 4 ? 'selected="selected"' : '' ) ?>>Start Up</option>
                                 </select>
                              </div>
							  </div>
							  <div class="control-group">
                                       <label class="control-label">Experience
									   
									   </label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" name="experience" value="<?=$user_profile_data['experience'];  ?>">
                                       </div>
                                    </div>
                                    
									<div class="control-group">
                              <label class="control-label"> Highest Qualification</label>
                              <div class="controls">
                                 <select tabindex="1" data-placeholder="Choose a Category" class="span6 m-wrap" name="highest_qualification" >
                                    
                                    <option value="Accounting / Finance">Accounting / Finance </option>
<option value="Advertising / PR / MR / Event Management">Advertising / PR / MR / Event Management </option>
<option value="Agriculture / Dairy">Agriculture / Dairy </option>
<option value="Animation / Gaming">Animation / Gaming </option>
<option value="Architecture / Interior Design">Architecture / Interior Design </option>
<option value="Automobile / Auto Anciliary / Auto Components">Automobile / Auto Anciliary / Auto Components </option>
<option value="Aviation / Aerospace Firms">Aviation / Aerospace Firms </option>
<option value="Banking / Financial Services / Broking">Banking / Financial Services / Broking </option>
<option value="BPO / Call Centre / ITES">BPO / Call Centre / ITES </option>
<option value="Brewery / Distillery ">Brewery / Distillery </option>
<option value="Ceramics / Sanitary ware">Ceramics / Sanitary ware </option>
<option value="Chemicals / PetroChemical / Plastic / Rubber">Chemicals / PetroChemical / Plastic / Rubber </option>
<option value="Construction / Engineering / Cement / Metals">Construction / Engineering / Cement / Metals </option>
<option value="Consumer Electronics / Appliances / Durables ">Consumer Electronics / Appliances / Durables </option>
<option value="Courier / Transportation / Freight / Warehousing">Courier / Transportation / Freight / Warehousing </option>
<option value="Design/UI/UX">Design/UI/UX</option>
<option value="E-Commerce">E-Commerce</option>
<option value="Education / Teaching / Training">Education / Teaching / Training </option>
<option value="Electricals / Switchgears">Electricals / Switchgears </option>
<option value="Export / Import">Export / Import </option>
<option value="Facility Management">Facility Management </option>
<option value="Fertilizers / Pesticides">Fertilizers / Pesticides </option>
<option value="FMCG / Foods / Beverage">FMCG / Foods / Beverage </option>
<option value="Food Processing">Food Processing </option>
<option value="Fresher / Trainee / Entry Level">Fresher / Trainee / Entry Level </option>
<option value="Gems / Jewellery">Gems / Jewellery </option>
<option value="Glass / Glassware">Glass / Glassware </option>
<option value="Government / Defence">Government / Defence </option>
<option value="Heat Ventilation / Air Conditioning">Heat Ventilation / Air Conditioning </option>
<option value="Industrial Products / Heavy Machinery">Industrial Products / Heavy Machinery </option>
<option value="Insurances">Insurance </option>
<option value="Iron and Steel">Iron and Steel </option>
<option value="IT-Hardware & Networking">IT-Hardware & Networking </option>
<option value="IT-Software / Software Services">IT-Software / Software Services </option>
<option value="KPO / Research / Analytics">KPO / Research / Analytics </option>
<option value="Legal">Legal </option>
<option value="Media / Entertainment / Internet">Media / Entertainment / Internet </option>
<option value="Medical / Healthcare / Hospitals">Medical / Healthcare / Hospitals </option>
<option value="Mining / Quarrying">Mining / Quarrying </option>
<option value="NGO / Social Services / Regulators / Industry Associations">NGO / Social Services / Regulators / Industry Associations </option>
<option value="Office Equipment / Automation">Office Equipment / Automation </option>
<option value="Oil and Gas / Energy / Power / Infrastructure">Oil and Gas / Energy / Power / Infrastructure </option>
<option value="Other">Other </option>
<option value="Pharma / Biotech / Clinical Research">Pharma / Biotech / Clinical Research </option>
<option value="Printing / Packaging">Printing / Packaging </option>
<option value="Private Equity/Investment Banking">Private Equity/Investment Banking</option>
<option value="Publishing">Publishing </option>
<option value="Pulp and Paper">Pulp and Paper </option>
<option value="Real Estate / Property">Real Estate / Property </option>
<option value="Recruitment / Staffing">Recruitment / Staffing </option>
<option value="Retail / Wholesale">Retail / Wholesale </option>
<option value="Security / Law Enforcement">Security / Law Enforcement </option>
<option value="Seed Fund/Venture Capital/ Angel Investor">Seed Fund/Venture Capital/ Angel Investor</option>
<option value="Semiconductors / Electronics">Semiconductors / Electronics </option>
<option value="Shipping / Marine">Shipping / Marine </option>
<option value="Strategy / Management Consulting Firms">Strategy / Management Consulting Firms </option>
<option value="Telcom / ISP">Telcom / ISP </option>
<option value="Textiles / Garments / Accessories">Textiles / Garments / Accessories </option>
<option value="Travel / Hotels / Restaurants / Airlines / Railways">Travel / Hotels / Restaurants / Airlines / Railways </option>
<option value="Tyres">Tyres </option>
<option value="Water Treatment / Waste Management">Water Treatment / Waste Management </option>
<option value="Wellness / Fitness / Sports">Wellness / Fitness / Sports </option>
                                 </select>
                              </div>
                           </div>
                                    
                                    <div class="control-group">
                              <label class="control-label">Institue/Organization</label>
                              <div class="controls">
                                 <input type="text" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;California&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" data-items="4" data-provide="typeahead" class="span6 m-wrap"  name="institute_name" value="<?=$user_profile_data['institute_name'];  ?>">
                                 
                              </div>
                           </div>
						   <div class="control-group">
                              <label class="control-label">Batch Year</label>
                              <div class="controls">
                                 <select tabindex="1" data-placeholder="Choose a Category" class="span6 m-wrap" name="batch_year">
                                    <option value="-1">Year</option>
<option value="2016">2016</option>
<option value="2015">2015</option>
<option value="2014">2014</option>
<option value="2013">2013</option>
<option value="2012">2012</option>
<option selected="" value="2011">2011</option>
<option value="2010">2010</option>
<option value="2009">2009</option>
<option value="2008">2008</option>
<option value="2007">2007</option>
<option value="2006">2006</option>
<option value="2005">2005</option>
<option value="2004">2004</option>
<option value="2003">2003</option>
<option value="2002">2002</option>
<option value="2001">2001</option>
<option value="2000">2000</option>
<option value="1999">1999</option>
<option value="1998">1998</option>
<option value="1997">1997</option>
<option value="1996">1996</option>
<option value="1995">1995</option>
<option value="1994">1994</option>
<option value="1993">1993</option>
<option value="1992">1992</option>
<option value="1991">1991</option>
<option value="1990">1990</option>
<option value="1989">1989</option>
<option value="1988">1988</option>
<option value="1987">1987</option>
<option value="1986">1986</option>
<option value="1985">1985</option>
<option value="1984">1984</option>
<option value="1983">1983</option>
<option value="1982">1982</option>
<option value="1981">1981</option>
<option value="1980">1980</option>
<option value="1979">1979</option>
<option value="1978">1978</option>
<option value="1977">1977</option>
<option value="1976">1976</option>
<option value="1975">1975</option>
<option value="1974">1974</option>
<option value="1973">1973</option>
<option value="1972">1972</option>
<option value="1971">1971</option>
<option value="1970">1970</option>
<option value="1969">1969</option>
<option value="1968">1968</option>
<option value="1967">1967</option>
<option value="1966">1966</option>
<option value="1965">1965</option>
<option value="1964">1964</option>
<option value="1963">1963</option>
<option value="1962">1962</option>
<option value="1961">1961</option>
<option value="1960">1960</option>
<option value="1959">1959</option>
<option value="1958">1958</option>
<option value="1957">1957</option>
<option value="1956">1956</option>
<option value="1955">1955</option>
<option value="1954">1954</option>
<option value="1953">1953</option>
<option value="1952">1952</option>
<option value="1951">1951</option>
<option value="1950">1950</option>
<option value="1949">1949</option>
<option value="1948">1948</option>
<option value="1947">1947</option>
<option value="1946">1946</option>
<option value="1945">1945</option>
<option value="1944">1944</option>
<option value="1943">1943</option>
<option value="1942">1942</option>
<option value="1941">1941</option>
<option value="1940">1940</option>
                                    
                                 </select>
                              </div>
							  </div>
							<div class="control-group">
                                       <label class="control-label" >Resume in Text Format</label>
                                       <div class="controls">
                                          <textarea rows="3" class="span6 m-wrap" name="resume_plain_text" ><?=$user_profile_data['resume_plain_text'];  ?></textarea>
                                       </div>
                                    </div>
							
				<!--			<div class="control-group">
                              <label class="control-label">Upload Latest Resume</label>
                              <div class="controls">
                                 <div data-provides="fileupload" class="fileupload fileupload-new">
                                    <div class="input-append">
                                       <div class="uneditable-input">
                                          <i class="icon-file fileupload-exists"></i> 
                                          <span class="fileupload-preview"></span>
                                       </div>
                                       <span class="btn btn-file">
                                       <span class="fileupload-new">Select file</span>
                                       <span class="fileupload-exists">Change</span>
                                       <input type="file" class="default">
                                       </span>
                                       <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
			-->				  
						   <div class="control-group">
                                       <label class="control-label">Website URL</label>
                                       <div class="controls">
                                          <input type="text" class="span6 m-wrap" name="website_url" value="<?=$user_profile_data['website_url'];  ?>">
                                       </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">Remarks</label>
                                       <div class="controls">
                                  <textarea rows="3" class="span6 m-wrap" name="remarks"><?=$user_profile_data['remarks'];  ?></textarea>
                                       </div>
                                    </div>
                                 </form>   
                                 </div>
                                 
				<div id="tab3" class="tab-pane">
				</div>
				<!--end tab-pane-->
							</div> 
                                 
								 </div>
                              
                              <div class="form-actions clearfix">
                                 <a class="btn button-previous" href="javascript:;" style="display: none;">
                                 <i class="m-icon-swapleft"></i> Back 
                                 </a>
                                 <a class="btn blue button-next" href="javascript:;" onclick="if($('#tab1').is(':visible')){addUserPersonalDetails();} if($('#tab2').is(':visible')){ addUserProfileDetails(); } ">
                                 Continue <i class="m-icon-swapright m-icon-white"></i>
                                 </a>
                                 <button class="btn green button-submit" href="javascript:;" style="display: none;" onclick="$('#register_form').submit();">
                                 Submit <i class="m-icon-swapright m-icon-white"></i>
                                 </button>
                              </div>
						   </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
            </div>
         </div>
