<div aria-hidden="false" aria-labelledby="myModalLabel1" role="dialog" tabindex="-1" class="modal hide fade in" id="select-user-type" style="display: none;">
<form method="post" action="<?php echo site_url('/users/recruiter_profile'); ?>">			
									<div class="modal-header">
										<button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="removeModel('select-user-type');"></button>
										<h3 id="myModalLabel1">Login as</h3>
									</div>
									<div class="modal-body">
						
										<div class="control-group">
                                       <div class="controls">
                                          <label class="radio">
                                          <div class="radio" id="uniform-undefined"><span><input type="radio" value="candidate" name="optionsRadios1" style="opacity: 0;" checked onclick="changeAction(this.value);"></span></div>
                                          Job Seeker
                                          </label>
                                          <label class="radio">
                                          <div class="radio" id="uniform-undefined"><span><input type="radio" checked="" value="recruiter" name="optionsRadios1" style="opacity: 0;" onclick="changeAction(this.value);"></span></div>
                                          Recruiter
                                          </label>  
                                            
                                       </div>
                                    </div>
                         
			</div>
									<div class="modal-footer">
				<button class="btn blue" id="proceed" type="submit" name="continue">Continue..</button>
				<button class="btn" onclick="alert('You can not Proceed until you select Your User Type..');">Cancel</button>
	</div>
	</form>           
</div>
  
<script>
	openDialog('select-user-type');
	function changeAction(user_type){
		$('#proceed').click(function(){
		if(user_type='candidate'){
            this.form.action = "<?php echo site_url('/users/candidate_profile'); ?>";
            }
            else{
            this.form.action = "<?php echo site_url('/users/recruiter_profile'); ?>";
            }
        });
	}
</script>
