<?php $alert=$this->session->flashdata('msg');
 if($alert){  
    echo '<script type=\'text/javascript\'>';  
    echo 'alert("'.$alert.'");';  
    echo '</script>';
}  
?>
<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Badges</h4>
                     </div>
<div class="portlet-body form">
<!-- BEGIN FORM-->
<div id="recommended">
<h3 class="form-section">My Badges</h3>
<?php if ($user_profile_data){?>
	<div class="row-fluid">
    <?php foreach($user_profile_data as $val){?>
    <div class="span3">
	<div class="item">
            <a class="fancybox-button" data-rel="fancybox-button" title="Photo" href="<? echo base_url() . 'files/badges'.'/'. $user_id.'/'.$val['badge_image']?>">
                <div>
                    <img src="<? echo base_url() . 'files/badges'.'/'. $user_id.'/'.$val['badge_image']?>" alt="Photo" />							
                </div>
            </a>
	</div>
</div><?php }?>
</div><?php }?>
</div>
						   
                           
                        <!-- END FORM-->           
                     </div>
                  </div>
			 	
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>