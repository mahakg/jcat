<!DOCTYPE html>
<html lang='en'><head>
<title>
Rate your skills
</title>
<link href="<?=asset_url(); ?>css/bootstrap-e87fe241f4fc63fcfdbb4c7f03ad8560.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?=asset_url(); ?>css/v3-98384bc016773cab114a08bb542e7304.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?=asset_url(); ?>js/application-8a01be4bfe0892191bc2767d0145d643.js" type="text/javascript"></script>
</head>
<body class='pt-sessions sessions-signin  anonymous'>
<div class='page-container'>
<div class='pt-signin' id='container'>
<div class="container mar-top-50">
	<div class="row">
		<div class="span6 offset3">
			<div class="content background">
				<form accept-charset="UTF-8" action="/signin" method="post" novalidate="novalidate"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="x2ss+dTf0Vcqd7tww9+k8UBPWBUbvm16piOMThdv9Ew=" /></div>
					<input id="user_type" name="user_type" type="hidden" value="profile" />
					<div class="row-fluid user-type-selector">
						<div class="span6">
							<input id="login-personal" name="type" type="radio" checked="checked" value="" />
							<label class="login-personal-label" for="login-personal"><span class="sprites-signup-personal"></span>Personal</label>
						</div>
						<div class="span6">
							<input id="login-employer" name="type" type="radio" />
							<label class="login-employer-label" for="login-employer"><span class="sprites-signup-business"></span>Business</label>
						</div>
						<div class="knob left"></div>
					</div>
					<section class="social" style="padding-bottom: 0;">
						<div class="pad-top-20">
							<span class="label inline-block">Sign in with:</span>
							<a href="/auth/facebook" class="oauth" id="auth-facebook"><span class="sprites-social-facebook">f</span></a>
							<a href="/auth/twitter" class="oauth" id="auth-twitter"><span class="sprites-social-twitter">t</span></a>
							<a href="/auth/linkedin" class="oauth" id="auth-linkedin"><span class="sprites-social-linkedin">l</span></a>
							<a href="/auth/google_oauth2" class="oauth" id="auth-google"><span class="sprites-social-google">g</span></a>
						</div>
						<div class="label-replace"><span>Or With Your Email</span></div>
					</section>
					<section>

						<div class="control-group">
							<label class="control-label" for="email_address">Email Address</label>
							<div class="controls">
								<input class="input-block" id="email" name="email" required="required" type="email" />
								<p class="help-block"></p>
							</div>
						</div>
						<div class="control-group flush">
							<label class="control-label" for="password">Password</label>
							<div class="controls inline-block">
								<input id="password" name="password" required="required" type="password" />
								<p class="help-block"></p>
							</div>
							<a href="/forgot_password" class="text-secondary">Forgot Password?</a>
						</div>
					</section>
					<section class="border-top">
						<input class="btn btn-success" id="auth-email" name="commit" type="submit" value="Sign In" />
						<span class="text-secondary">Don't have an account yet? <a href="/signup">Sign up!</a></span>
					</section>
</form>			</div>
		</div>
	</div>
</div>


<div class='container'>
<div id='footer'></div>
</div>
</div>
</div>
<div class='modal hide fade' id='modal'>
<div class='modal-body'>
<!-- * various reusable components will go here -->
</div>
</div>
</body>
</html>
