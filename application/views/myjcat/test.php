<style>
a.more_link {
    clear: both;
    display: block;
    text-align: right;
}
.form-horizontal > div {
 margin-left: 30px; 
}
.test_info {


    font-size: 13px;

}
a.skill-name {
 white-space: nowrap; 
font-size: 13px;

}



</style>

<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Select Any Skill</h4>
                        <div class="tools">
                           <a class="collapse" href="javascript:;"></a>
                           <a class="reload" href="javascript:;"></a>
                           <a class="remove" href="javascript:;"></a>
                        </div>
                     </div>
<div class="portlet-body form">
<!-- BEGIN FORM-->
<form class="form-horizontal" action="#">

<div class="control-group">
<h3><label class="control-label">Browse by Category</label></h3>
<div class="controls">
<input type="text" data-source="[&quot;Business Skills&quot;,&quot;Design Tools&quot;,&quot;Desktop Software&quot;,&quot;Engineering & Architecture&quot;,&quot;Language & Writing&quot;,&quot;Marketing&quot;,&quot;Math & Finance&quot;,&quot;Networking&quot;,&quot;Music&quot;,&quot;Office Tools&quot;,&quot;Sales&quot;,&quot;Programming&quot;,&quot;Trivia&quot;,&quot;Uncategorized&quot;,&quot;Websites & Apps&quot;]" data-items="4" data-provide="typeahead" style="margin: 0 auto;" class="span6 m-wrap">

</div>
</div>


<div id="recommended">
<h3 class="form-section">Skills Under...</h3>
<? 
$size = sizeof($skills);
for($i=0; $i< ceil(sizeof($skills)/5); $i++) { ?>
<div class="row-fluid">
<? 
$lim = 0;
if($size%5==0){
$lim =5;
}
else $lim = $size%5;

if($size>5){
$lim = 5;
}

for($j=0; $j<$lim; $j++){
 ?>
	<div class="span2">
		<div class="item">
			<a href="<?=site_url('/myjcat/skill_desc') . '?skill_id=' . $skills[(5*$i)+$j]['id']; ?>" title="Photo" data-rel="fancybox-button" class="fancybox-button">
				<img alt="sql" src="<? echo base_url() . 'files/skills/'. $skills[(5*$i)+$j]['id'] . '/' . $skills[(5*$i) +$j]['img'] ?>" width="60" height="60" />
			</a>
		</div>
		<div class="test_info"> 
			<h4><a _url_slug="basic-math" href="<?=site_url('/myjcat/skill_desc') . '?skill_id=' . $skills[(5*$i)+$j]['id']; ?>" class="skill-name"><?=$skills[(5*$i)+$j]['name'] ?></a></h4>
			<p></p>
			<p>22,859
			testers</p>
		</div>
	</div>
<? 
$size--;
} ?>
</div>
<? } ?>
</div>
						   
                           
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
			 	
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
