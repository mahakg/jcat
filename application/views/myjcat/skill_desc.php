<link href="<?=asset_url(); ?>css/test_landing_page.css" rel="stylesheet" />
<h3 class="page-title">Mysql</h3>
            <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="#">Test</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?=$skill_details[0]['name']; ?></a></li>
						</ul>
		<div class="grid12 box test">
			<img src="<? echo base_url() . 'files/skills/'. $skill_details[0]['id'] . '/' . $skill_details[0]['img'] ?>" width="120" id="test_icon" height="120" alt="" />
			<div class='test_heading'>
				<h1><?=$skill_details[0]['name']; ?></h1>
				<span>
						Recommended For You -
					Tested by
					4,621 users
				</span>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?=site_url('/myjcat/starttest') . '?skill_id=' . $skill_details[0]['id']; ?>" class="btn blue">Take This Test</a>
					<p>You took the practice question <br>for this test on September  3, 2013.</p>
			</div>
			<hr class='cf'/>
			<div class='third first'>
				<h4>About <?=$skill_details[0]['name']; ?></h4>
				<p><?=$skill_details[0]['desc']; ?></p>
				<h4 class='margin_top'>Statistics</h4>
				<ul class="test_vitals">

						<li>
							<strong>
								69th
							</strong>
							most popular Test on Rate Your skill
						</li>

						<li>
							<strong>4,621</strong>
							badges earned
							since June 11, 2011
						</li>


					<li>
							<strong>112</strong>
							questions added<br>
					</li>

						

				</ul>
			</div>
			<div class="two third">
				<div class='test_recommended'>
					<h4>Related Tests</h4>
					<ul  class="test_list horizontal test_details">
							<li>
								<a href="/tests/sql" ><img src="<?=asset_url(); ?>img/test/sql.png" width="60" class="border-3" _url_slug="sql" height="60" alt="" /></a>
								<div class='test_info'>
						 			<h4><a href="/tests/sql" _url_slug="sql">SQL</a></h4>
						 			<p>Recommended</p>
						 			<p>4,687
						 			   testers</p>
								</div>
							</li>
							<li>
								<a href="/tests/javascript" ><img src="<?=asset_url(); ?>img/test/js.png" width="60" class="border-3" _url_slug="javascript" height="60" alt="" /></a>
								<div class='test_info'>
						 			<h4><a href="/tests/javascript" _url_slug="javascript">JavaScript</a></h4>
						 			<p>Recommended</p>
						 			<p>9,021
						 			   testers</p>
								</div>
							</li>
							<li>
								<a href="/tests/linux" ><img src="<?=asset_url(); ?>img/test/linux.png" width="60" class="border-3" _url_slug="linux" height="60" alt="" /></a>
								<div class='test_info'>
						 			<h4><a href="/tests/linux" _url_slug="linux">Linux</a></h4>
						 			<p>Recommended</p>
						 			<p>2,982
						 			   testers</p>
								</div>
							</li>
							<li>
								<a href="/tests/html5" ><img src="<?=asset_url(); ?>img/test/icon_html.png" width="60" class="border-3" _url_slug="html5" height="60" alt="" /></a>
								<div class='test_info'>
						 			<h4><a href="/tests/html5" _url_slug="html5">HTML5</a></h4>
						 			<p>Recommended</p>
						 			<p>4,136
						 			   testers</p>
								</div>
							</li>
					</ul>
				</div>
				
			</div>
		</div>
	
