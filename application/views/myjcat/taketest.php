<style>
    span.tab{
    padding: 0 600px; /* Or desired space*/
}</style>
<div class="tab-content">
<div id="tab_2" class="tab-pane active">
                           <div class="portlet box green">
                              <div class="portlet-title">
                                 <h4><i class="icon-reorder"></i>Exam Zone</h4>
                                 <div class="tools">
                                    <a class="collapse" href="javascript:;"></a>
                                    <a class="reload" href="javascript:;"></a>
                                 </div>
                              </div>
                              <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form class="form-horizontal" method="post">                         
                                    <h3 class="form-section">Question <?=$attempt_question; ?><strong><span class="tab" id="timer"></span></strong></h3> 
                                    <div class="" style="display:none;" id="alert">
									<button data-dismiss="alert" class="close"></button>
									<strong id="alert-data"></strong>
					</div>
                                    <div class="row-fluid" id="question">
                                       <div class="span12 ">
                                          <div class="control-group">
                                              <label class="control-label"><h4>Question</h4></label>
                                             <div class="controls">
                                                 <div class="well"><wbr><?=$question_details['question']; ?></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                               <div class="row-fluid"> 
                                    <div class="span12">
                                       <div class="control-group">
                                             <label class="control-label"><h4>Correct Answer</h4></label>
                                             <div class="controls">                                                
                                                <label class="radio">
                                              <div id="uniform-undefined"><input type="radio" value="optiona" name="question[correctanswer]" onclick="sendStatus(this.value);">
                                                <?=$question_details['optiona']; ?></div>
                                                </label>
                                              </div>  
                                              <div class="controls">
                                                <label class="radio">
                                                <div id="uniform-undefined"><input type="radio" checked="" value="optionb" name="question[correctanswer]" onclick="sendStatus(this.value);">
                                                <?=$question_details['optionb']; ?></div>
                                                </label>
                                                </div>
                                              <div class="controls">  
                                                <label class="radio">
						<div id="uniform-undefined"><input type="radio" checked="" value="optionc" name="question[correctanswer]"  onclick="sendStatus(this.value);">
                                                <?=$question_details['optionc']; ?></div>
                                                </label>  
                                                </div><?php if($question_details['optiond']){?>
                                               <div class="controls">
                                                <label class="radio">
                                                <div id="uniform-undefined"><input type="radio" checked="" value="optiond" name="question[correctanswer]" onclick="sendStatus(this.value);">
                                                <?=$question_details['optiond']; ?></div>
                                                </label>
                                                </div><?php }?><?php if($question_details['optione']){?>
						<div class="controls">
                                                <label class="radio">
                                                <div id="uniform-undefined"><input type="radio" checked="" value="optione" name="question[correctanswer]" onclick="sendStatus(this.value);">
                                                <?=$question_details['optione']; ?></div>
                                                </label>  
                                                </div><?}?>
                                                
                                             </div>
                                          </div>
                                        </div>  
                                       <!--/span-->
                                  <?php if($question_details['explanation']){?>  
                                    <!--/row-->
                                    <div class="row-fluid">
                                       <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label"><h4>Explanation</h4></label>
                                             <div class="controls">
                                                <span class="m-wrap span12" name="question[explanation]"><?=$question_details['explanation']; ?></span>
                                             </div>
                                          </div>
                                       </div>
                                  </div><?}?>
                                    <div class="form-actions">
                                       <button class="btn blue" type="submit" id="next-question" style="display:none;"><i class="icon-ok"></i> Next Question</button>
                                    </div>
                                 </form>
                                 <!-- END FORM-->                
                              </div>
                           </div>
                        </div>
</div>
<script>
var count = checkCookie();
var counter=setInterval(timer, 1000); //1000 will  run it every 1 second
function timer()
{
	count=count-1;
	if (count <= 0)
	{
		sendStatus("");
		clearInterval(counter);
		return;
	}
	setCookie("question_timer",count,21);
	document.getElementById("timer").innerHTML=count + " secs"; // watch for spelling
}
function sendStatus(answer){
$("#timer").hide();
clearInterval(counter);
$.ajax({
	type: 'GET',
	url: "<? echo site_url('/myjcat/questionresult'); ?>",
	data: {time: $("#timer").html(),ans: answer }	,
	dataType: 'json',
	success: function(data){
		var status = data.status;
		if(status=='true'){
			if(data.mode=='error'){
			
				
			}
			jQuery("input:radio").attr('disabled',true);
			
			$("#alert").attr("class", "alert alert-"+ data.mode);
		  	$("#alert").attr("style", "display:block");
		  	$("#alert-data").html(data.text);
		  	$("#next-question").show();		
		}
		setCookie("question_timer","",-1);
	}
	});

}
function setCookie(c_name,value,exdays)
{
var exdate=new Date();
exdate.setDate(exdate.getDate() + exdays);
var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
document.cookie=c_name + "=" + c_value;
}
function checkCookie()
{
var timer=getCookie("question_timer");
  if(timer!=null && timer!="")
  {
  count = timer;
  }
else
  {
	count=<?=$timer ?>;
	setCookie("question_timer",count,21);
  }
  return count;
}
function getCookie(c_name)
{
var c_value = document.cookie;
var c_start = c_value.indexOf(" " + c_name + "=");
	if(c_start == -1)
	{
		c_start = c_value.indexOf(c_name + "=");
	}
	if (c_start == -1)
	{
	c_value = null;
	}
	else
	{
	c_start = c_value.indexOf("=", c_start) + 1;
	var c_end = c_value.indexOf(";", c_start);
	if (c_end == -1)
	{
	c_end = c_value.length;
	}
	c_value = unescape(c_value.substring(c_start,c_end));
	}
	return c_value;
}

</script>
