<div class="portlet box blue">
							<div class="portlet-title">
								<h4><i class=""></i>Jobs Submitted By You</h4>
								<div class="tools">
									<a class="remove" href="javascript:;"></a>
								</div>
							</div>
    		

							<div class="portlet-body">
								<div class="clearfix">
									
									<div class="btn-group pull-right">
										<button data-toggle="dropdown" class="btn dropdown-toggle">Tools <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">Print</a></li>
											<li><a href="#">Save as PDF</a></li>
											<li><a href="#">Export to Excel</a></li>
										</ul>
									</div>
								</div>
								
                                                                       
				<? if(isset($view_jobs)){ ?>				
								<table id="sample_editable_1" class="table table-striped table-hover table-bordered dataTable" aria-describedby="sample_editable_1_info">
									<thead>
										<tr role="row">
										<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 100px;" aria-sort="ascending" aria-label="Username: activate to sort column descending">Job id</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 238px;" aria-label="Full Name: activate to sort column ascending">Company Name</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 116px;" aria-label="Points: activate to sort column ascending">Application Status</th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" style="width: 116px;" aria-label="Points: activate to sort column ascending">Date - Slot</th>
										</tr>
									</thead>
									
								<tbody role="alert" aria-live="polite" aria-relevant="all"><tr class="odd">
 <?php foreach ($view_jobs as $row)
{ ;?> 
<tr>
<td><?php echo $row['id'];?></td>
<td><?php echo $row['job_id'];?></td>
<td class=" "><span class="label label-success">
<?php echo $row['application_status'];?></td>
<td><?php echo $row['date_slot'];?></td>
</tr>
<?php } ?>
	
											
											
											
										</tbody></table>
                                                            
                                                        </div>
    <?	 } else{
					echo "You Have not applied to any Job so Far!!" ;
					?>
					<a class="btn info" type="submit" onclick="window.history.back();">Go Back</a>
                                        <?php }?>
							</div>
						