
<style>
.portfolio-info span {
    color: rgb(22, 161, 242);
    display: block;
    font-size: 20px;
    margin-top: 5px;
    font-weight: 200;
    text-transform: uppercase;
}
</style>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						
						<!-- END BEGIN STYLE CUSTOMIZER --> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Search Results				
						</h3>
						
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid profile">
					<div class="tabbable tabbable-custom">
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1_1">
							<? if($jobs_list){ ?>
							<? foreach($jobs_list as $job_data){ ?>	
								
								<div class="row-fluid portfolio-block">
									<div class="span5 portfolio-text">
										<div class="portfolio-text-info">
											<h3><?=$job_data['job_title']; ?></h3>
										</div>
									</div>
									<div class="span5">
										<div class="portfolio-info">
											Company Name
											<span><?=$job_data['company_profile']; ?></span>
										</div>
										<div class="portfolio-info">
											Annual Salary
											<span><?=$job_data['salary']; ?> INR</span>
										</div>
									</div>
									<div class="span2 portfolio-btn">
										<a class="btn bigicn-only" href="<?=site_url('/jobs/job_description?job_id='. $job_data['id'] );?>"><span>View</span></a>								
									</div>
								</div>
				<? }} else{
					echo "No Jobs Found matching your criteria, Please change your search criteria!" ;
					?>
					<a class="btn info" type="submit" onclick="window.history.back();">Go Back</a>
<?				} ?>				
	
								<div class="space5"></div>
							</div>
							<!--end tab-pane-->
							
							<!--end tab-pane-->
						</div>
					</div>
					<!--end tabbable-->				
				</div>
				<!-- END PAGE CONTENT-->
			</div>
