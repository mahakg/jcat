						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
						<? if($jobs_details){ ?>
							<? foreach($jobs_details as $job_data){ ?>	
							<h3><?=$job_data['job_title']; ?></h3>
					
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="#">Jobs</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?=$job_data['job_title']; ?></a></li>
						</ul>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span8">
						<div>
							<!-- BEGIN GENERAL PORTLET-->
							<div class="portlet">
								<div class="portlet-title">
									<h4><i class="icon-reorder"></i>Jobs Opportunity </h4>
									<div class="tools">
										<a href="javascript:;" class="collaspse"></a>
										<a href="#portlet-config" data-toggle="modal" class="config"></a>
										<a href="javascript:;" class="reload"></a>
										<a href="javascript:;" class="remove"></a>
									</div>
								</div>
								<div class="portlet-body">
									<div class="row-fluid">
										
										<div class="search-classic">
										<div class="span6">
											<h3 style="padding-bottom: 15px;"><?=$job_data['job_title']; ?></h3>
                                                                                        <div class="well">
                                                                                            <?php if ($submitted_jobs){?>
                                                                                                <div class="pull-right">
                                                                                                    <a role="button" class="btn green">Applied</a>
                                                                                                </div>
                                                                                            <?php }else{?>
                                                                                            <div class="pull-right">
                                                                                                    <a href="#myModal3" data-toggle="modal" class="btn red" id="pulsate-regular">Apply</a>
                                                                                            </div><?}?>
                                                                                            		<div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                                                                                       <form  action="<?=site_url('/jobs/applyjob');?>" method="post"> 
                                                                                                            <div class="modal-header">
                                                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                                                <h3 id="myModalLabel3">Book Your Exam</h3>
                                                                                                        </div>
                                                                                                        <div class="modal-body">
                                                                                                                                       
                                                                                                                <table class="table sliders table-striped">
                                                                                                                        <tbody>
                                                                                                                                 <tr>
                                                                                                                                       <td> <label class="control-label">Select Exam date Slot</label></td>
                                                                                                                                        <td>
                                                                                                                                              <div class="controls">
                                                                                                                                                    <input class="m-wrap m-ctrl-medium date-picker"  name="job[date_slot]" size="16" type="text" value="12-02-2012" />
                                                                                                                                              </div>
                                                                                                                                        </td>
                                                                                                                                 </tr>
                                                                                                               <input type="hidden" name="job[job_id]" value="<?php echo $job_data['id'];?>">
                                                                                                                <input type="hidden" name="job[user_id]" value="<?php echo $user_data;?>">          
                                                                                                                     </tbody>
                                                                                                                </table>
                                                                                                              
                                                                                                        </div>
                                                                                                        <div class="modal-footer">
                                                                                                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                                                                <button class="btn blue" type="submit" value="confirm"  name="confirm">save</button>
                                                                                                        </div> </form>
                                                                                                    </div>
                                                                                        <table width="1000" border="0" cellspacing="0" cellpadding="00">
                                                                                                    <tr>
                                                                                                      <td width="100" align="right" valign="top" style="font-size:13px;"><strong style="font-size:13px;">Job Type:</strong></td>
                                                                                                      <td width="15" align="left" valign="top" style="font-size:13px;">&nbsp;</td>
                                                                                                <?php      if($job_data['employment_status'] =="fulltime"){?>
                                                                                                <td align="left" valign="top" style="font-size:13px;">Full Time</td>
                                                                                                <?php }else{?><td align="left" valign="top" style="font-size:13px;">Part Time</td><?}?>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                      <td align="right" valign="top" style="font-size:13px;"><strong style="font-size:13px;">City: </strong></td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;">&nbsp;</td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;"><?=$job_data['job_location']; ?></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                      <td align="right" valign="top" style="font-size:13px;"><strong style="font-size:13px;">Job Title: </strong></td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;">&nbsp;</td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;"><?=$job_data['job_title']; ?></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                      <td align="right" valign="top" style="font-size:13px;"><strong style="font-size:13px;">Qualification: </strong></td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;">&nbsp;</td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;"><?=$job_data['candidate_qualification']; ?></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                      <td align="right" valign="top" style="font-size:13px;"><strong style="font-size:13px;">Experience: </strong></td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;">&nbsp;</td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;"><?=$job_data['experience']; ?></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                      <td align="right" valign="top" style="font-size:13px;"><strong style="font-size:13px;">Last Date:</strong></td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;">&nbsp;</td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;"><?=$job_data['date_expiry']; ?></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                      <td align="right" valign="top" style="font-size:13px;"><strong style="font-size:13px;">Positions:</strong> </td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;">&nbsp;</td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;">1</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                      <td width="100" align="right" valign="top" style="font-size:13px;"><strong style="font-size:13px;">Stipend:</strong></td>
                                                                                                      <td width="15" align="left" valign="top" style="font-size:13px;">&nbsp;</td>
                                                                                                      <td align="left" valign="top" style="font-size:13px;">Paid (Monthly, Variable)<br/><?=$job_data['salary']; ?> INR</td>
                                                                                                    </tr>
                                                                                                  </table>
                                                                                            
                                                                               </div>
										</div>
                                                                                </div>
                                                                        </div>
                                                                    
                                                                    <div class="row-fluid">
                                                                              <h3 style="padding-bottom: 15px;">Required Skills</h3>
                                                                              <ul itemprop="skills" class="skills">
                                                                                <?php 
                                                                                  $skills=$job_data['key_skills'];
                                                                                  $skills_explode=explode(",", $skills);
                                                                                  foreach($skills_explode as $val){
                                                                                ?>
                                                                            <li>
                                                                                <span class="skill-star-wrapper">
                                                                                <span class="skill-star"></span></span>
                                                                                <span class="skill-text"><?=$val; ?>
                                                                                </span>
                                                                            </li>
                                                                   <?php }?>  
                                                                              </ul>
                                                                            </div>
                                                                    
                                                                            <div class="row-fluid">
                                                                              <h3 style="padding-bottom: 15px;">Description</h3>                                                                                                                                    
										<div class="well">
										<p><?=$job_data['job_description']; ?></p>	
                                                                                </div>
                                                                            </div>
                                                                             
                                                                    		<div class="row-fluid">
                                                                                 <h4><strong>About Employer</strong></h4> 
                                                                                    <ul class="unstyled profile-nav span3">
                                                                                    <li><img src="<?=asset_url(); ?>img/profile/profile-img.png" alt="" /> <a href="#"></a></li>
                                                                        	</ul>
											<div class="span8 profile-info">
												<h1><?=$job_data['company_profile']; ?></h1>
												<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt laoreet dolore magna aliquam tincidunt erat volutpat laoreet dolore magna aliquam tincidunt erat volutpat.</p>
												<p><a href="#">www.mywebsite.com</a></p>
											</div>
											<!--end span8-->
											
											<!--end span4-->
										</div>
								</div>
							</div>
						</div>
						<!-- END GENERAL PORTLET-->
					</div>
				<div class="box span4" style="padding-left:3px;margin-top:-22px;">
                                      <div class="span12">
                                            <h5 class="well"  style="height:20px;padding:10px"><strong>Similar Opportunities</strong></h5>
                      <div style=" border: 1px solid #e2e2e2; padding: 10px; margin-top: -21px; ">
                      			 
                       <a style="text-decoration:none;" href='assets/img/profile/profile-img.png'>
                          <div style="padding-bottom:5px;padding-top:15px;">
                            <div class='sugOppComLogo'>
                              <img alt="Designer at Aqsis" class="thumbnail" src = "<?=asset_url(); ?>img/agis_logo.png" height="58px" width="58px" />
                            </div>
                            <div class='sugOppComDtl'>
                              <strong>Graphic Designer</strong>
                              <br/> Aqsis <br/>Bangalore
                            </div>
                          </div>
                       </a>                				
                      
                          <a style="text-decoration:none;" href=''>
                          <div style="padding-bottom:5px;padding-top:15px;">
                            <div class='sugOppComLogo'>
                              <img alt="Software Developer India" class="thumbnail" src = "<?=asset_url(); ?>img/nobllesse_logo.jpg" height="58px" width="58px" />
                            </div>
                            <div class='sugOppComDtl'>
                              <strong>Software Developer India</strong>
                              <br /> Noblesse India<br /> Mumbai</div>
                          </div>
                          </a>                				
                            
                          <a style="text-decoration:none;" href=''>
                          <div style="padding-bottom:5px;padding-top:15px;">
                            <div class='sugOppComLogo'>
                              <img alt="internship at SignEasy" class="thumbnail" src = "<?=asset_url(); ?>img/masterwarks_logo.png" height="58px" width="58px" />
                            </div>
                            <div class='sugOppComDtl'>
                              <strong>Visual Communications Design Intern</strong>
                              <br />SignEasy<br /> Bangalore South</div>
                          </div>
                          </a>                				
                                             
                          <a style="text-decoration:none;" href=''>
                          <div style="padding-bottom:5px;padding-top:15px;">
                            <div class='sugOppComLogo'>
                              <img alt="internship at iView Labs Pvt Ltd" class="thumbnail" src = "<?=asset_url(); ?>img/iview_logo.jpg" height="58px" width="58px" />
                            </div>
                            <div class='sugOppComDtl'>
                              <strong>UI / UX Designer </strong>
                              <br /> iView Labs Pvt Ltd<br /> Ahmedabad</div>
                          </div>
                         </a>  
                          
                           <a style="text-decoration:none;" href=''>
                          <div style="padding-bottom:5px;padding-top:15px;">
                            <div class='sugOppComLogo'>
                                <img alt="internship at Masterworks Technologies" class="thumbnail" src = "<?=asset_url(); ?>img/masterwarks_logo.png" height="58px" width="58px" />
                            </div>
                            <div class='sugOppComDtl'>
                              <strong>Graphic designer </strong>
                              <br /> Masterworks Technologies<br />Pune</div>
                          </div>
                       </a>                				
                      </div>
                    </div>
                                    
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->			
		<?	}} else{
					echo "No Jobs Found matching your criteria, Please change your search criteria!" ;
					?>
					<a class="btn info" type="submit" onclick="window.history.back();">Go Back</a>
                                        <?php }?>