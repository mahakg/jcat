
<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<h4><i class="icon-edit"></i>Search All Jobs</h4>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<form class="form-horizontal" method="post" action="<?=site_url('/jobs/searchresults');?>">
									<div class="control-group">
										<label class="control-label">Select Skill Set</label>
										<div class="controls">
											  
											
											<select tabindex="1" data-placeholder="Choose a Category" class="span6 m-wrap" name="skill_id">
<?php 
    foreach($skill_name as $id=>$name)
	
{
?>
    <option value="<?=$id;?>"> <?=$name;?> </option>
	<?
}
?>
</select>
										
										</div>
										
									</div>
									

									<div class="control-group">
										<label class="control-label">Location</label>
										<div class="controls">
											<input type="text" name="job_location" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;California&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" data-items="4" data-provide="typeahead" style="margin: 0 auto;" class="span6 m-wrap">
											<p class="help-block">Type City you want to Work   E.g: Georgia</p>
										</div>
									</div>
									
									<div class="control-group">
                                       <label class="control-label">Salary Expectation</label>
                                       <div class="controls">
                                          <select tabindex="1" class="large m-wrap" name="salary">
                                             <option value="Less than 50000">Less than 50000</option>
	 <option value="50,000-2,00,000" >50,000-2,00,000</option>
	 <option value="2,00,000-5,00,000" >2,00,000-5,00,000</option>
	 <option value="5,00,000-7,50,000">5,00,000-7,50,000</option>
	 <option value="7,50,000-10,00,000" >7,50,000-10,00,000</option>
	 <option value="10,00,000 and above">10,00,000 and above</option>
					</select>
                                       </div>
                                    </div>
									
									<div class="control-group">
										<label class="control-label">Work Experiance</label>
										<div class="controls">
											<select tabindex="1" data-placeholder="Choose a Category" class="span6 m-wrap" name="experience">
												<option value="">Select...</option>
												<option value="Fresher">Fresher</option>
												<option value="1-2 Years">1-2 Years</option>
												<option value="2-5 Years">2-5 Years</option>
												<option value="5-10 Years">5-10 Years</option>
												<option value="10-20 Years">10-20 Years</option>
												<option value="20 Years and Above">20 Years and Above</option>
												
											</select>
										</div>
									</div>
									<div class="form-actions">
									<button type="submit" class="btn blue"></i>
Search 
										</button>
										
										                            
									</div>
								</form>
								<!-- END FORM-->       
							</div>
						</div>		
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				
</div>
