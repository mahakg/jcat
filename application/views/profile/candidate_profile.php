<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?=$user_data['name']; ?>
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid profile">
					<div class="span12">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab_1_1" data-toggle="tab">Overview</a></li>
								<li class=""><a href="#tab_1_2" data-toggle="tab">Profile Info</a></li>
								<li class=""><a href="#tab_1_3" data-toggle="tab">Edit Profile Info</a></li>

							</ul>
							<div class="tab-content">
								<div class="tab-pane row-fluid active" id="tab_1_1">
									<ul class="unstyled profile-nav span3">
										<li>
										<?
                                    	if(isset($user_data['profileurl']) && $user_data['profileurl']==""){
                                    	$src = asset_url() . 'img/size.gif';
                                    	}
                                    	else{
                                    	$src = $user_data['profileurl'];
                                    	}
                                     ?> 
										<img alt="" src="<?=$src ?>"></li>
                                                                                <li><a href="#tab_1_2" data-toggle="tab">Profile Info</a></li>
										<li><a href="#tab_1_3" data-toggle="tab">Edit Profile</a></li>
                                                                                <li><a  href="<?=site_url('profile/index'.'/'.$user_data['username']);?>" target="_blank">Your Social Profile</a></li>
									</ul>
									<div class="span9">
										<div class="row-fluid">
											<div class="span8 profile-info">
												<h1><?=$user_data['username']; ?></h1>
												<p><?=$user_complete_profile['summary'];  ?></p>
												<p><?=$user_data['email'];  ?></p>
												<ul class="unstyled inline">
													<li><i class="icon-map-marker"></i><?=$user_data['location']; ?> </li>
													<li><i class="icon-briefcase"></i> <?=$user_profile_data['designation'];  ?></li>
													<li><i class="icon-star"></i><?=$user_profile_data['experience'];  ?></li>
													
													
												</ul>
												
											</div>
										</div>
										<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class=""><a href="#tab_1_11" data-toggle="tab"><h4><b>Activity Feeds</b></h4></a></li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_11">
													<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;><div class="scroller" data-always-visible="1" data-rail-visible1="1" style="overflow: hidden; width: auto;">
															<ul class="feeds">
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-success">								
																					<i class="icon-bell"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					You have just edit your Profile
                                                                                                                                                                            
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			Just now
																		</div>
																	</div>
																</li>
															</ul>
														</div>

														
														</div>
												</div>
												
												<!--tab-pane-->
										</div>
									</div>
								</div>
									
							
							
												
								<div class="tab-pane profile-classic row-fluid" id="tab_1_2">
									<div class="span2"><?
                                    	if(isset($user_data['profileurl']) && $user_data['profileurl']==""){
                                    	$src = asset_url() . 'img/size.gif';
                                    	}
                                    	else{
                                    	$src = $user_data['profileurl'];
                                    	}
                                     ?>
                                       <img alt="" src="<?=$src ?>"></div>
									<ul class="unstyled span10">
										<li><span>User Name:</span><?=$user_data['username']; ?></li>
										<li><span>E-Mail:</span><?=$user_data['email']; ?></li>
										<li><span>Tagline:</span><?=$user_profile_data['tagline'];  ?></li>                                                                           
										<li><span>About me:</span><?=$user_profile_data['about_me'];  ?></li>
                                                                                <li><span>skill:</span><?=$user_profile_data['skill'];  ?></li>
										<li><span>Current Employer:</span><?=$user_profile_data['current_employer'];  ?></li>
										<li><span>Current Salary:</span><?=$user_profile_data['current_salary'];  ?></li>
										<li><span>Designation:</span><?=$user_profile_data['designation'];  ?></li>
										<li><span>Experience:</span><?=$user_profile_data['experience'];  ?></li>
										<li><span>Institute:</span><?=$user_profile_data['institute_name'];  ?></li>
										<li><span>Website URL:</span><?=$user_profile_data['website_url'];  ?></li>
									</ul>
								</div>
								<!--tab_1_2-->
								<div class="tab-pane row-fluid profile-account" id="tab_1_3">
									<div class="row-fluid">
										<div class="span12">
											<div class="span3">
                                                                                            <ul class="ver-inline-menu tabbable margin-bottom-10">
													<li class=""><a href="<?=site_url('profile/index'.'/'.$user_data['username']);?>" target="_blank"><i class="icon-picture"></i>Your Social Profile</a></li>
													<li class=""><a href="<?=site_url('profile/index/nikhiltuteja')?>" target="_blank"><i class="icon-user"></i>Sample Profile</a></li>
												</ul>
											</div>
											<div class="span9">
												<div class="tab-content">
													<div id="tab_1-1" class="tab-pane active">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse in">
																<form id="profile-details" name="profile-details" method="post" action="">
                                                                                                                                <label class="control-label">Tagline</label>
																<textarea class="span8 m-wrap" rows="3" placeholder="For Example :- Front-end developer, coffee lover & part-time photographer." name="tagline"><?=$user_profile_data['tagline'];  ?></textarea>
                                                                                                                                <label class="control-label">About Me</label>
																<textarea class="span8 m-wrap" rows="3" placeholder="For Example :- Hi there. I'm a front-end developer with over 11 years experience. I have a passion for details and visual quality. Experience on handling ambitious deadlines, organized and a good team-player." name="about_me"><?=$user_profile_data['about_me'];  ?></textarea>
                                                                                                                                <label class="control-label">Skill</label>
																<textarea class="span8 m-wrap" rows="3" placeholder="For Example:- Php,Excel,Java" name="skill"><?=$user_profile_data['skill'];  ?></textarea>
                                                                                                                                <span class="help-inline">Add Your Skills</span>
																<label class="control-label">Current Employer</label>
																<input type="text" value="<?=$user_profile_data['current_employer'];  ?>" class="m-wrap span8" placeholder="Your Current Company " name="current_employer">
																<label class="control-label">Current Salary</label>
																<input type="text" value="<?=$user_profile_data['current_salary'];  ?>" class="m-wrap span8" placeholder="For Example: 3.9 lpa"name="current_salary">
																<label class="control-label">Designation</label>
																<input type="text" value="<?=$user_profile_data['designation'];  ?>" class="m-wrap span8" placeholder="For Example:- software developer" name="designation">
																<label class="control-label">Experience</label>
																<input type="text" value="<?=$user_profile_data['experience'];  ?>" class="m-wrap span8" placeholder="For Example:- 2 years" name="experience">
																<label class="control-label">Website URL</label>
																<input type="text" value="<?=$user_profile_data['website_url'];  ?>" class="m-wrap span8" placeholder="www.xyz.com" name="website_url">
																<label class="control-label">Institute</label>
																<input type="text" value="<?=$user_profile_data['institute_name'];  ?>" placeholder="Harvard University" class="m-wrap span8" name="institute_name">
																<div class="submit-btn">
																	<a href="#" class="btn green" onclick="addUserProfileDetails();">Save Changes</a>
																	<a href="#" class="btn">Cancel</a>
																</div>
															</form>
														</div>
													</div>
																
																
															
												</div>
											</div>
										</div>
									</div>
											                                 
								</div>
                                                               
							</div>
						</div>
								<!--end tab-pane-->
					</div>
				</div>
						<!--END TABS-->
</div>
				
