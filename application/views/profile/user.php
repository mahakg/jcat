<?php $alert=$this->session->flashdata('msg');
 if($alert){  
    echo '<script type=\'text/javascript\'>';  
    echo 'alert("'.$alert.'");';  
    echo '</script>';
}  ?>
<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8" />
    <title><?=$user_data['name']; ?></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <!-- @todo: fill with your company info or remove -->

    <!-- Bootstrap CSS -->
    <link href="<?=asset_url(); ?>css/bootstrap.css" rel="stylesheet" />
    <link href="<?=asset_url(); ?>css/bootstrap-responsive.css" rel="stylesheet" />

    <!-- Plugins -->
    <link href="<?=asset_url(); ?>css/font-awesome.css" rel="stylesheet" />
    <link href="<?=asset_url(); ?>plugins/flexslider/flexslider.css" rel="stylesheet" />
    
    <!-- Theme style -->    
    <link href="<?=asset_url(); ?>css/theme-style.css" rel="stylesheet" />
        
    
    <!-- Put all your custom code/overrides here -->    
    <link href="<?=asset_url(); ?>css/custom-style.css" rel="stylesheet" />       
            
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons - @todo: fill with your icons or remove -->
    <link rel="shortcut icon" href="img/icons/favicon.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icons/114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/icons/72x72.png" />
    <link rel="apple-touch-icon-precomposed" href="img/icons/default.png" />
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Condiment' rel='stylesheet' type='text/css' />
      
    <!--Retina.js plugin - @see: http://retinajs.com/-->
    <script src="<?=asset_url(); ?>plugins/retina/retina.js"></script> 
 <style type="text/css">
 .skill-item {
  background-color: #54bac8;
    width: 200px;
    height: 200px;
    -webkit-border-radius: 200px;
    -moz-border-radius: 200px;
    border-radius: 200px; 
}
.skill {
  width: 100%!important;

}
.skill-title {
  font-weight: bold;
  text-align: center;
  font-size: 25px;
  margin-top: -10px;
}
 </style>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

  <body class="has-navbar-fixed-top page-index">
    
    <!-- @region: Navigation -->
    <div id="navigation" class="wrapper">
      <!-- Branding & Navigation -->
              <!--Main Navigation-->
        <div class="navbar navbar-fixed-top">
          <div class="navbar-inner">
            <div class="container">          
   
              <!--branding-->
              <div class="brand">
                <!-- Logo added with CSS -->
                <h1><a href="#content">Curr<span class="em">i</span></a></h1>
              </div>
              
              <!--mobile collapse menu button-->
              <a class="mobile-toggle-trigger"><i class="icon-reorder"></i></a>
              
              <!--scroll mobile navigation--> 
              <a class="mobile-toggle-trigger scroll-nav" data-js="scroll-show"><i class="icon-reorder"></i></a>
              
              <!--everything within this div is collapsed on mobile-->
              <div class="mobile-toggle pull-right">
                              
                <!--main navigation-->
                <ul class="nav" id="main-menu">
                   <li><a href="<?=site_url();?>">home</a></li>
                   <li><a href="<?=site_url('/home/index');?>">Dashboard</a></li>
                  <li><a href="#about">About</a></li>
                  <li><a href="#contact" class="stamp">Hire me</a></li>              
                </ul>
              </div><!--/.nav-collapse -->
            </div>
          </div>
        </div>
    </div>
    
    <!-- @region: Content -->
    <!--scroll-section class must be added to all scrollable sections-->
    <div id="content">
      <section class="scroll-section about block primary" id="about">
	<!-- About text & photo -->
	<div class="container">
	  <div class="row-fluid">
	    <div class="span3 photo">
	      <img src="<?=$user_data['profileurl']; ?>" alt="My picture" class="img-circle pull-center" />
              <ul class="unstyled inline">
		<li><i class="icon-map-marker"></i><?=$user_data['location']; ?> </li>
		<li><i class="icon-briefcase"></i> <?=$user_data['designation'];  ?></li>
		<li><i class="icon-star"></i><?=$user_data['experience'];  ?></li>
                <li><i class="icon-book"></i><?=$user_data['institute_name'];  ?></li>
              </ul>
	    </div>
	    <div class="span9 details">
	      <h2 class="primary-focus"><?=$user_data['name']; ?></h2>
	      <h3 class="secondary-focus"><?=$user_data['tagline']; ?></h3>
	      <p><?=$user_data['about_me']; ?></p>
	    </div>
	  </div>
	</div>
      </section>
        <?php if ($user_data['skill']){?>
              <!-- Skills & Services -->
      <section class="scroll-section services block gray" id="services">
          <div class="container">
                        <div class="row-fluid skill-row">	 <?php 
                                                                                  $skills=$user_data['skill'];
                                                                                  $skills_explode=explode(",", $skills);
                                                                                  foreach($skills_explode as $val){
                                                                                ?>
<div class="span3">

                                <div class="skill-item">
                                    <canvas class="skill"></canvas>
                                    <div class="skill-title"><?= $val;?></div>
                                </div> 
                            </div><?php }?>
                                                              </div> 
	</div>
        </section><?php }?>
             <?php if ($user_profile_data){?>
              <section class="scroll-section services block gray" id="services">
          <div class="container">
                        <div class="row-fluid skill-row"><?php foreach($user_profile_data as $val){?>
                            <div class="span3">
                                        <a class="fancybox-button" data-rel="fancybox-button" title="Photo" href="<? echo base_url() . 'badges/index'?>">
                           <img src="<? echo base_url() . 'files/badges'.'/'. $user_id.'/'.$val['badge_image']?>" alt="Photo" />							
                            </div></a><?php }?>
                        </div> 
	</div>
             </section><?}?>
        
      <!--Contact form-->
      <section class="scroll-section contact block" id="contact">
	<div class="container">
	  <h2>Hire Me</h2>
	  <div class="row-fluid">
	    <form id="contact-form" action="<?=site_url('/profile/contact');?>" method="post">
	      <div class="span5">
		<input type="text" placeholder="Name" name="contact[name]" class="input-primary" />        
		<input type="email" placeholder="Email" name="contact[from_email]" class="input-primary" />
                <input type="hidden" value="<?php echo $user_data['email']; ?>" name="contact[to_email]" class="input-primary" />
                <input type="hidden" value="<?php echo $user_data['username']; ?>" name="contact[username]" class="input-primary" />
              </div>
	      <div class="span7">
		<textarea rows="4" name="contact[message]" placeholder="Message"></textarea>
	      </div>
	      <button type="submit" class="btn btn-large btn-primary-light btn-square"><i class="icon-envelope"></i> Send Message</button>
	    </form>
	  </div>
	</div>
      </section>
      
    </div>
      
    <!-- @region: Footer -->
    <footer id="footer">
        <div class="container">
          <div class="row-fluid pull-center">
            <div class="social-media">
              <a href="#"><i class="icomoon-twitter-3"></i></a>
              <a href="#"><i class="icomoon-facebook-3"></i></a>
              <a href="#"><i class="icomoon-google-plus-4"></i></a>
              <a href="#"><i class="icomoon-dribbble-3"></i></a>
            </div>
            <!--@todo: replace with company copyright details-->
            <p>Copyright 2013 &copy; <a href="http://rateyourskill.com/beta">Rate Your Skill</a></p>
          </div>
        </div>    </footer>
    
    <!--Scripts -->
    <script src="<?=asset_url(); ?>js/jquery.js"></script>
    
    <!--Bootstrap Javascript -->
    <script src="<?=asset_url(); ?>js/bootstrap.min.js"></script>
    
    <!--Plugins -->
    <script src="<?=asset_url(); ?>js/jquery.flexslider-min.js"></script>
    <script src="<?=asset_url(); ?>plugins/jPanelMenu/jquery.jpanelmenu.min.js"></script>
    <script src="<?=asset_url(); ?>plugins/jRespond/js/jRespond.js"></script>
    <script src="<?=asset_url(); ?>plugins/onePageNav/jquery.scrollTo.js"></script>
    <script src="<?=asset_url(); ?>plugins/onePageNav/jquery.nav.js"></script>
    
    <!--Custom scripts mainly used to trigger libraries -->
    <script src="<?=asset_url(); ?>js/script.js"></script>
  </body>
</html>