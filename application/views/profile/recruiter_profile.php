<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?=$user_data['name']; ?>
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid profile">
					<div class="span12">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab_1_1" data-toggle="tab">Overview</a></li>
								<li class=""><a href="#tab_1_2" data-toggle="tab">Profile Info</a></li>
								<li class=""><a href="#tab_1_3" data-toggle="tab">Edit Profile Info</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane row-fluid active" id="tab_1_1">
									<ul class="unstyled profile-nav span3">
										<li>
										<?
                                    	if(isset($user_data['profileurl']) && $user_data['profileurl']==""){
                                    	$src = asset_url() . 'img/size.gif';
                                    	}
                                    	else{
                                    	$src = $user_data['profileurl'];
                                    	}
                                     ?> 
										<img alt="" src="<?=$src ?>"></li>
									</ul>
									<div class="span9">
										<div class="row-fluid">
											<div class="span8 profile-info">
												<h1><?=$user_data['username']; ?></h1>
												<p><?=$user_profile_data['remarks'];  ?></p>
												<p><?=$user_data['email'];  ?></p>
												<ul class="unstyled inline">
													<li><i class="icon-map-marker"></i> <?=$user_profile_data['company_location'];  ?></li>
													<li><i class="icon-briefcase"></i> <?=$user_profile_data['about_company'];  ?></li>
													<li><i class="icon-star"></i><?=$user_profile_data['company_size'];  ?></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
												
								<div class="tab-pane profile-classic row-fluid" id="tab_1_2">
									<div class="span2"><?
                                    	if(isset($user_data['profileurl']) && $user_data['profileurl']==""){
                                    	$src = asset_url() . 'img/size.gif';
                                    	}
                                    	else{
                                    	$src = $user_data['profileurl'];
                                    	}
                                     ?>
                                       <img alt="" src="<?=$src ?>"></div>
									<ul class="unstyled span10">
										<li><span>User Name:</span><?=$user_data['username']; ?></li>
										<li><span>E-Mail:</span><?=$user_data['email']; ?></li>
										<li><span>Company Name:</span><?=$user_profile_data['company_name'];  ?></li>
										<li><span>Company Type:</span><?=$user_profile_data['company_size'];  ?></li>
										<li><span>Birthday:</span><?=$user_profile_data['about_company'];  ?></li>
										<li><span>Company Location:</span><?=$user_profile_data['company_location'];  ?></li>
										<li><span>Remarks:</span><?=$user_profile_data['remarks'];  ?></li>
										<li><span>Website Url:</span> <a href="#"><?=$user_profile_data['website_url'];  ?></a></li>
									</ul>
								</div>
								<!--tab_1_2-->
								<div class="tab-pane row-fluid profile-account" id="tab_1_3">
									<div class="row-fluid">
										<div class="span12">
											<div class="span3">
											</div>
											<div class="span9">
												<div class="tab-content">
													<div id="tab_1-1" class="tab-pane active">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse in">
															<form id="profile-details" name="profile-details" method="post">   
		<label class="control-label">Company Name</label>
																<input type="text" value="<?=$user_profile_data['company_name'];  ?>" class="m-wrap span8" name="company_name">
																<label class="control-label">Company Type</label>
																<input type="text" value="<?=$user_profile_data['company_size'];  ?>" class="m-wrap span8" name="company_size">
																<label class="control-label">About Company</label>
																<input type="text" value="<?=$user_profile_data['about_company'];  ?>" class="m-wrap span8" name="about_company">
																<label class="control-label">Company Location</label>
									<input type="text" value="<?=$user_profile_data['company_location'];  ?>" class="m-wrap span8" name="company_location">
																<label class="control-label">Website URL</label>
																<input type="text" value="<?=$user_profile_data['website_url'];  ?>" class="m-wrap span8" name="website_url">
																<label class="control-label">Remarks</label>
																<textarea class="span8 m-wrap" rows="3" name="remarks"><?=$user_profile_data['remarks'];  ?></textarea>
																<div class="submit-btn">
																	<a href="#" class="btn green" onclick="addUserProfileDetails();">Save Changes</a>
																	<a href="#" class="btn">Cancel</a>
																</div>
															</form>
														</div>
													</div>
																
																
															
												</div>
											</div>
										</div>
									</div>
											                                 
								</div>
							</div>
						</div>
								<!--end tab-pane-->
					</div>
				</div>
						<!--END TABS-->
</div>
				
