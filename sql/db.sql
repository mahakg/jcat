
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(512) DEFAULT NULL,
  `uid` varchar(512) DEFAULT NULL,
  `provider` varchar(30) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `secret` varchar(512) DEFAULT NULL,
  `token` varchar(512) DEFAULT NULL,
  `summary` text,
  `profileurl` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
