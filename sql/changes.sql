-- Added By Mahak on Jul 15

CREATE TABLE `jcat_skills` (    `skill_id` int(10) unsigned NOT NULL AUTO_INCREMENT, `skill_name` int(11) NOT NULL , `skill_desc` varchar(500) DEFAULT NULL, `date_added` timestamp DEFAULT CURRENT_TIMESTAMP, primary key(skill_id) )ENGINE=InnoDB DEFAULT CHARSET=utf8;	 

-- Added by Mahak on july 21
CREATE TABLE `jcat_questions` (    `question_id` bigint(20) NOT NULL AUTO_INCREMENT,   `skill_id` int(10) unsigned not null,    `question` varchar(1024) DEFAULT NULL,    `optiona` varchar(100) DEFAULT NULL,    `optionb` varchar(100) DEFAULT NULL,    `optionc` varchar(100) DEFAULT NULL,    `optiond` varchar(100) DEFAULT NULL,   `optione` varchar(100) DEFAULT NULL,    `correctanswer` enum('optiona','optionb','optionc','optiond', 'optione') DEFAULT NULL,   `explanation` varchar(1024) DEFAULT NULL,    `difficulty_level` enum('1', '2', '3', '4', '5') DEFAULT 1,    PRIMARY KEY (`question_id`),   CONSTRAINT `skillid_ibfk_1` FOREIGN KEY (`skill_id`) REFERENCES `jcat_skills` (`skill_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Added by Mahak on july 21
CREATE TABLE `jcat_test_users` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
   `user_email` varchar(50) NOT NULL,
    `test_id` int(11) NOT NULL,
     PRIMARY KEY (`id`)
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- Renamed table users to jcat_users by Mahak on Jul 28   
Drop table users;

CREATE TABLE `jcat_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(512) DEFAULT NULL,
  `uid` varchar(512) DEFAULT NULL,
  `provider` varchar(30) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `secret` varchar(512) DEFAULT NULL,
  `token` varchar(512) DEFAULT NULL,
  `summary` text,
  `profileurl` varchar(512) DEFAULT NULL,
  `user_type` varchar(30) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;     

-- Added by Mahak on Jul 28
CREATE TABLE `jcat_users_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `company_name` varchar(150) DEFAULT NULL,
  `company_size` varchar(30) DEFAULT NULL,
  `about_company` varchar(8192) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `company_location` varchar(100) DEFAULT NULL,
  `website_url` varchar(255) DEFAULT NULL,
  `current_employer` varchar(512) DEFAULT NULL,
  `current_salary` varchar(11) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `skill_set` varchar(255) DEFAULT NULL,
  `experience` text,
  `highest_qualification` varchar(50) DEFAULT NULL,
  `institute_name` varchar(255) DEFAULT NULL,
  `batch_year` varchar(255) DEFAULT NULL,
  `resume_plain_text` text,
  `remarks` varchar(4096) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Added by Mahak on Aug 2
CREATE TABLE `jcat_newopportunities` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `job_title` varchar(50) NOT NULL,
 `job_description` varchar(1200) NOT NULL,
 `key_skills` varchar(1200) NOT NULL,
 `experience` varchar(50) NOT NULL,
 `job_location` text NOT NULL,
 `job_role` varchar(50) NOT NULL,
 `candidate_qualification` varchar(50) NOT NULL,
 `industry` varchar(50) NOT NULL,
 `salary` varchar(20) NOT NULL,
 `company_profile` varchar(1200) NOT NULL,
 `employment_type` varchar(20) NOT NULL,
 `date_expiry` date NOT NULL,
 `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `date_modified` datetime NOT NULL,
 `employment_status` enum('option1','option2') NOT NULL,
 `added_by` int(11) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Added by Mahak on Aug 5
ALTER TABLE  `jcat_newopportunities` add column  `status` ENUM(  'active',  'expired' ) NOT NULL AFTER  `date_expiry` ;

-- Added by Mahak on Aug 6
ALTER TABLE  `jcat_tests` CHANGE  `date_added`  `date_added` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ;

-- Added by Mahak on Aug 12
CREATE TABLE `jcat_tests` ( `id` int(11) NOT NULL AUTO_INCREMENT, `role` varchar(50) NOT NULL, `skill_set` varchar(512) NOT NULL, `candidate_name` varchar(512) NOT NULL, `candidate_email` varchar(512) NOT NULL, `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `conduct_status` enum('y','n','d') NOT NULL DEFAULT 'n', `hash_id` varchar(50) NOT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Added By Mahak on Aug 18 to keep track of 
CREATE TABLE `jcat_test_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `activity_name` varchar(250) NOT NULL,
   `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Added By Mahak on Aug 19 to keep track of score on every question activity
alter table jcat_test_log add column score int(11) not null after activity_name;

--Added By Mahak Gupta	on Aug27 to change the type of jcat_skills
ALTER TABLE `jcat_skills` CHANGE `skill_name` `skill_name` VARCHAR( 50 ) NOT NULL ;

-- Added by Mahak, on Aug 28 ,table to keep track of applied applications
CREATE TABLE `jcat_job_applications`(`application_id` int(11) NOT NULL AUTO_INCREMENT, `job_id` int(11) NOT NULL, `user_id` int(11) NOT NULL, `application_status` enum('rejected','approoved','applied', 'blacklisted') NOT NULL DEFAULT 'applied', `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`date_modified` datetime , PRIMARY KEY (`application_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Added by Mahak, Aug 28
alter table jcat_newopportunities modify column employment_status varchar(20);


-- Added by nikhil,Aug 30
ALTER TABLE  `jcat_job_applications` CHANGE  `date_modified`  `date_slot` DATETIME NULL DEFAULT NULL;


--Added by Mahak, Sep 5 -- Table to keep track of traffic on website
CREATE TABLE `jcat_log_table` (   `id` int(11) NOT NULL AUTO_INCREMENT,   `user_id` int(11) NOT NULL, module_name varchar(20),  `content_id` int(11) NOT NULL,   `date_added` datetime NOT NULL,   `week_num` int(10) NOT NULL,   `month_num` int(10) NOT NULL,   `ipaddress` varchar(30) NOT NULL,   `browser` varchar(255) NOT NULL,   `location` varchar(50) NOT NULL,   `country` varchar(20) NOT NULL,   `city` varchar(20) NOT NULL,   `act_via_newsletter` enum('Y','N') NOT NULL DEFAULT 'N',   PRIMARY KEY (`id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;  

-- Added by Mahak, Sep 5 -- table to track of user activities (activity type can be question_add, like a question, comment on a question, question_not_clear, etc) module name can be questions, skills, test, bages etc.
CREATE TABLE `jcat_activity` (   `id` int(11) NOT NULL AUTO_INCREMENT,   `user_id` int(11) NOT NULL,   `activity_type` varchar(50) NOT NULL,   `content_id` int(11) NOT NULL, module_name varchar(20),   `activity_date` datetime NOT NULL, activity_data varchar(256), PRIMARY KEY (`id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table to track for the activity of users';

-- Added by Mahak, Sep 8  -- table for handling mail queue
CREATE TABLE `jcat_queue` ( `id` int(11) NOT NULL AUTO_INCREMENT, `senderemail` varchar(100) NOT NULL, `sendername` varchar(100) NOT NULL,  `recpemail` varchar(100) NOT NULL, `subject` varchar(255) NOT NULL, `message` varchar(8192) NOT NULL, `error` enum('y','n') NOT NULL DEFAULT 'n', `createdon` datetime NOT NULL, `updatedon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, `attachments` varchar(2048) NOT NULL, `cc` varchar(255) DEFAULT NULL, `bcc` varchar(255) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Mailing queue';
-- Added on Mahak on Sep 8-- 
alter table jcat_log_table drop column act_via_newsletter;
alter table jcat_log_table drop column location;

-- Added by Mahak on Sep 9
alter table jcat_test_log add column skill_id int(11) after user_id;
create table jcat_badges ( `id` int(11) NOT NULL AUTO_INCREMENT, `user_id` int(11) NOT NULL, skill_id int(11) not null, badge_type varchar(20) not null, PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Badge List';
alter table jcat_badges add unique(user_id, skill_id, badge_type);
alter table jcat_badges add column processed enum('Y','N') default 'N';
alter table jcat_badges add column badge_image varchar(50);
alter table jcat_badges add column date_added timestamp default current_timestamp;
alter table jcat_badges add column date_modified datetime default '0000-00-00 00:00:00';

-- Added by Mahak on Sep 11
alter table jcat_badges add column score int(11) not null after skill_id;

